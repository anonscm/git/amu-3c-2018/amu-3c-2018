#####################################
# Cecile Prouteau
# CTES Projet Math-Info
# Projet segmentation d'image
# 15/03/2019
# Classe Pixel Version 3.0
#####################################
import math

### DEBUT de la classe Pixel
### contient la recherche des voisins , on se place du point de vue des coordonnées sur l'image
class Pixel:
    """

    **LES METHODES**
     
    """
    
    def __init__(self,x,y,v):
        """
        Constructeur d'un objet Pixel
        
        Construit un sommet à partir des coordonnées d'un pixel sur une image
        
        :param self: Objet Sommet 
        :type self: Sommet
        
        :param x: abscisse du point sur l'image
        :type FilePath: int  
        
        
        :param y: ordonnée du point  
        :type FilePath: int      
        
        :param v: valeur du point  
        :type FilePath: int 
        
        :return: Objet Sommet
        :rtype: Sommet
        
        Liste des champs de la classe
        
        * self.x abscisse 
        * self.y ordonnée
        * self.v valeur du pixel self
        * self.zone indique dans quelle zone se trouve le pixel 
        
        Pour les lignes et les colonnes je me place du point de vue de l'image. 
        Dans le tableau numpy, les coordonnées sont inversées.
        
        """
        
        self.x=x
        self.y=y
        self.v=v
        self.zone=None
        
    def maj_v(self,val):
        """
        Procedure qui met à jour l'attribu self.id avec la valeur val
        
        :param self: Objet Sommet 
        :type self: Sommet
        
        :param val: entier à pour mise à jour du numéro de sommet
        :type val: int  
        
        :return: None 
        :rtype: None object
        
        """
        self.v=val
        
    def maj_zone(self,z):
        """
        Procedure qui met à jour l'attribu self.dist avec la valeur val
        
        :param self: Objet Sommet 
        :type self: Sommet
        
        :param val: entier à pour mise à jour de la zone d'appartenance au sommet
        :type val: int  
        
        :return: None 
        :rtype: None object
        
        """
        self.zone=z
        
   
    def coord_voisin(self,direction):
        
        """
        :param self: Objet Sommet 
        :type self: Sommet
        
        :param direction: chaine de caractère parmi 'droite', 'gauche', 'haut', 'bas'
        :type direction: str  
        
        :return: un couple de coordonnée entières (x', y') du pixel voisin de celui de coordonnée (x,y) dans la direction indiquée par la variable direction. 
        :rtype: int*int
        
        """
        
        if direction=='droite':
            return (self.x+1,self.y)
        elif direction=='gauche':
            return (self.x-1,self.y)
        elif direction=='haut':
            return (self.x,self.y-1)
        elif direction=='bas':
            return (self.x,self.y+1)
        else :
            print("Erreur de paramètre dans Sommet.coord_voisin!")  
            exit(1) 
             
    def est_superieur(self,pixel):
        """
        
        :param self: Objet Pixel 
        :type self: Pixel
        
        :param sommet: Objet Pixel
        :type sommet: Pixel  
        
        :return: True si la valeur de self est plus grande que la valeur de pixel et False sinon
        :rtype: bool
        
        """ 
        
        return  self.v > pixel.v  
            
    def affiche_sommet(self):
        """
        Affiche les coordonnées de self
        
        :param self: Objet Sommet 
        :type self: Sommet
        
        :return: None 
        :rtype: None object
        
        """
        print(self.x,self.y,self.v,self.zone)        