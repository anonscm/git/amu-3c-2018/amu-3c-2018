#####################################
# Cecile Prouteau
# CTES Projet Math-Info
# Projet segmentation d'image
# 15/01/2019
# Classe Image Version 1.4
#####################################

### IMPORTATION DES BIBLIOTHEQUES 
#import tkinter.filedialog as tkfd
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
#import tkinter as tk
import numpy as np

### IMPORTATION DES CLASSES
from Couleur import Couleur

### DEBUT de la classe Image
### contient IMPORT - EXPORT PNG en niveaux de gris <-> NUMPY ARRAY et AFFICHAGE 
class Image:
    def __init__(self,FilePath):
        self.file=FilePath
        self.image_np_gray=self.import_image_from_file()
        """pour les lignes et les colonnes je me place du point de vue de l'image
        Dans le tableau numpy, les coordonnées sont inversées"""
        self.nb_colonne = self.image_np_gray.shape[0]
        self.nb_ligne = self.image_np_gray.shape[1]
        self.image_np_RGB=self.gray_2_RGB()
    
    def import_image_from_file(self):
        """ fonction de chargement dans la variable img de l'image png accessible avec le chemin filepath
        str -> Numpy Array
        importe l'image png à l'adresse FilePath"""
        
        return mpimg.imread(self.file)
    
    def gray_2_RGB(self):
        """ fonction de conversion d'un tableau numpy image importe du fichier image en niveaux de gris PNG en un tableau numpy png RGB
        image -> numpy Array 
        les donnees de l'image importee en niveaux de gris PNG sont recopiees dans un numpy Array image RGB PNG qui est retourne """
        
        # Initialisation des tableau numpy a la taille de l'image et pleins de zeros
        # gray = np.zeros((nb_colonne,nb_ligne))
        gray_RGB = np.zeros((self.nb_colonne,self.nb_ligne,3))
        # Importation des donnees de l'image dans les tableaux numpy, en niveau de gris d'une part (pour le traitement)
        # en couleur RVB d'autre part (pour l'affichage et l'export)
        for i in range (self.nb_colonne):
            for j in range (self.nb_ligne):
                gray_RGB[i, j, 0] = self.image_np_gray[i, j]
                gray_RGB[i, j, 1] = self.image_np_gray[i, j]
                gray_RGB[i, j, 2] = self.image_np_gray[i, j]
        return gray_RGB

    ## Fonction de coloriage dans la couleur Couleur d'une liste de points de l'image dans un tableau NUMPY

    def colorie_liste_points(self,L,Color):
        """ List * str * Numpy Array -> 
        Procedure prend en entree une liste de points au format [x,y], une couleur de la liste List_couleur, 
        un tableau Numpy Array representant une image PNG en RVB.
        la fonction change la couleur des points du tableau T de coordonnées les valeurs se trouvant dans la liste pour Couleur.
        le tableau self.image_np_RGB est modifie par effets de bord"""
        
        color = Couleur(Color)
        (rouge,vert,bleu)= color.RVB_png
        for point in L:     # un point est une liste de 2 cordonnees [x,y]
            """ point[0] correspond à x sur l'image, point[1] correspond à y sur l'image. 
            Les coordonnées sont inversées sur le tableau où i correspond à l'indice de ligne et j l'indice de colonne"""
            i=point[1]
            j=point[0]
            self.image_np_RGB[i, j, 0] = rouge
            self.image_np_RGB[i, j, 1] = vert
            self.image_np_RGB[i, j, 2] = bleu
    
    def colorie_zone_img_RGB_np(self,L,Couleur):
        """ colorie la zone determinee par L dans l'image RGB avec la couleur Couleur"""
        
        self.colorie_liste_points(L,Couleur)
    
    def affiche_image_RGB(self): 
        """ Affichage de l'image transformee 
        numpy Array ->
        procedure affichant l'image contenu dans le numpy Array image"""   
           
        plt.axis('off')
        plt.imshow(self.image_np_RGB)
        plt.show()
    
    def export_image_2_file(self,chaine):
        """ Export de l'image  
        numpy Array * str ->
        procedure exportant le tableau numpy image à l'adresse FilePath modifiee pour tenir compte du changement de format """
        
        filepath_gray_RGB=self.file.rstrip('.png')+"_"+chaine+"_gray_RGB.png"
        mpimg.imsave(filepath_gray_RGB,self.image_np_RGB)
        
    def couleur_entier_8bits(self,x,y):
        """retourne la couleur/niveau de gris du pixel de coordonnées (x,y) comme entier entre 0 et 255"""
        return couleur_float_2_entier_8bits(self.image_np_gray[x, y])
    
### FIN DE LA CLASSE IMAGE