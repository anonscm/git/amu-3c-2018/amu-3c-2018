#####################################
# Cecile Prouteau
# CTES Projet Math-Info
# Projet segmentation d'image
# 15/01/2019
# Classe Image Version 1.4
#####################################

### IMPORTATION DES BIBLIOTHEQUES 
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np

### IMPORTATION DES CLASSES
from Couleur import Couleur

### DEBUT de la classe Image
### contient IMPORT - EXPORT PNG en niveaux de gris <-> NUMPY ARRAY et AFFICHAGE 
class Image:
    
    """
    
    **AUTRES CLASSES IMPORTEES**
    
    * Couleur
    * matplotlib.pyplot
    * matplotlib.image
    * numpy
   
    
    **LES METHODES**
     
    
    """
    
    def __init__(self,FilePath):
        """
        Constructeur d'un objet Image
        
        :param self: Objet Image 
        :type self: Image
        
        :param FilePath: Chemin du fichier image à importer  
        :type FilePath: string        
        
        :return: Objet Image
        :rtype: Image
        
        Liste des champs de la classe
        
        * self.file Chemin du fichier image sur la machine
        * self.image_np_gray Image importée au format png avec mpimg.imread 
        * self.hauteur hauteur de l'image en pixels
        * self.largeur largeur de l'image en pixels
        * self.image_np_RGB Tableau numpy RGB obtenu à partir d'un tableau numpy niveaux de gris (chacune des 3 valeurs est un flottant compris entre 0 et 1 et prend la vareur du niveau de gris de l'image de départ)
        
        Pour les lignes et les colonnes je me place du point de vue de l'image. 
        Dans le tableau numpy, les coordonnées sont inversées.
        
        """
        self.file=FilePath
        self.image_np_gray=self.import_image_from_file()
        # Pour les lignes et les colonnes je me place du point de vue de l'image
        # Dans le tableau numpy, les coordonnées sont inversées

        self.hauteur = self.image_np_gray.shape[0]
        self.largeur = self.image_np_gray.shape[1]
        self.image_np_RGB=self.gray_2_RGB()
    
    def import_image_from_file(self):
        """ 
        Fonction de chargement de l'image png accessible avec le chemin filepath et de conversion en tableau Numpy

        :param self: Objet Image 
        :type self: Image
        
        :return: le tableau Numpy correspondant à l'image png niveaux de gris située à l'adresse self.FilePath
        :rtype: Numpy Array
        
        """
        
        return mpimg.imread(self.file)
    
    def gray_2_RGB(self):
        """ 
        Fonction de conversion d'un tableau numpy image importe du fichier image en niveaux de gris PNG en un tableau numpy png RGB.
        Les donnees de l'image importée en niveaux de gris PNG sont recopiées dans un Numpy Array image RGB PNG qui est retourné.
        Pour chaque pixel, les valeurs de RVB sont toutes les trois égales à la valeur du niveau de gris du fichier en niveau de gris initial.
        3 canaux : 
            gray_RGB[i, j, 0] -> rouge
            gray_RGB[i, j, 1] -> vert
            gray_RGB[i, j, 2] -> bleu
        
        :param self: Objet Image 
        :type self: Image
        
        :return: le tableau Numpy correspondant à l'image png situtée à l'adresse self.FilePath
        :rtype: Numpy Array
        
        """
        
        # Initialisation des tableau numpy a la taille de l'image et pleins de zeros
        gray_RGB = np.zeros((self.hauteur,self.largeur,3))
        # Importation des donnees de l'image dans les tableaux numpy,
            # a partir de self.image_np_gray niveau de gris (pour le traitement)
            # convertie en couleur RVB (pour l'affichage et l'export)
        for i in range (self.hauteur):
            for j in range (self.largeur):
                gray_RGB[i, j, 0] = self.image_np_gray[i, j]
                gray_RGB[i, j, 1] = self.image_np_gray[i, j]
                gray_RGB[i, j, 2] = self.image_np_gray[i, j]
        return gray_RGB

    ## Fonction de coloriage dans la couleur Couleur d'une liste de points de l'image dans un tableau NUMPY

    def colorie_liste_points(self,L,Color):
        """ 
         
        Procedure qui prend en entree une liste de points au format [x,y], une couleur de la liste List_couleur.  
        Elle change la couleur des points du tableau self.image_np_RGB (un tableau Numpy Array representant une image PNG en RVB) de coordonnées les valeurs se trouvant dans la liste pour Couleur. le tableau  est modifie par effets de bord
        
        :param self: Objet Image 
        :type self: Image
        
        :param L: Liste de points au format [x,y]
        :type L: List
        
        :param Color: une couleur du dictionnaire coul 
        :type Color: str
        
        :return: None 
        :rtype: None object
        
        """
        
        color = Couleur(Color)
        (rouge,vert,bleu)= color.RVB_png
        for point in L:     # un point est une liste de 2 cordonnees [x,y], y en vertical et x en horizontal lors de la capture
            # point[0] correspond à x sur l'image, point[1] correspond à y sur l'image. 
            # Les coordonnées sont inversées sur le tableau où i correspond à l'indice de ligne et j l'indice de colonne
            i=point[1]
            j=point[0]
            if 0<=i<self.hauteur and 0<=j<self.largeur:
                self.image_np_RGB[i, j, 0] = rouge
                self.image_np_RGB[i, j, 1] = vert
                self.image_np_RGB[i, j, 2] = bleu
    
    
    def affiche_image_RGB(self): 
        """ 
        Affichage de l'image self.image_np_RGB. (Numpy png niveaux de gris) 
        
        :param self: Objet Image 
        :type self: Image
        
        :return: None 
        :rtype: None object
        
        """   
           
        plt.axis('off')
        plt.imshow(self.image_np_RGB)
        plt.show()
    
    def export_image_2_file(self,chaine):
        """ 
        
        Procedure exportant le tableau numpy image RGB en image png à l'adresse FilePath modifiee. 
        
        :param self: Objet Image 
        :type self: Image
        
        :param chaine: chaine de caractère servant à modifier le nom du fichier d'enregistrement de l'image à partir du nom de départ. 
        :type chaine: Str
        
        :return: None 
        :rtype: None object
        
        """
        
        filepath_gray_RGB=self.file.rstrip('.png')+"_"+chaine+"_gray_RGB.png"
        mpimg.imsave(filepath_gray_RGB,self.image_np_RGB)
        
    def couleur_entier_8bits(self,x,y):
        """
        Retourne la couleur(niveau de gris) du pixel de coordonnées (x,y) qui était codée comme flottant entre 0 et 1 comme entier entre 0 et 255
        
        :param self: Objet Image 
        :type self: Image
        
        :param x: abscisse du pixel sur l'image. 
        :type x: float
        
        :param y: ordonnée du pixel sur l'image.
        :type y: float
        
        :return: le tableau Numpy correspondant à l'image png situtée à l'adresse self.FilePath
        :rtype: Numpy Array
        
        """

        return Couleur.couleur_float_2_entier_8bits(self.image_np_gray[x, y])
    
### FIN DE LA CLASSE IMAGE