#####################################
# Cecile Prouteau
# CTES Projet Math-Info
# Projet segmentation d'image
# 15/01/2019
# Classe Couleur Version 1.4
#####################################
coul = {'red':(255,0,0), 'blue':(0,0,255), 'green':(0,255,0), 'yellow':(255,255,0), 'cyan':(0,255,255), 'magenta':(255,0,255)}

class Couleur:
    """
    
    **LES CONSTANTES**
    
 
    **Couleurs disponibles pour le coloriage des zones détectées**
    
    Le dictionnaire coul associe des noms de couleur au triplet de couleurs RGB codes chacun sur 8 bits.
    Il contient les couleurs suivantes : 
    'red'(rouge) ; 'blue'(bleu) ; 'green'(vert) ; 'yellow'(jaune) ; 'cyan' ; 'magenta'
   
    
    **LES METHODES**
     
    
    """
    
    #coul = {'red':(255,0,0), 'blue':(0,0,255), 'green':(0,255,0), 'yellow':(255,255,0), 'cyan':(0,255,255), 'magenta':(255,0,255)}
    
    
    def __init__(self,couleur):
        """
        Constructeur d'un objet Couleur
        
        :param self: Objet Couleur 
        :type self: Couleur
        
        :param couleur: chaine de caractère définissant la couleur  
        :type couleur: string        
        
        :return: Objet Couleur
        :rtype: Couleur
        
        Liste des champs de la classe
        
        * Couleur.nom nom du dictionnaire
        * Couleur.RVB_8bits codage RVB sur 3 fois 8 bits (chacune des 3 valeurs est comprise entre 0 et 255)
        * Couleur_png codage RVB pour format PNG (chacune des 3 valeurs est comprise entre 0 et 1)
        
        """
        
        self.nom=couleur
        self.RVB_8bits=self.couleur_2_RVB(couleur)
        self.RVB_png=self.couleur_2_RVB_png(couleur)
        
    ### DEFINITION DES FONCTIONS
    
    def couleur_2_RVB(self,Couleur):
        """ 
        Fonction convertissant la couleur 'Couleur' au format texte provenant de Liste_couleur en un triplet correspondant a son codage RGB.
        :param self: Objet Application  
        :type self: Application
        
        :param Couleur: Couleur doit être une entrée du dictionnaire coul.  
        :type Couleur: String
        
        :return: code couleur RVB. Triplet d'entiers compris entre 0 et 255
        :rtype: (int,int,int)
        """   
        
        return coul[Couleur]
    
    def couleur_2_RVB_png(self,Couleur):
        """ 
        Fonction convertissant une couleur au format texte provenant de Liste_couleur en un triplet correspondant a son codage RGB dans une image PNG.
        
        :param self: Objet Application  
        :type self: Application
        
        :param Couleur: Couleur doit être une entrée du dictionnaire coul.  
        :type Couleur: String
        
        :return: code couleur RVB format PNG. Triplet de flottants compris entre 0 et 1
        :rtype: (float,float,float)
        """
        
        (rouge,vert,bleu)=coul[Couleur]
        return (rouge/256,vert/256,bleu/256)
    
    def couleur_float_2_entier_8bits(c):
        """
        Converti un flottant compris entre 0 et 1 en entier du format png comme entier entre 0 et 255. Pour faciliter la conversion d'une couleur codée en RVB format PNG en RVB 3 fois 8bits.
        
        :param c: doit être un nombre flottant entre 0 et 1.  
        :type c: float
        
        :return: code couleur RVB. Entiers compris entre 0 et 255
        :rtype: int
        
        """
        
        c1=int(256*c)
        if c1>255:
            c1=255
        return c1
