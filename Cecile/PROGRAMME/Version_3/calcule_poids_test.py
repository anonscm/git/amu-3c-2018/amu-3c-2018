def calcule_poids(o,d):
    """
    Fonction de calul du poids d'un arête en fonction des valeurs des pixels
    
    :param o: doit être un nombre entier entre 0 et 255.  
    :type o: int
    
    :param d: doit être un nombre entier entre 0 et 255.  
    :type d: int
    
    :return: valeur de l'arc. Entier compris entre 0 et 255
    :rtype: int
    
    """
    return 255-abs(d-o)