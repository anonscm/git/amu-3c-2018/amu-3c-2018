#####################################
# Cecile Prouteau
# CTES Projet Math-Info
# Projet segmentation d'image
# 15/03/2019
# Classe ListeTrieePixel Version 3.0
#####################################
import math

from Pixel import Pixel

### DEBUT de la classe ListeTrieePixel

class ListeTrieePixel:
    
    """
    
    **AUTRES CLASSES IMPORTEES**
    
    * math
    
    **LES METHODES**
     
    
    """
    
    def __init__(self):
        """
        Constructeur d'un objet ListeTrieePixel (Liste initialisée à vide)

        
        :param self: Objet ListeTrieePixel
        :type self: ListeTrieePixel
        
        :return: Objet ListeTrieePixel des pixels dans l'ordre croissant de leurs valeurs
        :rtype: ListeTrieePixel
        
        Liste des champs de la classe
        
        * self.liste liste triée de Pixels dans l'ordre croissant de leurs valeurs


        """
        
        self.liste=[]
    
    ## Fonctions sur les listes triées de pixels

    def insere_pixel(self, pix):
        """
        
        Insère un pixel à sa place dans l'objet ListeTrieePixel

        
        :param self: Objet ListeTrieePixel
        :type self: ListeTrieePixel
        
        :param pix: pixel à insérer à sa place dans la liste triée en ordre croissant
        :type pix: Objet Pixel
        
        :return: Objet ListeTrieePixel
        :rtype: ListeTrieePixel
        

        """
        if len(self.liste)==0:
            self.liste.append(pix)
        else :
            i=0
            while i<len(self.liste) and pix.est_superieur(self.liste[i]):
                i+=1
            self.liste.insert(i,pix)

    def pop_pixel(self,i):
        """
        
        Extrait le pixel d'indice i dans l'objet ListeTrieePixel, le retourne et le retire de la liste.

        
        :param self: Objet ListeTrieePixel
        :type self: ListeTrieePixel
        
        :param i: indice du pixel à retirer de la liste et à retourner.
        :type i: int
        
        :return:  Pixel d'indice i dans la liste self.liste
        :rtype: Objet Pixel
        

        """
        return self.liste.pop(i)
    
    def plus_petit_pixel(self):
        
        """
        
        Retourne le plus petit pixel de la ListeTrieePixel, c'est à dire le premier élément

        
        :param self: Objet ListeTrieePixel non vide
        :type self: ListeTrieePixel
        
        :return: Objet ListeTrieePixel
        :rtype: ListeTrieePixel
        

        """
        
        #On recupere le premier pixel de la liste 

        return self.liste[0]

    
    def taille_liste(self):
        
        """
        
        Retourne le nombre d'éléments de la liste


        :param self: Objet ListeTrieePixel
        :type self: ListeTrieePixel
        
        :return: le nombre d'éléments de la liste self.liste
        :rtype: int
        

        """
        
        return len(self.liste)
    
    def est_vide(self):
        """
        
        Retourne True si la liste self.liste est vide et False sinon


        :param self: Objet ListeTrieePixel
        :type self: ListeTrieePixel
        
        :return: True si la liste self.liste est vide et False sinon
        :rtype: bool
        

        """
        return self.liste==[]

    def affiche_liste_triee_pixel(self):
        
        """
        
        Affiche la ListeTrieePixel avec toutes les informations de chaque pixel, un par ligne

        
        :param self: Objet ListeTrieePixel
        :type self: ListeTrieePixel
        
        :return: None
        :rtype: None
        

        """
        
        for pix in self.liste:
            pix.affiche_pixel()