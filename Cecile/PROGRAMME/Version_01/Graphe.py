#####################################
# Cecile Prouteau
# CTES Projet Math-Info
# Projet segmentation d'image
# 15/01/2019
# Classe Graphe Version 0.1
#####################################

### IMPORTATION DES BIBLIOTHEQUES
import numpy as np
import math

### IMPORTATION DES CLASSES
from Image import Image
from Sommet import Sommet
from Arc import Arc


### DEBUT de la classe Graphe

class Graphe:
    def __init__(self,image,source,puits):
        """image est un tableau numpy d'une image codée sur une couche en niveaux de gris
        source et puits sont des sommets particulier du graphe"""
        
        #self.image=image.image_np_gray
        
        self.largeur = image.nb_colonne
        
        self.hauteur = image.nb_ligne
        
        self.nb_sommets = image.nb_colonne*image.nb_ligne
        
        #La liste des sommets du graphe
        self.liste_sommets = self.image_2_liste_sommets(image)
        
        #La liste des listes des arcs partant de chaque sommet graphe
        self.liste_arcs = self.image_2_liste_arcs(image)
        
        self.source = source
        
        self.no_source = self.coordonnees_2_no_sommet(source.x,source.y)
        
        self.puits = puits
        
        self.no_puits=self.coordonnees_2_no_sommet(puits.x,puits.y)
    
    ## FONCTIONS SUR LES SOMMETS    
    def coordonnees_2_no_sommet(self,x,y):
        """Retourne l'indice du sommet dans la liste à partir de ses coordonnées de point sur l'image"""
        return x*(self.largeur-1)+y
        
    def sommet_2_no_sommet(self,sommet):
        """Retourne l'indice du sommet dans la liste à partir du sommet"""
        return self.coordonnees_2_no_sommet(sommet.x,sommet.y)
        
    def no_sommet_2_coordonnees(self,n):
        """Retourne les coordonnées du sommet sur l'image à partir de son numéro dans la liste"""
        return (n//self.largeur,n%self.largeur)
    
    def ajoute_sommet(self,x,y):
        """Ajoute le sommet de coordonnées (x,y) sur l'image à la liste des sommets"""
        sommet=Sommet(x,y)
        self.liste_sommet.append(sommet)
    
    def image_2_liste_sommets(self,img):
        """Crée la liste des sommets à partir de l'image parcourue colonne par colonne - le tableau numpy est parcouru ligne par ligne à cause de l'inversion des coordonnées )"""
        self.liste_sommets=[]
        for x in range (img.nb_colonne):
            for y in range(img.nb_ligne):
                self.ajoute_sommets(x, y)
                
    def liste_no_sommets_voisins(self,no_sommet):
        """ détermine la liste des numéros de sommets voisins d'un sommet identifié par son numéro
        est utilisée pour la création de la liste des arcs donc ne peut pas se servir de celle-ci"""
        Liste_voisins=[]
        
        sommet=self.liste_sommets[no_sommet]
        if sommet.x > 0:
            Liste_voisins.append(sommet.voisin('gauche'))
        if sommet.x < self.largeur :
            Liste_voisins.append(sommet.voisin('droit'))
        if sommet.y > 0:
            Liste_voisins.append(sommet.voisin('haut'))
        if sommet.y < self.hauteur :
            Liste_voisins.append(sommet.voisin('bas'))
            
        # si liste des arcs connue la fonction pourrait être
        # for a in self.liste_arcs:
        #     Liste_voisins.append(self.coordonnees_2_no_sommet(a.destination.x,a.destination.y))
        return Liste_voisins

    ## FONCTIONS SUR LES ARCS

    def cree_arc(self,origine,destination,img): 
        """ Crée un arc allant du sommet origine au sommet destination. son poids est déterminé comme valeur absolue de la différence entre les couleurs des pixels correspondants sur l'image img codées sur 8 bits en niveaux de gris"""
        poids = abs(img.couleur_entier_8bits(destination.x,destination.y) - img.couleur_entier_8bits(origine.x,origine.y))
        return Arc(origine,destination,poids)
    
    def cree_double_arc(self,sommet_1,sommet_2):
        """Crée les arcs dans les deux sens entre le sommet 2 et le sommet 2"""
        self.cree_arc(sommet_1,sommet_2)
        self.cree_arc(sommet_2,sommet_1)
        
    def ajoute_arc(self,origine,destination,img): #??? 
        """ Ajoute l'arc allant du sommet origine au sommet destination. son poids est déterminé comme valeur absolue de la différence entre les couleurs des pixels correspondants sur l'image img codées sur 8 bits en niveaux de gris à la liste des arcs issus du sommet origine"""
        arc=self.cree_arc(origine,destination,img)
        no_sommet = coordonnees_2_no_sommet(origine.x,origine.y)
        self.liste_arcs[no_sommet].append(arc)
        
    def supprime_arc(self,arc):
        no_origine= self.coordonnees_2_no_sommet(arc.origine.x,arc.origine.y)
        for a in self.liste_arcs[no_origine]:
            if arc.destination == a.destination :
                self.liste_arcs[no_origine].remove(a)
    
    def cree_arcs_de_sommet_vers_voisins(self, no_sommet, img):
        """ Crée tous les arc partant d'un sommet identifié par son no vers tous ses voisins
        si le sommet n'est pas le puits, chercher les voisins et creer les arcs qui vont su sommet vers ses voisins"""
        sommet=self.liste_sommets[no_sommet]
        if no_sommet != self.no_puits :
            Liste_no_sommets_voisins = self.liste_no_sommets_voisins(no_sommet)
            for no_voisin in Liste_no_sommets_voisins:
                voisin = self.liste_sommets[no_voisin]
                self.ajoute_arc(sommet,voisin,img)
                
    def supprime_arcs_vers_sommet(self,no_sommet):
        """supprime tous les arcs arrivant à un sommet repéré par son no_sommet et partant de ses voisins"""
        sommet=self.liste_sommets[no_sommet]
        Liste_no_sommets_voisins = self.liste_no_sommets_voisins(no_sommet)
        for no_voisin in Liste_no_sommets_voisins:
            # recopie de l'adresse de la liste puis modification du contenu commun
            Liste_arcs_du_voisin = self.liste_arcs[no_voisin] 
            for arc in Liste_arcs_du_voisin :
                if  sommet.est_egal(arc.destination) :
                    Liste_arcs_du_voisin.remove(arc)
        
    def image_2_liste_arcs(self,img):
        """ Crée la liste des arcs à partir de l'image 
        La liste des sommets doit être créée au préalable """
        self.liste_arcs=[]
        for no_sommet in range (self.nb_sommets):
            self.cree_arcs_de_sommet_vers_voisins(no_sommet, img)    
        
        """ On supprime les arcs qui arrivent au sommet source et qui ont étés créés inutilement 
        (c'est moins long que de tester si source est le voisin à chaque création d'arc étant donné le nombre
        de pixels dans une image, le coût de la supression est constant)"""
        self.supprime_arcs_vers_sommet(self.no_source)
            
    ## FONCTIONS POUR LA RECHERCHE DE CHEMIN
    
    def chemin_minimal(self,no_sommet_depart):
        """ calcule le chemin minimal entre """
        # Création de la liste des distances au sommet de départ
        liste_distance=self.nb_sommets*[math.inf]
        liste_distance[no_sommet_depart]=0
        # Céation de la liste des numéros de sommets à explorer
        Liste_no_sommets_a_explorer=[no_sommet_depart]
        while Liste_no_sommets_a_explorer != []:
            no_sommet_a_explorer = Liste_no_sommets_a_explorer [0]
            #Liste_no_sommets_voisins = self.liste_no_sommets_voisins(no_sommet_a_explorer)
            ##on a la liste des arcs... donc on connais les voisins...
            for arc in self.liste_arcs[no_sommet_a_explorer]:
                distance_sommet_a_explorer=liste_distance[no_sommet_a_explorer]
                no_sommet_voisin= self.sommet_2_no_sommet(arc.destination)
                distance_sommet_voisin=liste_distance[no_sommet_voisin]
                distance_sommet_a_voisin = arc.poids
                if distance_sommet_voisin > distance_sommet_a_explorer + distance_sommet_a_voisin :
                    Distance[no_sommet_voisin] = distance_sommet_a_explorer + distance_sommet_a_voisin
                    Liste_no_sommets_a_explorer.append(no_sommet_voisin)
            Liste_no_sommets_a_explorer.remove(no_sommet_a_explorer)
    return liste_distance   
        
        
    
    ## FONCTION DE GRAPH CUT
    
     
        
                
        