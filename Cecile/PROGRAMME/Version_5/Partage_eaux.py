#####################################
# Cecile Prouteau
# CTES Projet Math-Info
# Projet segmentation d'image
# 15/03/2019
# Classe Partage des eaux Version 0.1
#####################################

### IMPORTATION DES BIBLIOTHEQUES
import numpy as np
import math

### IMPORTATION DES CLASSES
from Image import Image
from Pixel import Pixel
from Gradient import Gradient
from ListeTrieePixel import ListeTrieePixel



### DEBUT de la classe Partage_eaux

class Partage_eaux:
    def __init__(self, image, L_Obj, L_Bkg, gamma):
        """

        Constructeur d'un objet Graphe à partir d'une image d'un pixel servant de 

        :param self: Objet Graphe 
        :type self: Graphe
        
        :param image: objet Image est un tableau numpy d'une image codée sur une couche en niveaux de gris
        :type image: Image 
        
        :return: Objet Partage_eaux
        :rtype: Partage_eaux
        
        Liste des champs de la classe
        
        * self.largeur largeur de l'image
        * self.hauteur hauteur de l'image
        
        * self.tab_gradient image du gradient
        * self.tab_objet image des objets 
        * self.tab_bkg image de l'arrière plan
        #* self.Liste_Pixels_Objet liste de tous les pixels objet détectés

        
        Pour les lignes et les colonnes je me place du point de vue de l'image. 
        Dans le tableau numpy, les coordonnées sont inversées.
        
        """
        
        #self.image=image.image_np_gray
        
        self.largeur = image.largeur
        self.hauteur = image.hauteur
        
        self.image=image
        self.gradient = Gradient(image,gamma)
        self.L_objet = L_Obj 
        self.L_bkg = L_Bkg
        #self.Liste_Pixels_Objet = self.partage()
        ###### zone pour test
        # self.L_objet =[[0,0]]
        # self.L_bkg =[[9,4]]
        
        ###### FIN zone pour test
        


   ## FONCTIONS POUR L'ALGORITHME DE PARTAGE DES EAUX 
    
    def initialise_liste_depuis_marqueurs(self):
        """

        Initialise la liste triée des pixels pour l'algorithme de partage des eaux.

        :param self: Objet Partage_eaux 
        :type self: Objet Partage_eaux
        
        :return: list_triee_pixel liste triée des pixels contenant les pixels de l'objet self.gradient correspondants aux points sélectionnés sur l'image.
        :rtype: objet ListeTrieePixel

        
        """
        # initialisation de la liste triée
        list_triee_pixel=ListeTrieePixel()
        # mise à jour et ajout à la liste triée des pixels background - champ zone à 0
        for coord in self.L_bkg:
            pix=self.gradient.tab[coord[0]][coord[1]]
            pix.maj_zone(0)
            list_triee_pixel.insere_pixel(pix)
        # mise à jour et ajout à la liste triée des pixels objets - champ zone à 1   
        for coord in self.L_objet:
            pix=self.gradient.tab[coord[0]][coord[1]]
            pix.maj_zone(1)
            list_triee_pixel.insere_pixel(pix)
        return list_triee_pixel
    
    
    def partage(self):
        """

        Procédure de mise en oeuvre de l'algorithme de partage des eaux. 
        Mise à jour du champ zone de tous les pixels dans l'objet self.gradient.

        :param self: Objet Partage_eaux 
        :type self: Objet Partage_eaux
        
        :return: None
        :rtype: None

        
        """
        
        # initialisation
        print("initialisation")
        liste_triee_pixel=self.initialise_liste_depuis_marqueurs() 
        
        #liste_triee_pixel.affiche_liste_triee_pixel()
        
        print("Fin initialisation - debut boucle")
        i=0
        while not liste_triee_pixel.est_vide():
            min=liste_triee_pixel.pop_pixel(0)
            liste_voisins_de_min=self.gradient.calcule_liste_pixels_voisins_depuis_pixel(min)
            #print("%d étapes "%i)
            for pixel in liste_voisins_de_min:
                
                if pixel.zone==-1:
                    #print(min.val)
                    #print(min.zone)
                    pixel.maj_zone(min.zone)
                    liste_triee_pixel.insere_pixel(pixel)
                #liste_triee_pixel.affiche_liste_triee_pixel()
            i+=1
        print("%d étapes "%i)
        return(i)
        #self.gradient.affiche()
        
                    
    def extrait_liste_coordonnees_des_pixels_de_zone(self,zone):
        """

        Extrait de l'objet self.gradient la liste des pixels de la zone identifiée par zone.

        :param self: Objet Partage_eaux 
        :type self: Objet Partage_eaux
        
        :param zone: identifiant de la zone à extraire 
        :type zone: int
        
        :return: Liste des coordonnées de tous les pixels de l'objet identifié par l'entier zone
        :rtype: List

        
        """
        liste_coord=[]
        for ligne in self.gradient.tab:
            for pixel in ligne:
                if pixel.zone==zone:
                    liste_coord.append([pixel.x,pixel.y])
        return liste_coord