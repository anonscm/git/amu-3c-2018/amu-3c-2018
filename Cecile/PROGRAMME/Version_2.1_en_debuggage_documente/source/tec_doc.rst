Documentation technique
+++++++++++++++++++++++

.. automodule:: Segmentation_GUI_V2

Segmentation_GUI_V2
===================
 
This comments are not included in the *docstrings*.

.. autoclass:: Application
  :members:
  :private-members:
  :special-members:
  
.. automodule:: Couleur

Couleur
=======
 
This comments are not included in the *docstrings*.

.. autoclass:: Couleur
  :members:
  :private-members:
  :special-members:
  

.. automodule:: Image

Image
=====
 
This comments are not included in the *docstrings*.

.. autoclass:: Image
  :members:
  :private-members:
  :special-members:
  

.. automodule:: Sommet

Sommet
======
 
This comments are not included in the *docstrings*.

.. autoclass:: Sommet
  :members:
  :private-members:
  :special-members:
  
.. automodule:: Arc

Arc
===
 
This comments are not included in the *docstrings*.

.. autoclass:: Arc
  :members:
  :private-members:
  :special-members:

 
.. automodule:: Graphe

Graphe
======
 
This comments are not included in the *docstrings*.

.. autoclass:: Graphe
  :members:
  :private-members:
  :special-members:
  

