#####################################
# Cecile Prouteau
# CTES Projet Math-Info
# Projet segmentation d'image
# 15/01/2019
# Classe Graphe Version 0.1
#####################################

### IMPORTATION DES BIBLIOTHEQUES
import numpy as np
import math

### IMPORTATION DES CLASSES
from Image import Image
from Sommet import Sommet
from Arc import Arc


### DEBUT de la classe Graphe

class Graphe:
    def __init__(self,image,source,puits):
        """image est un tableau numpy d'une image codée sur une couche en niveaux de gris
        source et puits sont des sommets particulier du graphe"""
        
        #self.image=image.image_np_gray
        
        self.largeur = image.largeur
        self.hauteur = image.hauteur
        
        self.nb_sommets = self.largeur*self.hauteur
        
        print(self.largeur)
        print(self.hauteur)
        print(self.nb_sommets)
        
        # gestion de la source et du puits
        self.source = source
        
        self.no_source = self.coordonnees_2_no_sommet(source.x,source.y)
        # NS=self.no_source
        # print("source = %d"%NS)
        # source.affiche_sommet()
        # print(self.no_sommet_2_coordonnees(NS))
        # print()
        
        self.puits = puits

        self.no_puits=self.coordonnees_2_no_sommet(puits.x,puits.y)
        # NP=self.no_puits
        # print("puits = %d"%NP)
        # puits.affiche_sommet()
        # print(self.no_sommet_2_coordonnees(NP))
        # print()
        
        #La liste des sommets du graphe
        #self.liste_sommets = 
        self.image_2_liste_sommets(image)
        #print(len(self.liste_sommets))
        # for s in self.liste_sommets:
        #     s.affiche_sommet()
        # print()
        
        #La liste des listes des arcs partant de chaque sommet graphe
        #self.liste_arcs = 
        self.image_2_liste_arcs(image)
        #print(self.liste_arcs)
        #print(len(self.liste_arcs))
        # for l in self.liste_arcs:
        #     for a in l:
        #         a.affiche_arc()
        #     print()
        
    ## FONCTIONS SUR LES SOMMETS    
    def coordonnees_2_no_sommet(self,x,y):
        """Retourne l'indice du sommet dans la liste à partir de ses coordonnées de point sur l'image"""
        return x+y*(self.largeur)
        
    def sommet_2_no_sommet(self,sommet):
        """Retourne l'indice du sommet dans la liste à partir du sommet"""
        return self.coordonnees_2_no_sommet(sommet.x,sommet.y)
        
    def no_sommet_2_coordonnees(self,n):
        """Retourne les coordonnées du sommet sur l'image à partir de son numéro dans la liste"""
        return (n%self.largeur,n//self.largeur)
    
    def ajoute_sommet(self,x,y):
        """Ajoute le sommet de coordonnées (x,y) sur l'image à liste"""
        sommet=Sommet(x,y)
        self.liste_sommets.append(sommet)
    
    def image_2_liste_sommets(self,img):
        """Crée la liste des sommets à partir de l'image parcourue colonne par colonne - le tableau numpy est parcouru ligne par ligne à cause de l'inversion des coordonnées )"""
        self.liste_sommets=[]
        for y in range(self.hauteur):
            for x in range (self.largeur):
                self.ajoute_sommet(x, y)
                
    def liste_no_sommets_voisins(self,no_sommet):
        """ détermine la liste des numéros de sommets voisins d'un sommet identifié par son numéro
        est utilisée pour la création de la liste des arcs donc ne peut pas se servir de celle-ci"""
        Liste_no_voisins=[]
        sommet=self.liste_sommets[no_sommet]
        if sommet.x > 0:
            voisin_gauche=sommet.voisin('gauche')
            Liste_no_voisins.append(self.sommet_2_no_sommet(voisin_gauche))
        if sommet.x < self.largeur-1 :
            voisin_droit=sommet.voisin('droite')
            Liste_no_voisins.append(self.sommet_2_no_sommet(voisin_droit))
        if sommet.y > 0:
            voisin_haut=sommet.voisin('haut')
            Liste_no_voisins.append(self.sommet_2_no_sommet(voisin_haut))
        if sommet.y < self.hauteur-1 :
            voisin_bas=sommet.voisin('bas')
            Liste_no_voisins.append(self.sommet_2_no_sommet(voisin_bas))
            
        # si liste des arcs connue la fonction pourrait être
        # for a in self.liste_arcs:
        #     Liste_voisins.append(self.coordonnees_2_no_sommet(a.destination.x,a.destination.y))
        #print(Liste_no_voisins)
        return Liste_no_voisins

    ## FONCTIONS SUR LES ARCS

    def cree_arc(self,origine,destination,img): 
        """ Crée un arc allant du sommet origine au sommet destination. son poids est déterminé comme valeur absolue de la différence entre les couleurs des pixels correspondants sur l'image img codées sur 8 bits en niveaux de gris"""
        poids = abs(img.couleur_entier_8bits(destination.y,destination.x) - img.couleur_entier_8bits(origine.y,origine.x))
        return Arc(origine,destination,poids)
    
    # def cree_double_arc(self,sommet_1,sommet_2):
    #     """Crée les arcs dans les deux sens entre le sommet 2 et le sommet 2"""
    #     self.cree_arc(sommet_1,sommet_2)
    #     self.cree_arc(sommet_2,sommet_1)
        
    def ajoute_arc(self,origine,destination,img): #??? 
        """ Ajoute l'arc allant du sommet origine au sommet destination. son poids est déterminé comme valeur absolue de la différence entre les couleurs des pixels correspondants sur l'image img codées sur 8 bits en niveaux de gris à la liste des arcs issus du sommet origine"""
        arc=self.cree_arc(origine,destination,img)
        no_sommet = self.coordonnees_2_no_sommet(origine.x,origine.y)
        self.liste_arcs[no_sommet].append(arc)
        
    def supprime_arc(self,arc):
        no_origine= self.coordonnees_2_no_sommet(arc.origine.x,arc.origine.y)
        for a in self.liste_arcs[no_origine]:
            if arc.destination == a.destination :
                self.liste_arcs[no_origine].remove(a)
    
    # def cree_arcs_de_sommet_vers_voisins(self , no_sommet, img):
    #     """ Crée tous les arc partant d'un sommet identifié par son no vers tous ses voisins
    #     si le sommet n'est pas le puits, chercher les voisins et creer les arcs qui vont su sommet vers ses voisins"""
    #     sommet=self.liste_sommets[no_sommet]
    #     if no_sommet != self.no_puits :
    #         Liste_no_sommets_voisins = self.liste_no_sommets_voisins(no_sommet)
    #         for no_voisin in Liste_no_sommets_voisins:
    #             voisin = self.liste_sommets[no_voisin]
    #             self.ajoute_arc(sommet,voisin,img)
    
    def cree_arcs_de_sommet_vers_voisins(self, no_sommet, img):
        """ Crée tous les arc partant d'un sommet identifié par son no vers tous ses voisins
        si le sommet n'est pas le puits, chercher les voisins et creer les arcs qui vont su sommet vers ses voisins"""
        sommet=self.liste_sommets[no_sommet]
        if no_sommet != self.no_puits :
            Liste_no_sommets_voisins = self.liste_no_sommets_voisins(no_sommet)
            # affichage liste voisins
            # print(no_sommet)
            # sommet.affiche_sommet()
            # print()
            # print(Liste_no_sommets_voisins)
            for no_voisin in Liste_no_sommets_voisins:
                voisin = self.liste_sommets[no_voisin]
                #affiche debug
                # voisin.affiche_sommet()
                self.ajoute_arc(sommet,voisin,img)
                
    def supprime_arcs_vers_sommet(self,no_sommet):
        """supprime tous les arcs arrivant à un sommet repéré par son no_sommet et partant de ses voisins"""
        sommet=self.liste_sommets[no_sommet]
        Liste_no_sommets_voisins = self.liste_no_sommets_voisins(no_sommet)
        for no_voisin in Liste_no_sommets_voisins:
            # recopie de l'adresse de la liste puis modification du contenu commun
            Liste_arcs_du_voisin = self.liste_arcs[no_voisin] 
            for arc in Liste_arcs_du_voisin :
                if  sommet.est_egal(arc.destination) :
                    Liste_arcs_du_voisin.remove(arc)
        
    def image_2_liste_arcs(self,img):
        """ Crée la liste des arcs à partir de l'image 
        La liste des sommets doit être créée au préalable """
        #print("Création des arcs !")
        # initialisation de liste des listes d'arcs partant de chaque sommet
        self.liste_arcs=self.nb_sommets*[None]
        for no_sommet in range(self.nb_sommets):
            self.liste_arcs[no_sommet]=[]
        # Remplissage des listes
        for no_sommet in range (self.nb_sommets):
            self.cree_arcs_de_sommet_vers_voisins(no_sommet, img)   
        #print("Fin de création des arcs !")
        #affichage liste des arcs
        # for i in range(self.nb_sommets):
        #     print(self.liste_arcs[i])        
        """ On supprime les arcs qui arrivent au sommet source et qui ont étés créés inutilement 
        (c'est moins long que de tester si source est le voisin à chaque création d'arc étant donné le nombre
        de pixels dans une image, le coût de la supression est constant)"""
        self.supprime_arcs_vers_sommet(self.no_source)
        
    def arc_de_sommet1_a_sommet2(self,no_sommet1,no_sommet2):
        """ retourne l'arc qui va du sommet1 au sommet2"""
        # affichage debug
        #print(no_sommet1)
        if self.liste_arcs[no_sommet1]!=[]:
            for arc in self.liste_arcs[no_sommet1]:
                if self.sommet_2_no_sommet(arc.destination)==no_sommet2:
                    return(arc)
        else:
            print("Pas d'arc partant de ce sommet!")
            return()
            
    def arc_inverse(self,arc):
        """ retourne l'arc (sommet 2->sommet 1) inverse de arc (sommet 1->sommet 2)
        a ne pas appliquer si arc.destination = sommet d'arrivée ou si arc.origine = sommet de départ puisqu'aucun arc 
        n'arrive au départ ni ne part de l'arrivée """
        no_arc_destination=self.sommet_2_no_sommet(arc.destination)
        for a in self.liste_arcs[no_arc_destination]:
            if a.destination.est_egal(arc.origine):
                # print()
                # arc.affiche_arc()
                # a.affiche_arc()
                # print()
                return a  
                
    def supprime_arcs_poids_zero(self):
        """ supprime tous les arcs de poids zéro du graphe de flot"""
        for no_sommet in range(self.nb_sommets) :
            for arc in self.liste_arcs[no_sommet]:
                if arc.poids==0:
                    self.liste_arcs[no_sommet].remove(arc)   

    ## FONCTIONS POUR LA RECHERCHE DE CHEMIN
    
    def distances_minimales(self,no_sommet_depart):
        """ calcule la distance d'un chemin minimal entre le sommet de départ identifié par no_sommet_départ
        et tous les sommets du graphe. Retourne la liste des distances minimales indexée par les no de sommet correspondant
        Algorithme de Bellman-Ford avec mémorisation des pères"""
        # Création de la liste des distances au sommet de départ
        liste_distance=self.nb_sommets*[math.inf]
        liste_distance[no_sommet_depart]=0
        # Création de la liste des pères par chemin minimal
        liste_peres=self.nb_sommets*[None]
        # Céation de la liste des numéros de sommets à explorer
        Liste_no_sommets_a_explorer=[no_sommet_depart]
        while Liste_no_sommets_a_explorer != []:
            no_sommet_a_explorer = Liste_no_sommets_a_explorer [0]
            #Liste_no_sommets_voisins = self.liste_no_sommets_voisins(no_sommet_a_explorer)
            ##on a la liste des arcs... donc on connais les voisins...
            for arc in self.liste_arcs[no_sommet_a_explorer]:
                distance_sommet_a_explorer=liste_distance[no_sommet_a_explorer]
                no_sommet_voisin= self.sommet_2_no_sommet(arc.destination)
                distance_sommet_voisin=liste_distance[no_sommet_voisin]
                distance_sommet_a_voisin = arc.poids
                if distance_sommet_voisin > distance_sommet_a_explorer + distance_sommet_a_voisin :
                    # maj de la liste des distances
                    liste_distance[no_sommet_voisin] = distance_sommet_a_explorer + distance_sommet_a_voisin
                    # maj de la liste des sommets à explorer
                    Liste_no_sommets_a_explorer.append(no_sommet_voisin)
                    # maj de la liste des peres
                    liste_peres[no_sommet_voisin]=no_sommet_a_explorer
            Liste_no_sommets_a_explorer.remove(no_sommet_a_explorer)
        return [liste_distance, liste_peres ] 
    
    def cherche_chemin_minimal(self,no_sommet_depart,no_sommet_arrivee):
        """ Retourne la liste des sommets formant un chemin minimal du sommet de no_sommet_depart au sommet de  no_sommet_arrivee """
        Liste_distance,Liste_peres=self.distances_minimales(no_sommet_depart)
        chemin=[]
        no_sommet=no_sommet_arrivee
        while no_sommet!=no_sommet_depart:
            chemin.insert(0,no_sommet)
            no_sommet=Liste_peres[no_sommet]
        chemin.insert(0,no_sommet)
        return(chemin)
            
    # def cherche_chemin_minimal(self,no_sommet_depart,no_sommet_arrivee):
    #     Liste_distance=self.distances_minimales(no_sommet_depart)
    #     Liste_sommets=[]
    #     S=no_sommet_arrivee
    #     while S!=no_sommet_depart:
    #         for T in liste des peres de S
    #             if Liste_distance[S]=Liste_distance[T]+d(s,T):
    #                 Liste_sommets.append(T)
    #                 break
    #         S=T
        
    def distances_maximales(self,no_sommet_depart):
        """ calcule la pseudo-distance minimale (somme des inverses des poids des arêtes) d'un chemin maximal non infini
         entre le sommet de départ identifié par no_sommet_départ et tous les sommets du graphe. 
         Retourne la liste des pseudo distances minimales indexée par les no de sommet correspondant
         Algorithme de Bellman-Ford avec mémorisation des pères"""
        # Création de la liste des distances au sommet de départ
        liste_distance=self.nb_sommets*[math.inf]
        liste_distance[no_sommet_depart]=0
        # Création de la liste des pères par chemin minimal
        liste_peres=self.nb_sommets*[None]
        # Céation de la liste des numéros de sommets à explorer
        Liste_no_sommets_a_explorer=[no_sommet_depart]
        while Liste_no_sommets_a_explorer != []:
            no_sommet_a_explorer = Liste_no_sommets_a_explorer [0]
            #Liste_no_sommets_voisins = self.liste_no_sommets_voisins(no_sommet_a_explorer)
            ##on a la liste des arcs... donc on connais les voisins...
            for arc in self.liste_arcs[no_sommet_a_explorer]:
                distance_sommet_a_explorer=liste_distance[no_sommet_a_explorer]
                no_sommet_voisin= self.sommet_2_no_sommet(arc.destination)
                distance_sommet_voisin=liste_distance[no_sommet_voisin]
                distance_sommet_a_voisin = arc.inverse_poids()  # ici les poids des arcs sont remplacés par leurs inverses
                if distance_sommet_voisin > distance_sommet_a_explorer + distance_sommet_a_voisin :
                    # maj de la liste des distances
                    liste_distance[no_sommet_voisin] = distance_sommet_a_explorer + distance_sommet_a_voisin
                    # maj de la liste des sommets à explorer
                    Liste_no_sommets_a_explorer.append(no_sommet_voisin)
                    # maj de la liste des peres
                    liste_peres[no_sommet_voisin]=no_sommet_a_explorer
            Liste_no_sommets_a_explorer.remove(no_sommet_a_explorer)
            #print(liste_peres)
            #print(liste_distance)
        return liste_distance,liste_peres  
    
    def cherche_chemin_maximal_sommets(self,no_sommet_depart,no_sommet_arrivee):
        """ Retourne la liste des sommets formant un chemin maximal non infini du sommet de no_sommet_depart au sommet de  no_sommet_arrivee """
        Liste_distance,Liste_peres=self.distances_maximales(no_sommet_depart)
        chemin=[]
        no_sommet=no_sommet_arrivee
        while no_sommet!=no_sommet_depart:
            chemin.insert(0,no_sommet)
            no_sommet=Liste_peres[no_sommet]
        chemin.insert(0,no_sommet)
        return(chemin)
    
    def cherche_chemin_maximal_arcs(self,no_sommet_depart,no_sommet_arrivee):
        """ Retourne la liste des arcs formant un chemin maximal non infini du sommet de no_sommet_depart au sommet de  no_sommet_arrivee """
        # print("recherche chemin max arc")
        # print(no_sommet_depart)
        # print(no_sommet_arrivee)
        Liste_distance,Liste_peres=self.distances_maximales(no_sommet_depart)
        chemin=[]
        no_sommet=no_sommet_arrivee
        while no_sommet!=no_sommet_depart and no_sommet!=None:
            #print("no sommet = %d"%no_sommet)
            no_pere=Liste_peres[no_sommet]
            #print(no_pere)
            if no_pere!=None:
                arc=self.arc_de_sommet1_a_sommet2(no_pere,no_sommet)
                chemin.insert(0,arc)
            no_sommet=no_pere
            
        # affichage debug
        #print("chemin : ",chemin)
        #print("liste peres : ", Liste_peres)
        #print("longueur liste peres : ", len(Liste_peres))
        return(chemin)
        
    # def cherche_chemin_maximal(self,no_sommet_depart,no_sommet_arrivee):
    #     Liste_distance=self.distances_maximales(no_sommet_depart)
    #     Liste_sommets=[]
    
    ## FONCTIONS POUR L'ALGORITHME DE GRAPH CUT
    def poids_min_dans_chemin(self,chemin):
        """ Retourne le poids minimal des arcs contenus dans chemin formé d'une liste d'arcs """
        min=math.inf
        for arc in chemin:
            if arc.poids<min:
                min=arc.poids
        return min
        
    def ford_fulkerson(self,no_sommet_depart,no_sommet_arrivee):
        """ 
        Algorithme de Ford-Fulkerson """
        chemin=self.cherche_chemin_maximal_arcs(no_sommet_depart,no_sommet_arrivee)
        #i=0
        while chemin!=[]:
            #affichage debug
            #print(i)
            m=self.poids_min_dans_chemin(chemin)
            for arc in chemin:
                no_arc_origine= self.sommet_2_no_sommet(arc.origine)
                no_arc_destination=self.sommet_2_no_sommet(arc.destination)
                arc.poids-=m
                if not (no_arc_origine==no_sommet_depart or no_arc_destination==no_sommet_arrivee):
                    self.arc_inverse(arc).poids+=m
            chemin=self.cherche_chemin_maximal_arcs(no_sommet_depart,no_sommet_arrivee)
            #i+=1
    
    def liste_no_sommets_atteignables_from_liste_distances(self,liste_distances):
        liste_no_sommets_atteignables=[]
        for no_sommet in range(self.nb_sommets):
            if liste_distances[no_sommet]<math.inf:
                liste_no_sommets_atteignables.append(no_sommet)
        return liste_no_sommets_atteignables
        
    def liste_no_sommets_2_liste_coordonnees(self,liste_no_sommets):
        liste_sommets=[]
        for no_sommet in liste_no_sommets:
            x,y=self.no_sommet_2_coordonnees(no_sommet)
            liste_sommets.append([x,y])
        return liste_sommets    

    def graph_cut(self):
        self.ford_fulkerson(self.no_source,self.no_puits)
        self.supprime_arcs_poids_zero()
        liste_distance,Liste_peres=self.distances_minimales(self.no_source)
        #print(liste_distance)
        liste_no_sommets_atteignables=self.liste_no_sommets_atteignables_from_liste_distances(liste_distance)
        liste_sommets_atteignables = self.liste_no_sommets_2_liste_coordonnees(liste_no_sommets_atteignables)
        return liste_sommets_atteignables       
    