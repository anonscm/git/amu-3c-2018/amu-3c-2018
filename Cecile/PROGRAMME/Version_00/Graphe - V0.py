#####################################
# Cecile Prouteau
# CTES Projet Math-Info
# Projet segmentation d'image
# 15/01/2019
# Classe Graphe Version 0.1
#####################################

import numpy as np
from Image import Image
from Sommet import Sommet
from Arc import Arc


### DEBUT de la classe Graphe

class Graphe:
    def __init__(self,image,source,puits):
        """image est un tableau numpy d'une image codée sur une couche en niveaux de gris
        source et puis sont des sommets particulier du graphe"""
        
        #self.image=image.image_np_gray
        
        self.largeur = image.nb_colonne
        
        self.hauteur = image.nb_ligne
        
        self.nb_sommets = image.nb_colonne*image.nb_ligne
        
        #La liste des sommets du graphe
        self.liste_sommets = self.image_2_liste_sommets(image)
        
        #La liste des listes des arcs partant de chaque sommet graphe
        self.liste_arcs = self.image_2_liste_arcs(image)
        
        self.source = source
        
        self.puits = puits
    
    ## FONCTIONS SUR LES SOMMETS    
    def coordonnees_2_no_sommet(self,x,y):
        """Retourne l'indice du sommet dans la liste à partir de ses coordonnées de point sur l'image"""
        return x*(self.largeur-1)+y
        
    def no_sommet_2_coordonnees(self,n):
        """Retourne les coordonnées du sommet sur l'image à partir de son numéro dans la liste"""
        return (n//self.largeur,n%self.largeur)
    
    def ajoute_sommet(self,x,y):
        """Ajoute le sommet de coordonnées (x,y) sur l'image à la liste des sommets"""
        sommet=Sommet(x,y)
        self.liste_sommet.append(sommet)
    
    def image_2_liste_sommets(self,img):
        """Crée la liste des sommets à partir de l'image parcourue colonne par colonne - le tableau numpy est parcouru ligne par ligne à cause de l'inversion des coordonnées )"""
        self.liste_sommets=[]
        for x in range (img.nb_colonne):
            for y in range(img.nb_ligne):
                self.ajoute_sommets(x, y)

    ## FONCTIONS SUR LES ARCS

    def cree_arc(self,origine,destination,img): 
        """ Crée un arc allant du sommet origine au sommet destination. son poids est déterminé comme valeur absolue de la différence entre les couleurs des pixels correspondants sur l'image img codées sur 8 bits en niveaux de gris"""
        poids = abs(img.couleur_entier_8bits(destination.x,destination.y) - img.couleur_entier_8bits(origine.x,origine.y))
        return Arc(origine,destination,poids)
    
    def cree_double_arc(self,sommet_1,sommet_2):
        """Crée les arcs dans les deux sens entre le sommet 2 et le sommet 2"""
        self.cree_arc(sommet_1,sommet_2)
        self.cree_arc(sommet_2,sommet_1)
        
    def ajoute_arc(self,origine,destination,img): #??? 
        """ Ajoute l'arc allant du sommet origine au sommet destination. son poids est déterminé comme valeur absolue de la différence entre les couleurs des pixels correspondants sur l'image img codées sur 8 bits en niveaux de gris à la liste des arcs issus du sommet origine"""
        arc=self.cree_arc(origine,destination,img)
        no_sommet = coordonnees_2_no_sommet(origine.x,origine.y)
        self.liste_arcs[no_sommet].append(arc)
        
    def supprime_arc(self,arc):
        no_origine= self.coordonnees_2_no_sommet(arc.origine.x,arc.origine.y)
        for a in self.liste_arcs[no_origine]:
            if arc.destination == a.destination :
                self.liste_arcs[no_origine].remove(a)
                
    def image_2_liste_arcs(self,img):
        """ Crée la liste des arcs à partir de l'image 
        La liste des sommets doit être créée au préalable """
        self.liste_arcs=[]
        for x in range (img.nb_colonne):
            for y in range(img.nb_ligne):
                """ identifier le numero de sommet """
                no_sommet = coordonnees_2_no_sommet(x,y)
                sommet=self.sommet[no_sommet]
                """ si sommet n'est pas le puits, chercher les voisins et creer les arcs qui vont su sommet vers ses voisins"""
                if not(sommet.est_egal(self.puits)):
                    if x > 0:
                        self.ajoute_arc(sommet,sommet.voisin('gauche'),img)
                    if x < self.largeur :
                        self.ajoute_arc(sommet,sommet.voisin('droit'),img)
                    if y > 0:
                        self.ajoute_arc(sommet,sommet.voisin('haut'),img)
                    if y < self.hauteur :
                        self.ajoute_arc(sommet,sommet.voisin('bas'),img)    
        
        """ On supprime les arcs qui arrivent au sommet source et qui ont étés créés inutilement 
        (c'est moins long que de tester si source est le voisin à chaque création d'arc étant donné le nombre
        de pixels dans une image, le coût de la supression est constant)"""
        no_source = coordonnees_2_no_sommet(source.x,source.y)
        if source.x > 0:
            voisin_gauche = sommet.voisin('gauche')
            no_voisin_gauche = coordonnees_2_no_sommet(voisin_gauche.x,voisin_gauche.y)
            for arc in self.liste_arcs[no_voisin_gauche]:
                if arc.destination == source :
                    self.liste_arcs[no_voisin_gauche].remove(arc)

        if source.x < self.largeur :
            voisin_droit = sommet.voisin('droit')
            no_voisin_droit = coordonnees_2_no_sommet(voisin_droit.x,voisin_droit.y)
            for arc in self.liste_arcs[no_voisin_droit]:
                if arc.destination == source :
                    self.liste_arcs[no_voisin_droit].remove(arc)
            
        if source.y > 0:
            voisin_haut = sommet.voisin('haut')
            no_voisin_haut = coordonnees_2_no_sommet(voisin_haut.x,voisin_haut.y)
            for arc in self.liste_arcs[no_voisin_haut]:
                if arc.destination == source :
                    self.liste_arcs[no_voisin_haut].remove(arc)
            
        if source.y < self.hauteur :
            voisin_bas = sommet.voisin('bas')
            no_voisin_bas = coordonnees_2_no_sommet(voisin_bas.x,voisin_bas.y)
            for arc in self.liste_arcs[no_voisin_bas]:
                if arc.destination == source :
                    self.liste_arcs[no_voisin_bas].remove(arc)
            
    ## FONCTIONS POUR LA RECHERCHE DE CHEMIN
    
    
    ## FONCTION DE GRAPH CUT
    
     
        
                
        