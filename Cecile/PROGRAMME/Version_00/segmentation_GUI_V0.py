# -*- coding: utf-8 -*-

### IPORTATION DES BIBLIOTHEQUES 
import tkinter.filedialog as tkfd
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import tkinter as tk
import numpy as np
import time

### INITIALISATION DES CONSTANTES
## Couleurs disponibles pour le coloriage des zones detectees
# Ce dictionnaire associe des noms de couleur au triplet de couleurs RGB codes chacun sur 8 bits
coul = {'red':(255,0,0), 'blue':(0,0,255), 'green':(0,255,0), 'yellow':(255,255,0), 'cyan':(0,255,255), 'magenta':(255,0,255)}

### INITIALISATION DES VARIABLES GLOBALES
## Initialisation de la liste des points (variable globale) permettant de définir les zones de segmentation
Liste_points=[]

## Initialisation de Lambda (variable globale) parametre de l'algorithme de segmentation (entier entre 0 et 100)
Lambda = 0

## Initialisation de No_algo (variable globale) parametre de choix de l'algorithme de segmentation (entier entre 1 et 2)
No_algo = 1     # 1 pour Graph cuts (choix par defaut) et 2 pour partage des eaux

### DEFINITION DES FONCTIONS
## Fonction convertissant une couleur au format texte provenant de Liste_couleur en un triplet correspondant a son codage RGB
# str->(int,int,int)
# Couleur est un element de List_couleur et chacun des 3 entiers du triplet retourné est compris entre 0 et 255
def couleur_2_RVB(Couleur):
    return coul[Couleur]

## Fonction convertissant une couleur au format texte provenant de Liste_couleur en un triplet correspondant a son codage RGB dans une image PNG
# str->(float,float,float)
# Couleur est un element de List_couleur et chacun des 3 entiers du triplet retourné est compris entre 0 et 1
def couleur_2_RVB_png(Couleur):
    (rouge,vert,bleu)=coul[Couleur]
    return (rouge/256,vert/256,bleu/256)

## Fonction de coloriage dans la couleur Couleur d'une liste de points de l'image dans un tableau NUMPY
# List * str * Numpy Array -> 
# Procedure prend en entree une liste de points au format [x,y], une couleur de la liste List_couleur, 
# un tableau Numpy Array representant une image PNG en RVB.
# la fonction change la couleur des points du tableau T de coordonnées les valeurs se trouvant dans la liste pour Couleur.
# le tableau T est modifie par effets de bord
def colorie_liste_points(L,Couleur,T):
    (rouge,vert,bleu)=couleur_2_RVB_png(Couleur)
    for point in L:     # un point est une liste de 2 cordonnees [x,y]
        i=point[1]
        j=point[0]
        T[i, j, 0] = rouge
        T[i, j, 1] = vert
        T[i, j, 2] = bleu

## Fonction de sélection de pixel a la souris
# event ->
# prend en entree un evenement (qui sera le click droit de la souris), collecte les coordonnees X,Y du point clique
# ajoute [X,Y] à la liste Liste_point (variable globale)
def select_pixel(event):
    X=event.x
    Y=event.y
    Liste_points.append([X,Y])

### FENETRE TKINTER -> en faire une classe ...

## Creation de la fenetre
fenetre = tk.Tk()

## Selection et ouverture du fichier Image
# Selection du fichier image a traiter au format PNG niveau de gris a l'aide d'une fenetre de selection
filepath = tkfd.askopenfilename(title="Ouvrir une image",filetypes=[('png files','.png'),('all files','.*')])

# Ouverture du fichier image a traiter au format PNG niveau de gris et stockage dans la variable photo
photo = tk.PhotoImage(file=filepath)

## Label de la fenetre principale 'fenetre'
label = tk.Label(fenetre, text="SEGMENTATION D'IMAGE",bg="ivory",font=("Helvetica", 28))
label.pack()

## Organisation des Frames
# Frame1 dans fenetre
Frame1 = tk.Frame(fenetre, borderwidth=0, relief=tk.GROOVE)
Frame1.pack(side=tk.LEFT, padx=10, pady=10)

# Frame2 dans fenetre
Frame2 = tk.Frame(fenetre, borderwidth=0, relief=tk.GROOVE)
Frame2.pack(side=tk.BOTTOM, padx=5, pady=5)

# Frame3 dans Frame2
Frame3 = tk.Frame(Frame2, borderwidth=0, relief=tk.GROOVE)
Frame3.pack(side=tk.TOP, padx=5, pady=5)

# Frame 4 dans Frame2
Frame4 = tk.Frame(Frame2, bg="white", borderwidth=0, relief=tk.GROOVE)
Frame4.pack(side=tk.BOTTOM, padx=40, pady=40)

# Frame 5 dans Frame 4
Frame5 = tk.Frame(Frame2, borderwidth=0, relief=tk.GROOVE)
Frame5.pack(side=tk.TOP, padx=5, pady=5)

# Frame 6 dans Frame 4
Frame6 = tk.Frame(Frame4, borderwidth=0, relief=tk.GROOVE)
Frame6.pack(side=tk.BOTTOM, padx=5, pady=5)

## Ajout de labels pour les Frames
tk.Label(Frame1, text="CHOIX DES PARAMETRES").pack(padx=10, pady=10)
tk.Label(Frame3, text="LISTE DES POINTS SELECTIONNES").pack(padx=10, pady=10)
tk.Label(Frame6, text="Cliquez sur l'image avec le bouton droit de la souris pour sélectionner les points définissant les zones (fond en premier)").pack(padx=10, pady=10)

## Ajouts des Widgets

# Parametre de choix d'algorithme
# Mise à jour de l'echelle à l'aide du curseur
# int ->
# affecte la valeur x a la variable globale No_algo
def update_No_algo():
    global No_algo
    No_algo=value.get()
    
# radiobutton
value = tk.StringVar() 
bouton1 = tk.Radiobutton(Frame1, text="Graph Cuts", variable=value, value=1, command=update_No_algo)
bouton2 = tk.Radiobutton(Frame1, text="Partage des eaux", variable=value, value=2, command=update_No_algo)
bouton1.pack()
bouton2.pack()

# Parametre Lambda determine a l'aide d'un curseur

# Mise à jour du parametre a l'aide du curseur
# int ->
# affecte la valeur x a la variable globale Lambda
def updateLambda(x):
    global Lambda
    Lambda=x
    
valeur_scale = tk.DoubleVar()
scale = tk.Scale(Frame1, orient='horizontal', variable=valeur_scale, label='Lambda', command=updateLambda)
scale.pack()

# Affichage de la liste des points selectionnes dans la fenetre
sv = tk.StringVar()
Frame4 = tk.LabelFrame(Frame3, text="Liste des points sélectionnés", padx=20, pady=20)
Frame4.pack(fill="both", expand="yes")
sv.set(str(Liste_points))
tk.Label(Frame3, textvariable=sv).pack()

# Fonction d'initialisation de la liste des points
# procedure sans argument qui remet la variable globale Listepoints a vide 
# et qui met a jour son affichage dans la fenetre graphique
def Liste_nulle():
    global Liste_points
    Liste_points=[]
    sv.set(str(Liste_points))
    
# bouton de reinitialisation de la liste des points a vide 
bouton_init = tk.Button(Frame5, text="Réinitialisation de la liste des points", command = Liste_nulle )
bouton_init.pack()    

# Gestion de la selection des points
# event->
# quand event se produit (se sera click droit de la souris sur l'image), 
# la liste Liste_points est augmentee des coordonnees du point clique et l'affichage de cette liste est mise a jour 
def select_pixel_maj(event):
    select_pixel(event)
    sv.set(str(Liste_points))

# en cas de clik droit de la souris, appel de la fonction select_pixel_maj pour mise a jour de la liste et de l'affichage
fenetre.bind("<Button-3>", select_pixel_maj )

# Affichage du fichier image
canvas = tk.Canvas(Frame6, width=photo.width(), height=photo.height(), bg="gray")
canvas.create_image(0, 0, anchor=tk.NW, image=photo)
canvas.pack()

# Bouton de sortie
bouton=tk.Button(Frame6, text = "Valider et continuer", command = fenetre.quit)
bouton.pack()

fenetre.mainloop()
### FIN DE LA FENETRE TKINTER

### IMPORT - EXPORT PNG en niveaux de gris <-> NUMPY ARRAY et AFFICHAGE 
# fonction de chargement dans la variable img de l'image png accessible avec le chemin filepath
# str -> Numpy Array
# importe l'image png à l'adresse FilePath
def import_image_from_file(FilePath):
    return mpimg.imread(FilePath)

# fonction de conversion d'un tableau numpy image importe du fichier image en niveaux de gris PNG en un tableau numpy png RGB
# image -> numpy Array 
# les donnees de l'image importee en niveaux de gris PNG sont recopiees dans un numpy Array image RGB PNG qui est retourne 
def gray_2_RGB(image):
    # Recuperation des dimentions de l'image
    nb_ligne = image.shape[0]
    nb_colonne = image.shape[1]
    print(nb_ligne,nb_colonne)
    
    # Initialisation des tableau numpy a la taille de l'image et pleins de zeros
    #gray = np.zeros((nb_ligne,nb_colonne))
    gray_RGB = np.zeros((nb_ligne,nb_colonne,3))
    
    # Importation des donnees de l'image dans les tableaux numpy, en niveau de gris d'une part (pour le traitement)
    # en couleur RVB d'autre part (pour l'affichage et l'export)
    for i in range (nb_ligne):
        for j in range (nb_colonne):
            gray_RGB[i, j, 0] = image[i, j]
            gray_RGB[i, j, 1] = image[i, j]
            gray_RGB[i, j, 2] = image[i, j]
    return gray_RGB

# Affichage de l'image transformee 
# numpy Array ->
# procedure affichant l'image contenu dans le numpy Array image
def affiche_image(image):       
    plt.axis('off')
    plt.imshow(image)
    plt.show()

# Export de l'image  
# numpy Array * str ->
# procedure exportant le tableau numpy image à l'adresse FilePath modifiee pour tenir compte du changement de format
def export_image_2_file(image,FilePath):
    filepath_gray_RGB=FilePath.rstrip('.png')+"_gray_RGB.png"
    mpimg.imsave(filepath_gray_RGB,image)

## test des fonctions
# importe l'image png à l'adresse filepath
img = import_image_from_file(filepath)

# converti le tableau image de niveaux de gris en tableau image RGB
img_RGB=gray_2_RGB(img)

# Coloriage d'une liste de points sélectionnés aux clavier sur le tableau de l'image a exporter 
# permettra de colorier une zone segmentee ulterieurement
colorie_liste_points(Liste_points,'red',img_RGB)

# Affichage de l'image transformee 
affiche_image(img_RGB)

# Export de l'image transformee
export_image_2_file(img_RGB,filepath)

# Affichage de la liste_des points, de Lamdba et de No_algo
print(Liste_points)
print(Lambda)
print(No_algo)
