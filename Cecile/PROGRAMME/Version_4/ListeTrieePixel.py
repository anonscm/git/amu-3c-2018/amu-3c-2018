#####################################
# Cecile Prouteau
# CTES Projet Math-Info
# Projet segmentation d'image
# 15/03/2019
# Classe ListeTrieePixel Version 3.0
#####################################
import math

from Pixel import Pixel

### DEBUT de la classe ListeTrieePixel

class Pixel:
    """

    **LES METHODES**
     
    """
    
    def __init__(self):
        """
        Constructeur d'un objet ListeTrieePixel (Liste initialisée à vide)

        
        :param self: Objet ListeTrieePixel
        :type self: ListeTrieePixel
        
        :return: Objet ListeTrieePixel
        :rtype: ListeTrieePixel
        
        Liste des champs de la classe
        
        * self.tab liste triée de Pixels 


        """
        
        self.tab=[]
    
    
    
##########################################    
        
'''Fonctions sur les tableau tries de pixels'''
#Fonction permettant de creer une nouvelle liste de pixels tries
def listetriee_pixel_creer():
    return ListeTrieePixel()

##Fonction permettant d'inserer un nouveau pixel dans la liste en specifiant sa coordonnee x, y, sa valeur et un marqueur associe au pixel
def listetriee_pixel_inserer_pixel(tabpixel, x, y, valeur, marqueur):
    #On cherche ou placer, dans la liste triee, le nouveau pixel
    pos = numpy.searchsorted(tabpixel.tab_valpixel, valeur, 'right')
    #Et on insere le pixel a cet endroit (quatre listes triees, quatre elements a inserer)
    tabpixel.tab_valpixel = numpy.insert(tabpixel.tab_valpixel, pos, valeur)
    tabpixel.tab_pixelx = numpy.insert(tabpixel.tab_pixelx, pos, x)
    tabpixel.tab_pixely = numpy.insert(tabpixel.tab_pixely, pos, y)
    tabpixel.tab_marqueur = numpy.insert(tabpixel.tab_marqueur, pos, marqueur)

#Fonction permettant de sortir de la liste le premier element (la valeur de pixel la plus petite)
def listetriee_pixel_sortir_pluspetit_element(tabpixel):
    #On recupere les valeur du premier element de chacun des listes
    val = tabpixel.tab_valpixel[0]
    posx = tabpixel.tab_pixelx[0]
    posy = tabpixel.tab_pixely[0]
    marqueur = tabpixel.tab_marqueur[0]
    #Et on supprime ces elements de nos listes
    tabpixel.tab_valpixel = numpy.delete(tabpixel.tab_valpixel, 0)
    tabpixel.tab_pixelx = numpy.delete(tabpixel.tab_pixelx, 0)
    tabpixel.tab_pixely = numpy.delete(tabpixel.tab_pixely, 0)
    tabpixel.tab_marqueur = numpy.delete(tabpixel.tab_marqueur, 0)
    return posx, posy, val, marqueur

#Fonction permettant de connaitre le nombre d'elements d'une liste triee de pixels
def listetriee_pixel_taille(tabpixel):
    return len(tabpixel.tab_valpixel)

#Fonction permettant d'afficher les elements de la liste
def listetriee_pixel_afficher(tabpixel):
    for i in range(0, len(tabpixel.tab_pixelx)):
        print( '('+str(tabpixel.tab_pixelx[i])+', '+str(tabpixel.tab_pixely[i])+', '+str(tabpixel.tab_valpixel[i])+', '+str(tabpixel.tab_marqueur[i])+')')