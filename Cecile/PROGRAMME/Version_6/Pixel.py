#####################################
# Cecile Prouteau
# CTES Projet Math-Info
# Projet segmentation d'image
# 15/03/2019
# Classe Pixel Version 3.0
#####################################
import math

### DEBUT de la classe Pixel
### contient la recherche des voisins , on se place du point de vue des coordonnées sur l'image
class Pixel:
    
    """
    
    **AUTRES CLASSES IMPORTEES**
    
    * math
    
    **LES METHODES**
     
    
    """
    
    def __init__(self,x,y,val):
        
        """
        Constructeur d'un objet Pixel
        
        Construit un Pixel à partir des coordonnées d'un pixel sur une image
        
        :param self: Objet Pixel
        :type self: Pixel
        
        :param x: abscisse du point sur l'image
        :type x: int  
        
        
        :param y: ordonnée du point  
        :type y: int      
        
        :param val: valeur du point  
        :type val: int 
        
        :return: Objet Pixel
        :rtype: Pixel
        
        Liste des champs de la classe
        
        * self.x abscisse 
        * self.y ordonnée
        * self.val valeur du pixel self
        * self.zone indique dans quelle zone se trouve le pixel (vaut -1 tant que le pixel n'est pas affecté à une zone)
        * self.liste_coord_voisins contiendra la liste des coordonnées des voisins du pixel sur l'image ; initialisé à []
        
        Pour les lignes et les colonnes je me place du point de vue de l'image. 
        Dans le tableau numpy, les coordonnées sont inversées.
        
        """
        
        self.x=x
        self.y=y
        self.val=val
        self.zone=-1
        self.liste_coord_voisins=[]
        
    def maj_val(self,val):
        """
        Procedure qui met à jour le champ self.val avec la valeur val
        
        :param self: Objet Sommet 
        :type self: Sommet
        
        :param val: valeur entière pour mise à jour du numéro de sommet
        :type val: int  
        
        :return: None 
        :rtype: None object
        
        """
        self.val=val
        
    def maj_zone(self,zone):
        """
        Procedure qui met à jour le champ self.zone avec la valeur zone
        
        :param self: Objet Sommet 
        :type self: Sommet
        
        :param zone: valeur entière pour mise à jour du numéro de la zone d'appartenance au sommet
        :type zone: int  
        
        :return: None 
        :rtype: None object
        
        """
        self.zone=zone
    
    def maj_liste_coord_voisins(self,liste):
        """
        Procedure qui ajoute à la liste self.liste_coord_voisins avec les éléments de liste
        
        :param self: Objet Sommet 
        :type self: Sommet
        
        :param liste: liste de coordonnées à ajouter à self.liste_coord_voisins
        :type liste: List 
        
        :return: None 
        :rtype: None object
        
        """
        for  element in liste:
              self.liste_coord_voisins.append(element)

             
    def est_superieur(self,pixel):
        """
        
        :param self: Objet Pixel 
        :type self: Pixel
        
        :param sommet: Objet Pixel
        :type sommet: Pixel  
        
        :return: True si la valeur de self est strictement plus grande que la valeur de pixel et False sinon
        :rtype: bool
        
        """ 
        
        return  self.val > pixel.val  
        
    def est_inferieur(self,pixel):
        """
        
        :param self: Objet Pixel 
        :type self: Pixel
        
        :param sommet: Objet Pixel
        :type sommet: Pixel  
        
        :return: True si la valeur de self est strictement plus petite que la valeur de pixel et False sinon
        :rtype: bool
        
        """ 
        
        return  self.val < pixel.val 
            
    def affiche_pixel(self):
        """
        Procédure qui affiche les coordonnées de self
        
        :param self: Objet Sommet 
        :type self: Sommet
        
        :return: None 
        :rtype: None object
        
        """
        print("(",self.x,";",self.y,") ;",self.val,";",self.zone)        