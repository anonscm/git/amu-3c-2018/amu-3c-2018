#####################################
# Cecile Prouteau
# CTES Projet Math-Info
# Projet segmentation d'image
# 15/03/2019
# Classe Partage des eaux Version 0.1
#####################################

### IMPORTATION DES BIBLIOTHEQUES
import numpy as np
import math

### IMPORTATION DES CLASSES
from Image import Image
from Pixel import Pixel


### DEBUT de la classe Partage_eaux

class Partage_eaux:
    def __init__(self,image):
        """

        Constructeur d'un objet Graphe à partir d'une image d'un pixel servant de 

        :param self: Objet Graphe 
        :type self: Graphe
        
        :param image: objet Image est un tableau numpy d'une image codée sur une couche en niveaux de gris
        :type image: Image 
        
        :return: Objet Partage_eaux
        :rtype: Partage_eaux
        
        Liste des champs de la classe
        
        * self.largeur largeur de l'image
        * self.hauteur hauteur de l'image
        
        * self.tab_gradient image du gradient
        * self.tab_objet image des objets 
        * self.tab_bkg image de l'arrière plan
        * self.LPE image résultat
        
        Pour les lignes et les colonnes je me place du point de vue de l'image. 
        Dans le tableau numpy, les coordonnées sont inversées.
        
        """
        
        #self.image=image.image_np_gray
        
        self.largeur = image.largeur
        self.hauteur = image.hauteur
        
        self.tab_gradient = self.cree_image(image.largeur,image.hauteur)
        self.tab_objet = self.cree_image(image.largeur,image.hauteur)
        self.tab_bkg = self.cree_image(image.largeur,image.hauteur)
        self.LPE = self.cree_image(image.largeur,image.hauteur)

    ## FONCTIONS sur les images 
    
    def cree_image(self,L,H):
        img=L*[None]
        for i in range(L):
            img[i]=H*[None]
    
    ## FONCTIONS les pixels 
        
        
    ## FONCTIONS de création de l'image gradient   
    def calcule_gradient_image(img):
        """
        Procédure qui modifie l'Objet Partage_eaux self en calculant le gradient à partir des pixels de l'image img
        
        :param self: Objet Partage_eaux
        :type self: Partage_eaux
        
        :param img: Image
        :type image: objet Image  
        
        :return: None 
        :rtype: None object
        
        """
        
        gradient = cree_image(larg, haut)
    
        #Creation de l'image de gradient
        for i in range(0,larg):
            for j in range(0,haut):
                #On recherche, pour chaque pixel, dans un rayon donne, la plus grande et la plus petite valeur
                # A COMPLETER
                a, b = image_recherche_valeur_extremale(img, i, j, 1)
                #On ecrit la difference entre ces deux valeur dans une image de sortie
                #A COMPLETER
                0 #A EFFACER
                image_ecrire_pixel(grad, i, j, b-a)
    
    def image_2_liste_sommets(self,img):
        """
        Procédure qui modifie l'Objet Graphe self en créant la liste des sommets à partir des pixels de l'image img
        
        :param self: Objet Graphe 
        :type self: Graphe
        
        :param img: Image
        :type image: objet Image  
        
        :return: None 
        :rtype: None object
        
        """
        
        # l'image est parcourue colonne par colonne - le tableau numpy est parcouru ligne par ligne à cause de l'inversion des coordonnées
        
        self.liste_sommets=[]
        for y in range(self.hauteur):
            for x in range (self.largeur):
                id = self.coordonnees_2_id_sommet(x,y)
                c=img.couleur_entier_8bits(y,x)
                self.ajoute_sommet(x, y,id,c)
    


                
    def cree_liste_id_sommets_voisins(self,id_sommet):
        """
        
        :param self: Objet Graphe 
        :type self: Graphe
        
        :param id_sommet: indice d'un sommet dans la liste des sommets self.liste_sommets.
        :type id_sommet: int  
        
        :return: la liste des numéros de sommets voisins d'un sommet identifié par son indice id_sommet dans la liste des sommets self.liste_sommets dans l'image.
        :rtype: List
        
        """
        
        # Sera utilisée pour la création de la liste des arcs donc ne peut pas se servir de celle-ci ici
        
        Liste_id_voisins=[]
        sommet=self.liste_sommets[id_sommet]
        # print(sommet.y)
        # print(self.hauteur)
        if sommet.y!=None and 0 <= sommet.y < self.hauteur :
            if sommet.x > 0 :
                # x_voisin_gauche,y_voisin_gauche=sommet.coord_voisin('gauche')
                # id_voisin_gauche=self.coordonnees_2_id_sommet(x_voisin_gauche,y_voisin_gauche)
                # Liste_id_voisins.append(id_voisin_gauche)
                # 
                # self.ajoute_voisin_a_liste_voisin(sommet , Liste_id_voisins , 'gauche')
                
                Liste_id_voisins.append(id_sommet-1)
                
                
            if sommet.x < self.largeur-1 :
                # x_voisin_droite,y_voisin_droite=sommet.coord_voisin('droite')
                # id_voisin_droite=self.coordonnees_2_id_sommet(x_voisin_droite,y_voisin_droite)
                # Liste_id_voisins.append(id_voisin_droite)
                Liste_id_voisins.append(id_sommet+1)
        
        if sommet.x!=None and 0<=sommet.x<self.largeur :    
            if sommet.y > 0:
                # x_voisin_haut,y_voisin_haut=sommet.coord_voisin('haut')
                # id_voisin_haut=self.coordonnees_2_id_sommet(x_voisin_haut,y_voisin_haut)
                # Liste_id_voisins.append(id_voisin_haut)
                Liste_id_voisins.append(id_sommet-self.largeur)
                
            if sommet.y < self.hauteur-1 :
                # x_voisin_bas,y_voisin_bas=sommet.coord_voisin('bas')
                # id_voisin_bas=self.coordonnees_2_id_sommet(x_voisin_bas,y_voisin_bas)
                # Liste_id_voisins.append(id_voisin_bas)
                Liste_id_voisins.append(id_sommet+self.largeur)    
        return Liste_id_voisins
        

        


    ## FONCTIONS SUR LES PIXELS


    


        
   ## FONCTIONS POUR L'ALGORITHME DE PARTAGE DES EAUX 
   I image de gradient ; M1 image des objets et M2 image de l'arrière plan, LEP image résultat
    # initialisation
    L=[]
    for pixel x in M1:
        L.append(x)
        LPE(x)=1
    for pixel x in M2:
        L.append(x)
        LPE(x)=2 
        
    trier L selon 
    
    while L!=[]:
        choisir x de L tel que I(x) est minimal
        supprimer x de L
        pour tout y voisin de x tel que LPE(y)=0:
            LPE(y)=LPE(x)
            L.append(y)
        

def initialiser_liste_avec_marqueur(img_marqueur_obj, img_marqueur_fond, grad):
    #Construction d'une liste de pixel tries
    liste = listetriee_pixel_creer()

    larg = image_largeur(grad)
    haut = image_hauteur(grad)

    #Et creation de l'image resultat
    lpe = image_creer(larg, haut)

    #Lecture des images de marqueurs pour initialiser la liste des pixels qui doivent diffuser leur valeur
    for i in range(0,larg):
        for j in range(0,haut):
            #Si le pixel i,j est dans l'image des marqueurs de l'objet,
            #on le rajoute a la liste des pixels : sa valeur sera sa valeur dans l'image de gradient
            #et le marqueur associe sera 2
            if image_lire_pixel(img_marqueur_obj, i, j) > 0 :
                v = image_lire_pixel(grad, i, j)
                listetriee_pixel_inserer_pixel(liste, i, j, v, 2)
                image_ecrire_pixel(lpe, i, j, 2) #On ecrit son marqueur sur l'image resultat
            #Si le pixel i,j est dans l'image des marqueurs du fond,
            #on le rajoute a la liste des pixels : sa valeur sera sa valeur dans l'image de gradient
            #et le marqueur associe sera 1
            elif image_lire_pixel(img_marqueur_fond, i, j) > 0 :
                #A COMPLETER
                0 #A EFFACER

    return liste, lpe


def algorithme_lpe(liste, lpe, grad):
    larg = image_largeur(grad)
    haut = image_hauteur(grad)

    #Algorithme de LPE
    #Tant qu'il existe des pixels dont il faut diffuser la valeur
    while listetriee_pixel_taille(liste) > 0:
        #Recuperer toutes les informations sur le pixel avec la valeur la plus petite : on l'appelle p
        #A COMPLETER
        #Pour chacun des quatre voisins possible de ce pixel
        for voisin in ['haut', 'bas', 'droite', 'gauche']:
            #Recuperer les coordonnees du voisin dans l'image
            vx, vy = image_coordonnes_voisins(lpe, x, y, voisin) #Attention au x et y ici...
            if vx is not None: #Si le voisin existe
                if image_lire_pixel(lpe, vx, vy) == 0: #et qu'il n'a pas de marqueur associe dans l'image resultat
                    #On lui attribue un marqueur (le meme que celui du pixel p)
                    # A COMPLETER
                    #lire sa valeur dans l'image gradient
                    # A COMPLETER
                    #et le placer dans la liste triee des pixels a diffuser
                    # A COMPLETER
                    0 #A EFFACER
    #Fin de l'algorithme LPE

    #On parcourt l'image resultat, pour que chaque pixel avec un marqueur a 2 devienne blanc,
    #et chaque pixel avec un marqueur a 1 devienne noir
    for i in range(0,larg):
        for j in range(0,haut):
           if image_lire_pixel(lpe, i, j) ==2 :
               # A COMPLETER
               0 #A EFFACER
           else:
               # A COMPLETER
               0 #A EFFACER
    return lpe   