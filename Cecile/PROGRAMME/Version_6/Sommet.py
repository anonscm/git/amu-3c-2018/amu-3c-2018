#####################################
# Cecile Prouteau
# CTES Projet Math-Info
# Projet segmentation d'image
# 15/01/2019
# Classe Sommet Version 3.0
#####################################
import math

### DEBUT de la classe Sommet
### contient la recherche des voisins , on se place du point de vue des coordonnées sur l'image
class Sommet:
    """
    
    **AUTRES CLASSES IMPORTEES**
    
    * math
   
    
    **LES METHODES**
     
    
    """
    
    def __init__(self,x,y,id,v):
        """
        Constructeur d'un objet Sommet
        
        Construit un sommet à partir des coordonnées d'un pixel sur une image
        
        :param self: Objet Sommet 
        :type self: Sommet
        
        :param x: abscisse du point sur l'image
        :type FilePath: int  
        
        
        :param y: ordonnée du point  
        :type FilePath: int      
        
        :return: Objet Sommet
        :rtype: Sommet
        
        Liste des champs de la classe
        
        * self.x abscisse 
        * self.y ordonnée
        * self.v valeur du sommet self
        * self.id numéro d'identification unique du sommet dans la liste des sommets.
        * self.dist distance à la source. Intialisée à math.inf
        * self.id_pere numéro de sommet du père du sommet dans chemin de parcours du graphe. Intialisé à None
        * self.pere id du sommet père du sommet dans chemin de parcours du graphe. Intialisé à None
        * self.etat état donné au sommet lors du parcours du graphe. Intialisé à None pour non visité, passera à 0 pour visité et à 1 quand traité.
        * self.zone numéro de zone du pixel 
        
        Pour les lignes et les colonnes je me place du point de vue de l'image. 
        Dans le tableau numpy, les coordonnées sont inversées.
        
        """
        
        self.x=x
        self.y=y
        self.v=v
        self.id=id
        self.dist=math.inf
        self.id_pere=-1
        self.pere=None
        self.etat=-1
        self.zone=None
        
    def maj_id(self,val):
        """
        Procedure qui met à jour l'attribu self.id avec la valeur val
        
        :param self: Objet Sommet 
        :type self: Sommet
        
        :param val: entier pour mise à jour du numéro de sommet
        :type val: int  
        
        :return: None 
        :rtype: None object
        
        """
        self.id=val
        
    def maj_dist(self,val):
        """
        Procedure qui met à jour l'attribu self.dist avec la valeur val
        
        :param self: Objet Sommet 
        :type self: Sommet
        
        :param val: entier à pour mise à jour de la distance
        :type val: int  
        
        :return: None 
        :rtype: None object
        
        """
        self.dist=val
        
    def maj_etat(self,val):
        """
        Procedure qui met à jour l'attribu self.etat avec la valeur val
        
        :param self: Objet Sommet 
        :type self: Sommet
        
        :param val: entier à pour mise à jour de l'état du sommet (-1 = non visité, 0 = visité, 1 = traité)
        :type val: int  
        
        :return: None 
        :rtype: None object
        
        """
        self.etat=val
        
    def maj_id_pere(self,id_pere):
        """
        Procedure qui met à jour l'attribu self.id_pere avec la valeur id_pere
        
        :param self: Objet Sommet 
        :type self: Sommet
        
        :param id_pere: entier pour mise à jour du numéro de sommet du pere self.id_pere
        :type id_pere: int  
        
        :return: None 
        :rtype: None object
        
        """
        self.pere=id_pere
        
    def maj_pere(self,pere):
        """
        Procedure qui met à jour l'attribu self.pere avec la valeur pere
        
        :param self: Objet Sommet 
        :type self: Sommet
        
        :param pere: Objet Sommet pour mise à jour du sommet du pere self.pere
        :type pere: Sommet  
        
        :return: None 
        :rtype: None object
        
        """
        self.pere=id_pere
        
    def maj_zone(self,val):
        """
        Procedure qui met à jour l'attribu self.id avec la valeur val
        
        :param self: Objet Sommet 
        :type self: Sommet
        
        :param val: entier à pour mise à jour du numéro de sommet
        :type val: int  
        
        :return: None 
        :rtype: None object
        
        """
        self.zone=val
    
    def coord_voisin(self,direction):
        
        """
        :param self: Objet Sommet 
        :type self: Sommet
        
        :param direction: chaine de caractère parmi 'droite', 'gauche', 'haut', 'bas'
        :type direction: str  
        
        :return: un couple de coordonnée entières (x', y') du pixel voisin de celui de coordonnée (x,y) dans la direction indiquée par la variable direction. 
        :rtype: int*int
        
        """
        
        if direction=='droite':
            return (self.x+1,self.y)
        elif direction=='gauche':
            return (self.x-1,self.y)
        elif direction=='haut':
            return (self.x,self.y-1)
        elif direction=='bas':
            return (self.x,self.y+1)
        else :
            print("Erreur de paramètre dans Sommet.coord_voisin!")  
            exit(1) 
             
    def est_egal(self,sommet):
        """
        
        :param self: Objet Sommet 
        :type self: Sommet
        
        :param sommet: Objet Sommet
        :type sommet: Sommet  
        
        :return: True si self et sommet ont les même coordonnées et False sinon
        :rtype: bool
        """ 
        return  (self.id==sommet.id)   
            
    def affiche_sommet(self):
        """
        Affiche les coordonnées de self
        
        :param self: Objet Sommet 
        :type self: Sommet
        
        :return: None 
        :rtype: None object
        
        """
        print("(%d,%d)"%(self.x,self.y),end=";")        