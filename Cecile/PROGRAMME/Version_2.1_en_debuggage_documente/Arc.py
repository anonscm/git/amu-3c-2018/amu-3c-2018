#####################################
# Cecile Prouteau
# CTES Projet Math-Info
# Projet segmentation d'image
# 15/01/2019
# Classe Arc Version 0.1
#####################################

### IMPORTATION DES BIBLIOTHEQUES
import math

### IMPORTATION DES CLASESS
from Sommet import Sommet

### DEBUT de la classe Arc

class Arc:
    def __init__(self,origine,destination,poids):
        """Origine et destination sont des sommets, poids est un nombre entier"""
        self.origine=origine
        self.destination=destination
        self.poids=poids
        """distinguer capacité et flow"""
        self.capacite=poids
        self.flow=0
    
    def origine(self):
        """retourne l'origine de l'arc"""
        return self.origine 
        
    def destination(self):
        """retourne la destination de l'arc"""
        return self.destination 
        
    def poids(self):
        """retourne le poids de l'arc"""
        return self.poids
    
    def inverse_poids(self):
        if self.poids == 0 :
            return math.inf
        else :
            return 1/self.poids
    
    def est_identique(self,arc):
        """retourne vrai si self et arc sont égaux""" 
        return (self.origine.est_egal(arc.origine) and self.destination.est_egal(arc.destination) and self.poids==arc.poids)
    
    def augmente_poids(self,valeur):
        """self*int->int
        Augmente le poids de l'arc de valeur"""
        self.poids+=valeur
    
    def diminue_poids(self,valeur):
        """self*int->int
        Diminue le poids de l'arc de valeur"""
        self.poids-=valeur
    
    def affiche_arc(self):
        print("origine(%d,%d) ; destination(%d,%d) ; poids(%d)"%(self.origine.x,self.origine.y,self.destination.x,self.destination.y,self.poids),end=";")