#####################################
# Cecile Prouteau
# CTES Projet Math-Info
# Projet segmentation d'image
# 15/01/2019
# Classe Application Version 1.4
#####################################

### IMPORTATION DES BIBLIOTHEQUES 
import tkinter.filedialog as tkfd
import tkinter as tk
import numpy as np
# import matplotlib.pyplot as plt
# import matplotlib.image as mpimg
# import time

### IMPORTATION DES CLASSES
from Image import Image
from Graphe import Graphe
from Sommet import Sommet

### Application d'affichage principale ( FENETRE TKINTER )

class Application(object):
    def __init__(self):
        """Constructeur de la fenêtre principale"""
        ## Creation de la fenetre
        self.fenetre = tk.Tk()
        self.fenetre.title("SEGMENTATION D'IMAGE")
        
        ## Initialisation des variables de classe

        # Selection du fichier image a traiter au format PNG niveau de gris a l'aide d'une fenetre de selection
        self.filepath = tkfd.askopenfilename(title="Ouvrir une image",filetypes=[('png files','.png'),('all files','.*')])
        
        # Ouverture du fichier image a traiter au format PNG niveau de gris et stockage dans la variable photo
        self.photo = tk.PhotoImage(file=self.filepath)
        
        # Creation d'une instance de Image à partir du fichier
        self.img=Image(self.filepath)
        
        # Lite des points de repérage des zones
        self.Liste_points=[]
        
        ## Label de la fenetre principale 'fenetre'
        # self.label = tk.Label(self.fenetre, text="SEGMENTATION D'IMAGE",bg="ivory",font=("Helvetica", 28))
        # self.label.pack()
        
        ## Organisation des Frames
        # Frame1 dans fenetre
        self.Frame1 = tk.Frame(self.fenetre, borderwidth=0, relief=tk.GROOVE)
        self.Frame1.pack(side=tk.LEFT, padx=10, pady=10)
        
        # Frame2 dans fenetre
        self.Frame2 = tk.Frame(self.fenetre, borderwidth=0, relief=tk.GROOVE)
        self.Frame2.pack(side=tk.BOTTOM, padx=5, pady=5)
        
        # Frame3 dans Frame2
        self.Frame3 = tk.Frame(self.Frame2, borderwidth=0, relief=tk.GROOVE)
        self.Frame3.pack(side=tk.TOP, padx=5, pady=5)
        
        # Frame 4 dans Frame2
        self.Frame4 = tk.Frame(self.Frame2, bg="white", borderwidth=0, relief=tk.GROOVE)
        self.Frame4.pack(side=tk.BOTTOM, padx=40, pady=40)
        
        # Frame 5 dans Frame 4
        self.Frame5 = tk.Frame(self.Frame2, borderwidth=0, relief=tk.GROOVE)
        self.Frame5.pack(side=tk.TOP, padx=5, pady=5)
        
        # Frame 6 dans Frame 4
        self.Frame6 = tk.Frame(self.Frame4, borderwidth=0, relief=tk.GROOVE)
        self.Frame6.pack(side=tk.BOTTOM, padx=5, pady=5)
        
        # Frame 7 dans Frame 6
        self.Frame7 = tk.Frame(self.Frame6, borderwidth=0, relief=tk.GROOVE)
        self.Frame7.pack(side=tk.LEFT, padx=5, pady=5)
        
        # Frame 8 dans Frame 6
        self.Frame8 = tk.Frame(self.Frame6, borderwidth=0, relief=tk.GROOVE)
        self.Frame8.pack(side=tk.RIGHT, padx=5, pady=5)
        
        ## Ajout de labels pour les Frames
        tk.Label(self.Frame1, text="CHOIX DES PARAMETRES",font=("Helvetica", 10, 'bold')).pack(padx=10, pady=10)
        tk.Label(self.Frame3, text="LISTE DES POINTS SELECTIONNES",font=("Helvetica", 10, 'bold')).pack(padx=10, pady=10,side="top")
        tk.Label(self.fenetre, text="Cliquez sur l'image avec le bouton droit de la souris pour sélectionner les points définissant les zones (fond en premier)").pack(padx=10, pady=10)

        ## Ajouts des Widgets

        # Parametre de choix d'algorithme
        # radiobutton
        self.no_algo = tk.StringVar() 
        self.bouton1 = tk.Radiobutton(self.Frame1, text="Graph Cuts", variable=self.no_algo, value=1) 
        self.bouton2 = tk.Radiobutton(self.Frame1, text="Partage des eaux", variable=self.no_algo, value=2)
        self.bouton1.pack()
        self.bouton2.pack()
        
        # Parametre Lambda determine a l'aide d'un curseur
        self.Lambda = tk.DoubleVar()
        self.scale = tk.Scale(self.Frame1, orient='horizontal', from_=0, to=10, resolution=0.1, tickinterval=2, length=350, variable=self.Lambda, label='Lambda')
        self.scale.pack()
        
        # Affichage de la liste des points selectionnes dans la fenetre
        self.sv = tk.StringVar()
        self.Frame4 = tk.LabelFrame(self.Frame3, text="Liste des points sélectionnés", padx=20, pady=20)
        self.Frame4.pack(fill="both", expand="yes")
        self.sv.set(str(self.Liste_points))
        tk.Label(self.Frame3, textvariable=self.sv).pack()

        # bouton de reinitialisation de la liste des points a vide 
        self.bouton_init = tk.Button(self.Frame5, text="Réinitialisation de la liste des points", command = self.Liste_nulle )
        self.bouton_init.pack()    
        
        # en cas de clik droit de la souris, appel de la fonction select_pixel_maj pour mise a jour de la liste et de l'affichage
        self.fenetre.bind("<Button-3>", self.select_pixel_maj )
        
        # Affichage du fichier image
        self.canvas = tk.Canvas(self.fenetre, width=self.photo.width(), height=self.photo.height(), bg="gray")
        self.canvas.create_image(0, 0, anchor=tk.NW, image=self.photo)
        self.canvas.pack()
        
        # Bouton d'importation
        self.bouton_importation=tk.Button(self.Frame7, text = "Importer", command = self.import_image)
        self.bouton_importation.pack(side="left")
        
        # Bouton de calcul
        self.bouton_calcul=tk.Button(self.Frame7, text = "Calculer", command = self.calcul_zone)
        self.bouton_calcul.pack(side="left")
        
        # Bouton d'affichage
        self.bouton_affichage=tk.Button(self.Frame7, text = "Afficher", command = self.affiche_zone)
        self.bouton_affichage.pack(side="left")
        
        # Bouton d'exportation
        self.bouton_exportation=tk.Button(self.Frame7, text = "Exporter", command = self.exporte_image)
        self.bouton_exportation.pack(side="left")
        
        # Bouton de sortie
        """ command = self.fenetre.quit fenetre.quit plante la 2eme fois - comment quitter l'application ? """
        self.bouton_sortie=tk.Button(self.Frame8, text = "Quitter", command = exit ) 
        self.bouton_sortie.pack(side="right")
        
        self.fenetre.mainloop()

    ## FONCTIONS ASSOCIEES AUX WIDGETS

    def Liste_nulle(self):
        """Fonction d'initialisation de la liste des points
        procedure sans argument qui remet la variable globale Listepoints a vide 
        et qui met a jour son affichage dans la fenetre graphique"""
    
        global Liste_points
        self.Liste_points=[]
        self.sv.set(str(self.Liste_points))
     

    def select_pixel(self,event):
        """ Fonction de sélection de pixel a la souris
        event ->
        prend en entree un evenement (qui sera le click droit de la souris), collecte les coordonnees X,Y du point clique
        ajoute [X,Y] à la liste Liste_point (variable globale)"""
    
        X=event.x
        Y=event.y
        self.Liste_points.append([X,Y])
    
    def select_pixel_maj(self,event):
        """ Gestion de la selection des points
        event->
        quand event se produit (se sera click droit de la souris sur l'image), 
        la liste Liste_points est augmentee des coordonnees du point clique et l'affichage de cette liste est mise a jour """
    
        self.select_pixel(event)
        self.sv.set(str(self.Liste_points))
        
    def import_image(self): 
        """???en test ??? comment remplacer l'ancienne image par la nouvelle dans le canvas? Actuellement il reste un cadre gris. Peut-être raffraichir la fenetre recompletement ??? A chercher """
        # # Selection du nouveau fichier image a traiter au format PNG niveau de gris a l'aide d'une fenetre de selection
        # self.filepath = tkfd.askopenfilename(title="Ouvrir une image",filetypes=[('png files','.png'),('all files','.*')])
        # 
        # # Ouverture du fichier image a traiter au format PNG niveau de gris et stockage dans la variable photo
        # self.photo = tk.PhotoImage(file=self.filepath)
        # 
        # # Affichage du fichier image
        # self.canvas.delete(tk.ALL)
        # self.canvas = tk.Canvas(self.fenetre, width=self.photo.width(), height=self.photo.height(), bg="gray")
        # self.canvas.create_image(0, 0, anchor=tk.NW, image=self.photo)
        # self.canvas.pack()
        print("Import de la nouvelle image !")
    
    def calcul_zone_0(self):
        """ calcul de la zone en fonction de la méthode choisie puis coloriage des points correspondants"""
        if self.no_algo.get()=='1':
            methode_choisie="des Graph Cuts"
            print("Calcul de la zone a segmenter par la méthode %s. " %methode_choisie)
        elif self.no_algo.get()=='2':
            methode_choisie="de partage des eaux"
            print("Calcul de la zone à segmenter par la méthode %s. " %methode_choisie)
        else : 
            print("Vous n'avez pas choisi de méthode. Veuillez faire un choix.")
        
        """ Coloriage d'une liste de points sélectionnés aux clavier sur le tableau de l'image a exporter 
            permettra de colorier une zone segmentee ulterieurement"""
        if self.no_algo.get()=='1' or self.no_algo.get()=='2':
            self.img.colorie_zone_img_RGB_np(self.Liste_points,'red')
    
    def calcul_zone(self): 
        if self.no_algo.get()=='1' or self.no_algo.get()=='2':
            source=Sommet(self.Liste_points[1][0],self.Liste_points[1][1])
            puits=Sommet(self.Liste_points[0][0],self.Liste_points[0][1])
            graphe_flot=Graphe(self.img,source,puits)
            Liste_points_a_colorier=graphe_flot.graph_cut()
            print(Liste_points_a_colorier)
            self.img.colorie_zone_img_RGB_np(Liste_points_a_colorier,'blue')
    
    def affiche_zone(self):
        """ Affichage de l'image RGB avec les zones coloriees"""
        #print("Affichage de la zone segmentée!")
        self.img.affiche_image_RGB()
    
    
    def exporte_image(self):
        """" Exportation de l'image avec les zones coloriees"""
        #print("Exportation du fichier de l'image segmentée au format png!")  
        if self.no_algo.get()=='1' or self.no_algo.get()=='2':
            self.img.export_image_2_file(self.no_algo.get()) 
        else:
            self.img.export_image_2_file('0')
  
### FIN DE LA FENETRE TKINTER / classe APPLICATION


###______Programme principal_______________________________________________________
if __name__ == '__main__':
    f = Application() # instanciation de l'objet application
    
    ## test des fonctions
    # # Creation d'une instance de Image
    # img=Image(f.filepath)
    # # Coloriage d'une liste de points sélectionnés aux clavier sur le tableau de l'image a exporter 
    # # permettra de colorier une zone segmentee ulterieurement
    # img.colorie_zone_img_RGB_np(f.Liste_points,'red')
    # # Affichage de l'image transformee 
    # img.affiche_image_RGB()
    # # Export de l'image transformee
    # img.export_image_2_file(f.no_algo.get())
    
    # Affichage de la liste_des points, de Lamdba et de No_algo
    # print(f.Liste_points)
    # print(f.Lambda.get())
    # print(f.no_algo.get())

