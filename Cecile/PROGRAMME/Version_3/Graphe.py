#####################################
# Cecile Prouteau
# CTES Projet Math-Info
# Projet segmentation d'image
# 15/01/2019
# Classe Graphe Version 0.1
#####################################

### IMPORTATION DES BIBLIOTHEQUES
import numpy as np
import math

### IMPORTATION DES CLASSES
from Image import Image
from Sommet import Sommet
from Arc import Arc


### DEBUT de la classe Graphe

class Graphe:
    def __init__(self,image,L_S, L_P):
        """

        Constructeur d'un objet Graphe à partir d'une image d'un pixel servant de 

        :param self: Objet Graphe 
        :type self: Graphe
        
        :param image: objet Image est un tableau numpy d'une image codée sur une couche en niveaux de gris
        :type image: Image 
        
        :param L_S: objet Liste des coordonnées des pixels particuliers de l'image servant pour la création du graphe : ces couples de coordonnées servent pour repérer les zones objets à détecter donc sommets reliés à la source dans le graphe.
        :type L_S: List     
        
        :param L_P: objet Liste des coordonnées des pixels particuliers de l'image servant pour la création du graphe : ces couples de coordonnées servent pour repérer les zones de l'arrière plan donc sommets reliés au puits dans le graphe.
        :type L_P: List 
        
        :return: Objet Graphe
        :rtype: Graphe
        
        Liste des champs de la classe
        
        * self.largeur largeur de l'image
        * self.hauteur hauteur de l'image
        * self.nb_sommets nombre de pixels + 2
        
        * self.liste_sommets liste des sommets du graphe (un par pixel)
        
        * self.liste_coord_puits liste des coordonnées des pixels puits / dans l'arrière plan
        * self.liste_id_H_puits liste des identifiants des sommets puits
        * self.liste_puits = liste des sommets puits 
        
        * self.liste_coord_sources liste des coordonnées des pixels sources / dans les objets à  détecter
        * self.liste_id_H_source numéro du sommet source d'indice 0 dans la liste précédente
        * self.liste_source sommet source d'indice 0 dans la liste précédente
        
        * self.id_H_puits numéro du sommet hyper_puits
        * self.H_puits = sommet hyper_puits   
        
        * self.id_H_source numéro du sommet hyper_source
        * self.H_source sommet hyper_source
        
        * self.liste_adj_id_voisins liste des liste d'id de sommets voisins par id de sommet
        * self.liste_adj_arcs liste des listes des arcs du graphe par numéro de sommet d'origine
        
        Pour les lignes et les colonnes je me place du point de vue de l'image. 
        Dans le tableau numpy, les coordonnées sont inversées.
        
        """
        
        #self.image=image.image_np_gray
        
        self.largeur = image.largeur
        self.hauteur = image.hauteur
        
        self.nb_sommets = self.largeur*self.hauteur+2
        
        print ("Caractéristiques du graphe :")
        print(self.largeur)
        print(self.hauteur)
        print("nb sommets")
        print(self.nb_sommets)
        print ("Fin des caractéristiques du graphe !")
        
        #création de la liste des sommets du graphe self.liste_sommets issus des pixels de l'image
 
        self.image_2_liste_sommets(image)
        # print("liste sommets")
        # print(len(self.liste_sommets))
        # for s in self.liste_sommets:
        #     s.affiche_sommet()
        # print()
        
        # identification des sources et du puits à partir des listes  L_S et L_P 
        
        self.liste_coord_sources = L_S
        self.liste_id_H_source =  self.list_coordonnees_2_list_id_sommet(L_S) # liste des identifiants des sommets source
        self.liste_source = self.list_id_sommet_2_list_sommets(self.liste_id_H_source)# liste des sommets  sources 

        self.liste_coord_puits = L_P
        self.liste_id_H_puits = self.list_coordonnees_2_list_id_sommet(L_P) # liste des identifiants des sommets puits
        self.liste_puits = self.list_id_sommet_2_list_sommets(self.liste_id_H_puits) # liste des sommets puits 
        
        
        
        # Création de l'hyperpuits et de l'hypersource
        
        self.id_H_source = self.nb_sommets-2
        self.ajoute_extremite(self.id_H_source)
        self.H_source = self.liste_sommets[self.id_H_source]
        
        self.id_H_puits = self.nb_sommets-1    # crée l'identificateur
        self.ajoute_extremite(self.id_H_puits)  # ajoute l'hyper-puits à la liste des sommets
        self.H_puits = self.liste_sommets[self.id_H_puits] # crée le champ correspondant  
        
        print('liste id sources ')
        print(self.id_H_source)
        print(self.liste_id_H_source)
        
        print('liste id puits')
        print(self.id_H_puits)
        print(self.liste_id_H_puits)
        
        print("nb sommets")
        print(len(self.liste_sommets))
        
        # la liste des sommets est complète
        
        # création de la liste d'adjacence des id_sommets par id_sommet
        self.cree_liste_adj_id_voisins()
        
        #La liste des listes des arcs partant de chaque sommet graphe 
        self.image_2_liste_adj_arcs(image) # crée liste_adj_arcs
        
        # print(self.liste_adj_arcs)
        # print(len(self.liste_adj_arcs))
        # for l in self.liste_adj_arcs:
        #     for a in l:
        #         a.affiche_arc()
        #     print()
        
    ## FONCTIONS SUR LES SOMMETS    
    def coordonnees_2_id_sommet(self,x,y):
        
        """
        Retourne l'indice du sommet dans la liste à partir de ses coordonnées de point sur l'image
        
        :param self: Objet Graphe 
        :type self: Graphe
        
        :param x: abscisse du point sur l'image correspondante au sommet
        :type x: int 
        
        :param y: ordonnée du point sur l'image correspondante au sommet
        :type y: int   
        
        :return: indice du sommet dans la liste 
        :rtype: int
        
        """
        
        return x+y*(self.largeur)
        
    def list_coordonnees_2_list_id_sommet(self,L_coor):
        
        """
        Retourne l'indice du sommet dans la liste à partir de ses coordonnées de point sur l'image
        
        :param self: Objet Graphe 
        :type self: Graphe
        
        :param L_coor: Liste de couples de coordonnées
        :type L_coor: List 
        
        :return: Liste des indices des sommets correspondants aux couples de coordonnées
        :rtype: List
        
        """
        
        L=[]
        for coor in L_coor :
            x,y = coor
            L.append(self.coordonnees_2_id_sommet(x,y))
        return L
        
    def list_id_sommet_2_list_sommets(self,L_id):
        
        """
        Retourne l'indice du sommet dans la liste à partir de ses coordonnées de point sur l'image
        
        :param self: Objet Graphe 
        :type self: Graphe
        
        :param L_id: Liste d'entiers, identificateurs de sommets
        :type L_id: List 
        
        :return: Liste des sommets correspondants aux indices des sommets
        :rtype: List
        
        """
        
        L=[]
        for id in L_id :
            L.append(self.liste_sommets[id])
        return L
        
   #  def sommet_2_id_sommet(self,sommet):
   #      """
   #      Retourne l'indice du sommet dans la liste à partir du sommet
   #      
   #      :param self: Objet Graphe 
   #      :type self: Graphe
   #      
   #      :param sommet: Objet sommet
   #      :type image: sommet 

  ##   #       :return: numéro du sommet
   #      :rtype: int
   #      
   #      """
   #      
   #      return self.coordonnees_2_id_sommet(sommet.x,sommet.y)
        
    def id_sommet_2_coordonnees(self,n):
        """
        Retourne les coordonnées du sommet sur l'image à partir de son numéro dans la liste
        
        :param self: Objet Graphe 
        :type self: Graphe
        
        :param n: indice d'un sommet du graphe dans la liste self.liste_sommets
        :type n: int 

        :return: couple des coordonnées du sommet d'indice n
        :rtype: int*int
        
        """
        
        return (n%self.largeur,n//self.largeur)
    
    def ajoute_sommet(self,x,y,id,v):
        """
        Procédure qui ajoute le sommet de coordonnées (x,y) sur l'image à liste.sommets liste des sommets du graphe
        
        :param self: Objet Graphe 
        :type self: Graphe
        
        :param x: abscisse du point sur l'image correspondante au sommet
        :type x: int 
        
        :param y: ordonnée du point sur l'image correspondante au sommet
        :type y: int   
        
        :param id: identificateur du sommet
        :type id: int 
        
        :param v: valeur du sommet (couleur du pixel correspondant)
        :type v: int 
                
        :return: None 
        :rtype: None object
        
        """
        
        sommet=Sommet(x,y,id,v)
        self.liste_sommets.append(sommet)
        
    def ajoute_extremite(self,id):
        """
        Procédure qui ajoute l'hyperpuits ou l'hypersource à liste.sommets liste des sommets du graphe.
        En dehors de l'identificateur, tous les champs son à None
        
        :param self: Objet Graphe 
        :type self: Graphe
        
        :param x: abscisse du point sur l'image correspondante au sommet
        :type image: int 
        
        :return: None 
        :rtype: None object
        
        """
        
        self.ajoute_sommet(None,None,id,math.inf)
    
    def image_2_liste_sommets(self,img):
        """
        Procédure qui modifie l'Objet Graphe self en créant la liste des sommets à partir des pixels de l'image img
        
        :param self: Objet Graphe 
        :type self: Graphe
        
        :param img: Image
        :type image: objet Image  
        
        :return: None 
        :rtype: None object
        
        """
        
        # l'image est parcourue colonne par colonne - le tableau numpy est parcouru ligne par ligne à cause de l'inversion des coordonnées
        
        self.liste_sommets=[]
        for y in range(self.hauteur):
            for x in range (self.largeur):
                id = self.coordonnees_2_id_sommet(x,y)
                c=img.couleur_entier_8bits(y,x)
                self.ajoute_sommet(x, y,id,c)
    
    def ajoute_id_ds_liste_adj_id_voisin(self,id_sommet,id_voisin):
        
        """
        Ajoute dans self.liste_adj_id_voisin le voisin d'identificateur id_voisin du sommet d'identificateur id_sommet
        
        :param self: Objet Graphe 
        :type self: Graphe
        
        :param id_sommet: indice d'un sommet dans la liste des sommets self.liste_sommets.
        :type id_sommet: int  
        
        :param id_v: indice d'un sommet dans la liste des sommets self.liste_sommets.
        :type id_v: int  
        
        :return: None
        :rtype: None
        
        """
        
        self.liste_adj_id_voisins[id_sommet].append(id_voisin)
        
    # def ajoute_voisin_a_liste_voisin(self,sommet,L, orientation):
    #     """
    #     
    #     Modifie la liste L en lui ajoutant le voisin du sommet id_voisin dans l'orientation "orientation"
    #     
    #     :param self: Objet Graphe 
    #     :type self: Graphe
    #     
    #     :param sommet: objet sommet dans la liste des sommets self.liste_sommets.
    #     :type sommet: sommet  
    #     
    #     :param L: liste d'id de sommets voisins de id_sommet.
    #     :type L: List
    #     
    #     :param orientation: orientation dans laquelle chercher le voisin.
    #     :type orientation: Str
    #     
    #     :return: la liste des numéros de sommets voisins d'un sommet identifié par son indice id_sommet dans la liste des sommets self.liste_sommets dans l'image.
    #     :rtype: List
    #     
    #     """
    #     
    #     x_voisin,y_voisin=sommet.coord_voisin(orientation)
    #     id_voisin=self.coordonnees_2_id_sommet(x_voisin,y_voisin)
    #     L.append(id_voisin)

                
    def cree_liste_id_sommets_voisins(self,id_sommet):
        """
        
        :param self: Objet Graphe 
        :type self: Graphe
        
        :param id_sommet: indice d'un sommet dans la liste des sommets self.liste_sommets.
        :type id_sommet: int  
        
        :return: la liste des numéros de sommets voisins d'un sommet identifié par son indice id_sommet dans la liste des sommets self.liste_sommets dans l'image.
        :rtype: List
        
        """
        
        # Sera utilisée pour la création de la liste des arcs donc ne peut pas se servir de celle-ci ici
        
        Liste_id_voisins=[]
        sommet=self.liste_sommets[id_sommet]
        # print(sommet.y)
        # print(self.hauteur)
        if sommet.y!=None and 0 <= sommet.y < self.hauteur :
            if sommet.x > 0 :
                # x_voisin_gauche,y_voisin_gauche=sommet.coord_voisin('gauche')
                # id_voisin_gauche=self.coordonnees_2_id_sommet(x_voisin_gauche,y_voisin_gauche)
                # Liste_id_voisins.append(id_voisin_gauche)
                # 
                # self.ajoute_voisin_a_liste_voisin(sommet , Liste_id_voisins , 'gauche')
                
                Liste_id_voisins.append(id_sommet-1)
                
                
            if sommet.x < self.largeur-1 :
                # x_voisin_droite,y_voisin_droite=sommet.coord_voisin('droite')
                # id_voisin_droite=self.coordonnees_2_id_sommet(x_voisin_droite,y_voisin_droite)
                # Liste_id_voisins.append(id_voisin_droite)
                Liste_id_voisins.append(id_sommet+1)
        
        if sommet.x!=None and 0<=sommet.x<self.largeur :    
            if sommet.y > 0:
                # x_voisin_haut,y_voisin_haut=sommet.coord_voisin('haut')
                # id_voisin_haut=self.coordonnees_2_id_sommet(x_voisin_haut,y_voisin_haut)
                # Liste_id_voisins.append(id_voisin_haut)
                Liste_id_voisins.append(id_sommet-self.largeur)
                
            if sommet.y < self.hauteur-1 :
                # x_voisin_bas,y_voisin_bas=sommet.coord_voisin('bas')
                # id_voisin_bas=self.coordonnees_2_id_sommet(x_voisin_bas,y_voisin_bas)
                # Liste_id_voisins.append(id_voisin_bas)
                Liste_id_voisins.append(id_sommet+self.largeur)    
        return Liste_id_voisins
        
    def cree_liste_adj_id_voisins(self):
        
        """
        
        :param self: Objet Graphe 
        :type self: Graphe
        
        :return: la liste des numéros de sommets voisins d'un sommet identifié par son indice id_sommet dans la liste des sommets self.liste_sommets dans l'image.
        :rtype: List
        
        """
        # initialisation de la liste
        self.liste_adj_id_voisins=[]
        
        # Liste d'adjacence des sommets correspondants aux pixels
        for id in range(self.nb_sommets-2):
            self.liste_adj_id_voisins.append(self.cree_liste_id_sommets_voisins(id))
            
        # print("self.liste_adj_id_voisins")
        # print(self.liste_adj_id_voisins)
        # print(len(self.liste_adj_id_voisins))
        
        # Hypersource
        self.liste_adj_id_voisins.append([])
        
        for id_sommet in self.liste_id_H_source:
            self.liste_adj_id_voisins[self.id_H_source].append(id_sommet)
            
        # Hyperpuits n'a pas de voisin mais est le voisin des puits
        self.liste_adj_id_voisins.append([])
        for id_sommet in self.liste_id_H_puits:
            self.liste_adj_id_voisins[id_sommet].append(self.id_H_puits)
            
        #     print(id_sommet,end=' : ')
        #     print(self.liste_adj_id_voisins[id_sommet])
        # 
        # print('liste adj voisins')        
        # print(self.liste_adj_id_voisins)
        # print(len(self.liste_adj_id_voisins))
        
    def suppr_id_de_liste_adj_id_voisin(self,id_sommet,id_voisin):
        
        """
        Supprime de self.liste_adj_id_voisin le voisin d'identificateur id_voisin du sommet d'identificateur id_sommet
        
        :param self: Objet Graphe 
        :type self: Graphe
        
        :param id_sommet: indice d'un sommet dans la liste des sommets self.liste_sommets.
        :type id_sommet: int  
        
        :param id_v: indice d'un sommet dans la liste des sommets self.liste_sommets.
        :type id_v: int  
        
        :return: None
        :rtype: None
        
        """
        
        for id in self.liste_adj_id_voisins[id_sommet]:
            if id == id_voisin :
                self.liste_adj_id_voisins[id_sommet].remove(id_voisin)

    ## FONCTIONS SUR LES ARCS

    def calcule_capacite(self,o,d):
        """
        Fonction de calul du capacite d'une arête en fonction des valeurs des pixels
        
        :param o: doit être un nombre entier entre 0 et 255.  
        :type o: int
        
        :param d: doit être un nombre entier entre 0 et 255.  
        :type d: int
        
        :return: valeur de l'arc. Entier compris entre 0 et 255
        :rtype: int
        
        """
        # return d-o 
        # return abs(d-o)
        
        # return int(256/(1+d-o))
        # return int(256/(1+abs(d-o)))
        
        # return 255-(d-o)
        return 255-abs(d-o)
    
    def cree_arc(self,origine,destination,img): 
        """
        
        Pour les sommets correspondants aux pixels uniquement

        :param self: Objet Graphe 
        :type self: Graphe
        
        :param origine: Objet Sommet origine de l'arc à créer 
        :type origine: Sommet
        
        :param destination: Objet Sommet destination de l'arc à créer 
        :type destination: Sommet
        
        :param img: Image où récupérer les valeurs des pixels correspondant aux sommets origine et destination afin de déterminer le capacite de l'arc
        :type image: objet Image  
        
        :return: crée et retourne un objet Arc allant du sommet origine au sommet destination. Son capacite est déterminé comme 255 - valeur absolue de la différence entre les couleurs des pixels correspondants sur l'image img codées sur 8 bits en niveaux de gris
        :rtype: Arc
        
        """
        
        #capacite = abs(img.couleur_entier_8bits(destination.y,destination.x) - img.couleur_entier_8bits(origine.y,origine.x))
        #capacite=calcule_capacite(img.couleur_entier_8bits(origine.y,origine.x),img.couleur_entier_8bits(destination.y,destination.x))
        o=img.couleur_entier_8bits(origine.y,origine.x)
        d=img.couleur_entier_8bits(destination.y,destination.x)
        
        capacite = self.calcule_capacite(o,d)
        #print(capacite,end=";")
        return Arc(origine,destination,capacite)
    
    def cree_double_arc(self,sommet_1,sommet_2,img):
        """Crée les arcs dans les deux sens entre le sommet 2 et le sommet 2

        Pour les sommets correspondants aux pixels uniquement

        :param self: Objet Graphe 
        :type self: Graphe
        
        :param sommet_1: Objet Sommet origine de l'arc à créer 
        :type sommet_1: Sommet
        
        :param sommet_2: Objet Sommet destination de l'arc à créer 
        :type sommet_2: Sommet
        
        :param img: Image où récupérer les valeurs des pixels correspondant aux sommets origine et destination afin de déterminer le capacite de l'arc
        :type image: objet Image  
        
        :return: crée et retourne un couple d'objet Arc allant dans les deux sens. Leur capacite est déterminé comme 255 - valeur absolue de la différence entre les couleurs des deux pixels correspondants sur l'image img codées sur 8 bits en niveaux de gris
        :rtype: (Arc,Arc)
        
        """
        
        return (self.cree_arc(sommet_1,sommet_2,img),self.cree_arc(sommet_2,sommet_1,img))
        
    def ajoute_arc(self,origine,destination,img):  
        """ 
        Procédure qui crée un arc avec cree_arc(self,origine,destination,img) et l'ajoute au graphe self. Il va du sommet origine au sommet destination. son capacite est déterminé par cree_arc().
        
        :param self: Objet Graphe 
        :type self: Graphe
        
        :param origine: Objet Sommet origine de l'arc à créer 
        :type origine: Sommet
        
        :param destination: Objet Sommet destination de l'arc à créer 
        :type destination: Sommet
        
        :param img: Image où récupérer les valeurs des pixels correspondant aux sommets origine et destination afin de déterminer le capacite de l'arc
        :type image: objet Image 

        :return: None 
        :rtype: None object
        
        """
        
        arc=self.cree_arc(origine,destination,img)
        self.liste_adj_arcs[origine.id].append(arc)
        
    def supprime_arc(self,arc):
        """
        Procédure qui supprime l'arc 'arc' de la liste des arc liste_arc du graphe self et le sommet arc.destination de la liste des voisins de arc.origine
        
        :param self: Objet Graphe 
        :type self: Graphe
        
        :param arc: Objet Arc
        :type arc: Arc

        :return: None 
        :rtype: None object
        
        """
        
        for a in self.liste_adj_arcs[arc.origine.id]:
            if arc.destination == a.destination :
                self.liste_adj_arcs[id_origine].remove(a)
        
        self.suppr_id_de_liste_adj_id_voisin(arc.origine.id,arc.destination.id)
    
    # def cree_arcs_de_sommet_vers_voisins(self , id_sommet, img):
    #     """ Crée tous les arc partant d'un sommet identifié par son no vers tous ses voisins
    #     si le sommet n'est pas le puits, chercher les voisins et creer les arcs qui vont su sommet vers ses voisins"""
    #     sommet=self.liste_sommets[id_sommet]
    #     if id_sommet != self.id_H_puits :
    #         Liste_id_sommets_voisins = self.liste_id_sommets_voisins(id_sommet)
    #         for id_voisin in Liste_id_sommets_voisins:
    #             voisin = self.liste_sommets[id_voisin]
    #             self.ajoute_arc(sommet,voisin,img)
    
    # def cree_arcs_de_sommet_vers_voisins(self, id_sommet, img):
    #     """ 
    #     Procédure qui modifie l'objet Graphe self. 
    #     Si le sommet n'est pas le puits, crée tous les arcs partant d'un sommet identifié par son indice id_sommet dans la liste des sommets du graphe vers tous les sommets correspondants aux pixels voisins sur l'image. Ajoute ces arcs à la liste des arcs.
    #     
    #     :param self: Objet Graphe 
    #     :type self: Graphe
    #     
    #     :param id_sommet: indice d'un sommet dans la liste des sommets self.liste_sommets.
    #     :type id_sommet: int  
    #     
    #     :param img: Image où récupérer les valeurs des pixels correspondant aux sommets origine et destination afin de déterminer le capacite des arcs à créer
    #     :type image: objet Image
    #     
    #     :return: None 
    #     :rtype: None object
    #     
    #     """
    #     
    #     #On cherche les voisins du sommet et on crée les arcs qui vont du sommet vers ses voisins
    #     
    #     sommet=self.liste_sommets[id_sommet]
    #     if id_sommet != self.id_H_puits :
    #         Liste_id_sommets_voisins = self.liste_id_sommets_voisins(id_sommet)
    #         # affichage liste voisins
    #         # print(id_sommet)
    #         # sommet.affiche_sommet()
    #         # print()
    #         # print(Liste_id_sommets_voisins)
    #         for id_voisin in Liste_id_sommets_voisins:
    #             voisin = self.liste_sommets[id_voisin]
    #             #affiche debug
    #             # voisin.affiche_sommet()
    #             self.ajoute_arc(sommet,voisin,img)

                
    def supprime_arcs_vers_sommet(self,id_sommet):
        """
        Procédure qui supprime de la liste des arcs liste_arcs de l'Objet Graphe tous les arcs arrivant à un sommet repéré par son indice id_sommet dans la liste des sommets et partant de ses voisins. 
        
        :param self: Objet Graphe 
        :type self: Graphe
        
        :param id_sommet: indice d'un sommet dans la liste des sommets self.liste_sommets.
        :type id_sommet: int  
        
        :return: None
        :rtype: None Object
        
        """
        
        sommet=self.liste_sommets[id_sommet]
        Liste_id_sommets_voisins = self.liste_id_sommets_voisins(id_sommet)
        for id_voisin in Liste_id_sommets_voisins:
            # recopie de l'adresse de la liste puis modification du contenu commun
            Liste_arcs_du_voisin = self.liste_adj_arcs[id_voisin] 
            for arc in Liste_arcs_du_voisin :
                if  sommet.est_egal(arc.destination) :
                    Liste_arcs_du_voisin.remove(arc)
        
    # def image_2_liste_arcs(self,img):
    #     """ 
    #     Procédure qui modifie l'objet Graphe. Elle ajoute à la liste les listes d'adjacences de tous les arcs à partir de l'image indexées par numéro de sommet de départ.
    #     
    #     :param self: Objet Graphe  où la liste des sommets a déjà été créée
    #     :type self: Graphe
    #     
    #     :param img: Image où récupérer les les informations pour la crétion des arcs
    #     :type image: objet Image
    #     
    #     :return: None
    #     :rtype: None object
    #     
    #     """
    #     
    #     #print("Création des arcs !")
    #     # initialisation de liste des listes d'arcs partant de chaque sommet (liste d'adjacence)
    #     self.liste_adj_arcs=self.nb_sommets*[None]
    #     for id_sommet in range(self.nb_sommets):
    #         self.liste_adj_arcs[id_sommet]=[]
    #     # Remplissage des listes
    #     for id_sommet in range (self.nb_sommets):
    #         self.cree_arcs_de_sommet_vers_voisins(id_sommet, img) 
    #     
    #     # zone de tests      
    #     #print("Fin de création des arcs !")
    #     #affichage liste des arcs
    #     # for i in range(self.nb_sommets):
    #     #     print(self.liste_adj_arcs[i])   
    #          
    #     # On supprime les arcs qui arrivent au sommet source et qui ont étés créés inutilement 
    #     # (c'est moins long que de tester si source est le voisin à chaque création d'arc étant donné le nombre
    #     # de pixels dans une image, le coût de la supression est constant)
    #     self.supprime_arcs_vers_sommet(self.id_H_source)
        
    def image_2_liste_adj_arcs(self,img):
        """ 
        Procédure qui modifie l'objet Graphe. Elle ajoute à la liste les listes d'adjacences de tous les arcs à partir de l'image indexées par numéro de sommet de départ et les arcs partant de l'hypersource et vers l'hyperpuits avec une capacité infinie.
        
        :param self: Objet Graphe  où la liste des sommets a déjà été créée
        :type self: Graphe

        :param img: Image où récupérer les les informations pour la crétion des arcs
        :type image: objet Image
        
        :return: None
        :rtype: None object
        
        """
        
        print("Création des arcs !")
        # initialisation de liste des listes d'arcs partant de chaque sommet (liste d'adjacence)
        self.liste_adj_arcs=(self.nb_sommets)*[None]
        
        for id_origine in range(self.nb_sommets):
            self.liste_adj_arcs[id_origine]=[]
            origine=self.liste_sommets[id_origine]
            self.liste_adj_arcs[id_origine]=[]
            
            for id_destination in self.liste_adj_id_voisins[id_origine]:
                destination=self.liste_sommets[id_destination]
                
                if id_origine==self.id_H_source or id_destination==self.id_H_puits:
                    arc=Arc(origine,destination,math.inf)
                    self.liste_adj_arcs[id_origine].append(arc)
                else :
                    self.liste_adj_arcs[id_origine].append(self.cree_arc(origine,destination,img))

        # zone de tests      
        print("Fin de création des arcs !")
        #affichage liste des arcs
        # for i in range(self.nb_sommets):
        #     print(self.liste_adj_arcs[i])   
   

    def arc_de_sommet1_a_sommet2(self,id_sommet_1,id_sommet_2):
        """ 
        
        :param self: Objet Graphe où la liste des sommets a déjà été créée
        :type self: Graphe
        
        :param id_sommet_1: indice d'un sommet dans la liste des sommets self.liste_sommets.
        :type id_sommet_1: int  
        
        :param id_sommet_2: indice d'un sommet dans la liste des sommets self.liste_sommets.
        :type id_sommet_2: int
        
        :return: l'objet Arc qui va du sommet_1 au sommet_2 s'il y en a un None sinon.
        :rtype: Arc ou None
        
        """
        
        # affichage debug
        #print(id_sommet_1)
        if self.liste_adj_arcs[id_sommet_1]!=[]:
            for arc in self.liste_adj_arcs[id_sommet_1]:
                if arc.destination.id == id_sommet_2:
                    return(arc)
        else:
            print("Pas d'arc partant de ce sommet!")
            return(None)
            
    def arc_inverse(self,arc):
        """ 
        a ne pas appliquer si 
            arc.destination = Hyper-puits puisqu'aucun arc ne part de l'arrivée 
            ou si arc.origine = Hyper-source puisqu'aucun arc n'arrive au départ 
        
        :param arc: objet Arc arc qui va du sommet1 au sommet2.
        :type arc: Arc
        
        :return: l'objet arc (sommet 2->sommet 1) inverse de arc (sommet 1->sommet 2)
        :rtype: Arc
        
        """
        if arc.destination!=self.H_puits and arc.origine!=self.H_source:
            for a in self.liste_adj_arcs[arc.destination.id]:
                if a.destination==arc.origine:
                    # print()
                    # arc.affiche_arc()
                    # a.affiche_arc()
                    # print()
                    return a  
                
    def supprime_arcs_capacite_res_nulle(self):
        """ 
        Procédure qui supprime tous les arcs de capacite résiduelle nulle du graphe de flot
        
        :param self: Objet Graphe  complètement créé
        :type self: Graphe
        
        :return: None
        :rtype: None object
        
        """
        
        for id_sommet in range(self.nb_sommets) :
            for arc in self.liste_adj_arcs[id_sommet]:
                if arc.capacite_res==0:
                    # supression de l'arc
                    self.liste_adj_arcs[id_sommet].remove(arc) 
                    # supression du voisin correspondant dans la liste self.liste_adj_id_voisins
                    self.liste_adj_id_voisins[id_sommet].remove(arc.destination.id)
                    
    def supprime_arcs_capacite_res_neg_nulle(self):
        """ 
        Procédure qui supprime tous les arcs de capacite résiduelle nulle du graphe de flot
        
        :param self: Objet Graphe  complètement créé
        :type self: Graphe
        
        :return: None
        :rtype: None object
        
        """
        
        for id_sommet in range(self.nb_sommets) :
            for arc in self.liste_adj_arcs[id_sommet]:
                if arc.capacite_res<=0:
                    # supression de l'arc
                    self.liste_adj_arcs[id_sommet].remove(arc) 
                    # supression du voisin correspondant dans la liste self.liste_adj_id_voisins
                    self.liste_adj_id_voisins[id_sommet].remove(arc.destination.id)

    ## FONCTIONS POUR LA RECHERCHE DE CHEMIN version 1
    
    def distances_minimales(self,id_sommet_depart):
        """ 
         
        :param self: Objet Graphe  où la liste des sommets a déjà été créée
        :type self: Graphe
        
        :param id_sommet_depart: indice d'un sommet dans la liste des sommets self.liste_sommets.
        :type id_sommet_depart: int  
        
        :return: la liste [liste_distance, liste_peres ]. Elle est composée de la liste des distaces minimales et de la liste des pères indexées par les no de sommet correspondant . Résultats obtenus par la méthode de Bellman-Ford qui calcule la distance d'un chemin minimal entre le sommet de départ identifié par id_sommet_départ et tous les sommets du graphe avec mémorisation des pères.
        :rtype: List
        
        """
        
        # Création de la liste des distances au sommet de départ
        liste_distance=self.nb_sommets*[math.inf]
        liste_distance[id_sommet_depart]=0
        
        # Création de la liste des pères par chemin minimal
        liste_peres=self.nb_sommets*[None]
        
        # Céation de la liste des numéros de sommets à explorer
        Liste_id_sommets_a_explorer=[id_sommet_depart]
        
        while Liste_id_sommets_a_explorer != []:
            #id_sommet_a_explorer = Liste_id_sommets_a_explorer [0]
            id_sommet_a_explorer = Liste_id_sommets_a_explorer.pop(0)
            #Liste_id_sommets_voisins = self.liste_id_sommets_voisins(id_sommet_a_explorer)
            distance_sommet_a_explorer=liste_distance[id_sommet_a_explorer]
            
            for arc in self.liste_adj_arcs[id_sommet_a_explorer]:
                distance_sommet_voisin=liste_distance[arc.destination.id]
                distance_du_sommet_au_voisin = arc.capacite_res
                if distance_sommet_voisin > distance_sommet_a_explorer + distance_du_sommet_au_voisin :
                    # maj de la liste des distances
                    liste_distance[arc.destination.id] = distance_sommet_a_explorer + distance_du_sommet_au_voisin
                    # maj de la liste des sommets à explorer
                    Liste_id_sommets_a_explorer.append(arc.destination.id)
                    # maj de la liste des peres
                    liste_peres[arc.destination.id]=id_sommet_a_explorer
                    
            #Liste_id_sommets_a_explorer.remove(id_sommet_a_explorer)
        return [liste_distance, liste_peres ] 
    
    def cherche_chemin_minimal(self,id_sommet_depart,id_sommet_arrivee):
        """ 
        :param self: Objet Graphe  où la liste des sommets a déjà été créée
        :type self: Graphe
        
        :param id_sommet_depart: indice d'un sommet dans la liste des sommets self.liste_sommets.
        :type id_sommet_depart: int  
        
        :param id_sommet_arrivee: indice d'un sommet dans la liste des sommets self.liste_sommets.
        :type id_sommet_arrivee: int 
        
        :return: la liste des sommets formant un chemin minimal du sommet de id_sommet_depart au sommet de  id_sommet_arrivee
        :rtype: List
        
        """
        
        Liste_distance,Liste_peres=self.distances_minimales(id_sommet_depart)
        chemin=[]
        id_sommet=id_sommet_arrivee
        while id_sommet!=id_sommet_depart:
            chemin.insert(0,id_sommet)
            id_sommet=Liste_peres[id_sommet]
        chemin.insert(0,id_sommet)
        return(chemin)
            
    # def cherche_chemin_minimal(self,id_sommet_depart,id_sommet_arrivee):
    #     Liste_distance=self.distances_minimales(id_sommet_depart)
    #     Liste_sommets=[]
    #     S=id_sommet_arrivee
    #     while S!=id_sommet_depart:
    #         for T in liste des peres de S
    #             if Liste_distance[S]=Liste_distance[T]+d(s,T):
    #                 Liste_sommets.append(T)
    #                 break
    #         S=T
        
    def distances_maximales(self,id_sommet_depart):
        """ 
        calcule la pseudo-distance minimale (somme des inverses des capacite des arêtes) d'un chemin maximal non infini entre le sommet de départ identifié par id_sommet_départ et tous les sommets du graphe. 
        
        :param self: Objet Graphe  où la liste des sommets a déjà été créée
        :type self: Graphe
        
        :param id_sommet_depart: indice d'un sommet dans la liste des sommets self.liste_sommets.
        :type id_sommet_depart: int  
        
        :return: la liste [liste_distance, liste_peres ]. Elle est composée de la liste des pseudo distances minimales et de la liste des pères indexées par les no de sommet correspondant . Résultats obtenus par la méthode de Bellman-Ford qui calcule la distance d'un chemin minimal entre le sommet de départ identifié par id_sommet_départ et tous les sommets du graphe avec mémorisation des pères.
        :rtype: List
         
        """
        
        # Création de la liste des distances au sommet de départ
        liste_distance=self.nb_sommets*[math.inf]
        liste_distance[id_sommet_depart]=0
        # Création de la liste des pères par chemin minimal
        liste_peres=self.nb_sommets*[None]
        # Céation de la liste des numéros de sommets à explorer
        Liste_id_sommets_a_explorer=[id_sommet_depart]
        while Liste_id_sommets_a_explorer != []:
            id_sommet_a_explorer = Liste_id_sommets_a_explorer [0]
            #Liste_id_sommets_voisins = self.liste_id_sommets_voisins(id_sommet_a_explorer)
            ##on a la liste des arcs... donc on connais les voisins...
            for arc in self.liste_adj_arcs[id_sommet_a_explorer]:
                distance_sommet_a_explorer=liste_distance[id_sommet_a_explorer]
                id_sommet_voisin= arc.destination.id
                distance_sommet_voisin=liste_distance[id_sommet_voisin]
                distance_sommet_a_voisin = arc.inverse_capacite_res()  # ici les capacite des arcs sont remplacés par leurs inverses
                if distance_sommet_voisin > distance_sommet_a_explorer + distance_sommet_a_voisin :
                    # maj de la liste des distances
                    liste_distance[id_sommet_voisin] = distance_sommet_a_explorer + distance_sommet_a_voisin
                    # maj de la liste des sommets à explorer
                    Liste_id_sommets_a_explorer.append(id_sommet_voisin)
                    # maj de la liste des peres
                    liste_peres[id_sommet_voisin]=id_sommet_a_explorer
            Liste_id_sommets_a_explorer.remove(id_sommet_a_explorer)
            #print(liste_peres)
            #print(liste_distance)
        return liste_distance,liste_peres  
    
    def cherche_chemin_maximal_sommets(self,id_sommet_depart,id_sommet_arrivee):
        """ 

        :param self: Objet Graphe  où la liste des sommets a déjà été créée
        :type self: Graphe
        
        :param id_sommet_depart: indice d'un sommet dans la liste des sommets self.liste_sommets.
        :type id_sommet_depart: int  
        
        :param id_sommet_arrivee: indice d'un sommet dans la liste des sommets self.liste_sommets.
        :type id_sommet_arrivee: int 
        
        :return: la liste des sommets formant un chemin pseudo_maximal non infini du sommet de id_sommet_depart au sommet de  id_sommet_arrivee s'il en existe un, liste contenant uniquement le sommet d'arrivée sinon.
        :rtype: List
        
        """
        
        Liste_distance,Liste_peres=self.distances_maximales(id_sommet_depart)
        chemin=[id_sommet_arrivee]
        id_sommet=id_sommet_arrivee
        while id_sommet!=id_sommet_depart and id_sommet!=None:
            id_sommet=Liste_peres[id_sommet]
            if id_sommet!=None:
                chemin.insert(0,id_sommet)
        chemin.insert(0,id_sommet)
        return(chemin)
    
    def cherche_chemin_maximal_arcs(self,id_sommet_depart,id_sommet_arrivee):
        """ 

        :param self: Objet Graphe  où la liste des sommets a déjà été créée
        :type self: Graphe
        
        :param id_sommet_depart: indice d'un sommet dans la liste des sommets self.liste_sommets.
        :type id_sommet_depart: int  
        
        :param id_sommet_arrivee: indice d'un sommet dans la liste des sommets self.liste_sommets.
        :type id_sommet_arrivee: int 
        
        :return: la liste des arcs formant un chemin pseudo-maximal non infini du sommet de id_sommet_depart au sommet de  id_sommet_arrivee s'il en existe un, liste vide sinon.
        :rtype: List
        
        """
        
        # print("recherche chemin max arc")
        # print(id_sommet_depart)
        # print(id_sommet_arrivee)
        Liste_distance,Liste_peres=self.distances_maximales(id_sommet_depart)
        chemin=[]
        id_sommet=id_sommet_arrivee
        while id_sommet!=id_sommet_depart and id_sommet!=None:
            #print("no sommet = %d"%id_sommet)
            id_pere=Liste_peres[id_sommet]
            #print(id_pere)
            if id_pere!=None:
                arc=self.arc_de_sommet1_a_sommet2(id_pere,id_sommet)
                chemin.insert(0,arc)
            id_sommet=id_pere
            
        # affichage debug
        #print("chemin : ",chemin)
        #print("liste peres : ", Liste_peres)
        #print("longueur liste peres : ", len(Liste_peres))
        return(chemin)
        
    # def cherche_chemin_maximal(self,id_sommet_depart,id_sommet_arrivee):
    #     Liste_distance=self.distances_maximales(id_sommet_depart)
    #     Liste_sommets=[]

    ## FONCTIONS POUR LA RECHERCHE DE CHEMIN version 2
    
    def parcours_largeur(self,id_sommet_depart,id_sommet_arrivee):
        """ 
        
        Mise à jour des pères dans le champ pere de chaque sommet.
         
        :param self: Objet Graphe  où la liste des sommets a déjà été créée
        :type self: Graphe
        
        :param id_sommet_depart: indice d'un sommet dans la liste des sommets self.liste_sommets.
        :type id_sommet_depart: int 
        
        :param id_sommet_arrivee: indice d'un sommet dans la liste des sommets self.liste_sommets.
        :type id_sommet_arrivee: int 
        
        :return: la liste [liste_peres, booléen ]. Elle est composée de la liste des pères indexées par les no de sommet correspondant . Résultats obtenus par la méthode de parcours en largeur du graphe à partir du sommet de départ identifié par id_sommet_départ pour arriver au id_sommet_arrivee. Mémorisation des pères des sommets rencontré (c'est comme si on considérait que tous les arcs ont un capacite de 1). Booléen est True si id_sommet_arrivee est atteint et False sinon
        :rtype: List*bool
        
        """
        
        # Création de la liste des distances au sommet de départ
        # sommet.pere initialisés à None lors de leurs créations
        for sommet in self.liste_sommets:
            sommet.pere=-1
            sommet.etat=-1
        
        # Création de la liste des pères par chemin minimal
        liste_peres=self.nb_sommets*[-1]
        
        # Céation de la liste des numéros de sommets à explorer
        Liste_id_sommets_a_explorer=[id_sommet_depart]
        
        # mise de la distance du sommet de départ à lui même à zéro
        self.liste_sommets[id_sommet_depart].dist=0
        
        while Liste_id_sommets_a_explorer != []:
            
            id_sommet_a_explorer = Liste_id_sommets_a_explorer.pop(0)
            sommet_a_explorer=self.liste_sommets[id_sommet_a_explorer]
            sommet_a_explorer.etat=1
            
            for arc in self.liste_adj_arcs[id_sommet_a_explorer]:
                
                id_sommet_voisin = arc.destination.id
                sommet_voisin = self.liste_sommets[id_sommet_voisin]
                
                if arc.capacite_res > 0 and liste_peres[id_sommet_voisin]==-1 :
                # if arc.capacite_res > 0 and sommet_voisin.pere==-1 :
                
                    # maj de la liste des peres et du voisin (père et état à 0 = visité)
                    liste_peres[id_sommet_voisin]=id_sommet_a_explorer
                    sommet_voisin.pere=sommet_a_explorer
                    sommet_voisin.etat=0
                    
                    # si le sommet voisin est le sommet d'arrivée
                    if id_sommet_voisin == id_sommet_arrivee : 
                        print("il y a un chemin")
                        print(liste_peres)
                        return (liste_peres,True)
                        
                    # maj de la liste des sommets à explorer
                    Liste_id_sommets_a_explorer.append(id_sommet_voisin)
        print("pas de chemin")
        return (liste_peres,False) 
        
    def cherche_chemin_PL_depuis_arrivee(self,id_sommet_depart,id_sommet_arrivee):
        """ 

        :param self: Objet Graphe 
        :type self: Graphe
        
        :param id_sommet_depart: indice d'un sommet dans la liste des sommets self.liste_sommets.
        :type id_sommet_depart: int  
        
        :param id_sommet_arrivee: indice d'un sommet dans la liste des sommets self.liste_sommets.
        :type id_sommet_arrivee: int 
        
        :return: la liste des arcs formant un chemin du sommet de id_sommet_depart au sommet de id_sommet_arrivee s'il en existe un, [] sinon
        :rtype: List
        
        """
        
        # print("recherche chemin max arc")
        # print(id_sommet_depart)
        # print(id_sommet_arrivee)
        Liste_peres,B = self.parcours_largeur(id_sommet_depart,id_sommet_arrivee)
        chemin=[]
        id_sommet=id_sommet_arrivee
        while id_sommet!=id_sommet_depart and id_sommet!=None:
            #print("no sommet = %d"%id_sommet)
            id_pere=Liste_peres[id_sommet]
            #print(id_pere)
            if id_pere!=None:
                arc=self.arc_de_sommet1_a_sommet2(id_pere,id_sommet)
                chemin.insert(0,arc)
            id_sommet=id_pere
            
        # affichage debug
        #print("chemin : ",chemin)
        #print("liste peres : ", Liste_peres)
        #print("longueur liste peres : ", len(Liste_peres))
        return(chemin,B)

   ## FONCTIONS DE RECHERCHE DE MINIMUM DANS UN CHEMIN    
    def capacite_res_min_dans_chemin(self,chemin):
        """ 
        
        :param self: Objet Graphe  où la liste des sommets a déjà été créée
        :type self: Graphe
        
        :param chemin: liste d'arcs
        :type chemin: List  

        :return: le capacite minimal des arcs contenus dans chemin
        :rtype: int
        
        """
        
        min=math.inf
        for arc in chemin:
            if arc.capacite_res<min:
                min=arc.capacite_res
        return min

   ## FONCTIONS POUR L'ALGORITHME DE GRAPH CUT  version 1      
    def ford_fulkerson(self,id_sommet_depart,id_sommet_arrivee):
        """ 
        Procédure qui modifie le graphe par l'Algorithme de Ford-Fulkerson avec recherche de chemin par Bellmann-Ford inverse. Seuls les arcs du chemin trouvé sont modifiés. Le poid minimal des arcs du chemin est retiré de tous les arcs du chemin et retiré de tous les arcs inverses possibles (c'est à dire qui ne partent pas de source ou ne reviennent pas vers destination).  
        
        :param self: Objet Graphe complètement renseigné
        :type self: Graphe
        
        :param id_sommet_depart: indice d'un sommet dans la liste des sommets self.liste_sommets.
        :type id_sommet_depart: int  
        
        :param id_sommet_arrivee: indice d'un sommet dans la liste des sommets self.liste_sommets.
        :type id_sommet_arrivee: int 
        
        :return: None 
        :rtype: None object
        
        """
        
        chemin=self.cherche_chemin_maximal_arcs(id_sommet_depart,id_sommet_arrivee)
        #i=0
        # le chemin est vide si le sommet d'arrivée n'est pas atteint
        while chemin!=[]:
            #affichage debug
            #print(i)
            m=self.capacite_res_min_dans_chemin(chemin)
            for arc in chemin:
                arc.capacite_res-=m
                if not (arc.origine.id==id_sommet_depart or arc.destination.id==id_sommet_arrivee):
                    self.arc_inverse(arc).capacite_res+=m
            chemin=self.cherche_chemin_maximal_arcs(id_sommet_depart,id_sommet_arrivee)
            #i+=1
    
    def liste_id_sommets_atteignables_from_liste_distances(self,liste_distances):
        """
        :param self: Objet Graphe complètement renseigné
        :type self: Graphe
        
        :param liste_distances: liste des distances à la source avec comme indice l'indice/le numéro de sommet.
        :type liste_distances: List  

        :return: la liste des indices dans liste_sommets(des numéros de sommet) des sommets atteignables à partir de la source
        :rtype: List
        
        """
        
        liste_id_sommets_atteignables=[]
        for id_sommet in range(self.nb_sommets):
            if liste_distances[id_sommet]<math.inf:
                liste_id_sommets_atteignables.append(id_sommet)
        return liste_id_sommets_atteignables
        
    def liste_id_sommets_atteignables_capacite_R(self,id_sommet,Liste):
        
        """
        Procédure récursive qui enrichi la liste 'Liste' des indices dans liste_sommets(des numéros de sommet) des sommets atteignables à partir de la source avec les voisins du sommet indiqué par id_sommet tels que la capacité des arcs qui les lie est strictement positive.
        
        :param self: Objet Graphe complètement renseigné
        :type self: Graphe
        
        :param id_sommet: indice du numéro de sommet de départ pour l'exploration des arêtes.
        :type id_sommet: int
        
        :param Liste: liste des numéro de sommet atteignable déjà parcourus.
        :type Liste: List

        :return: None
        :rtype: None
        
        """
        
        Liste.append(id_sommet)
        #print(id_sommet, end=";")
        if id_sommet != self.id_H_puits:
            for arc in self.liste_adj_arcs[id_sommet]:
                if arc.capacite_res>0 and not arc.destination.id in Liste :
                    self.liste_id_sommets_atteignables_capacite_R(arc.destination.id,Liste)
        else:
            return (Liste)
    
    
    def liste_id_sommets_atteignables_capacite_R2(self,id_sommet,Liste):
        
        """
        Procédure récursive qui enrichi la liste 'Liste' des indices dans liste_sommets(des numéros de sommet) des sommets atteignables à partir de la source avec les voisins du sommet indiqué par id_sommet tels que la capacité des arcs qui les lie est strictement positive.
        
        :param self: Objet Graphe complètement renseigné
        :type self: Graphe
        
        :param id_sommet: indice du numéro de sommet de départ pour l'exploration des arêtes.
        :type id_sommet: int
        
        :param Liste: liste des numéro de sommet atteignable déjà parcourus.
        :type Liste: List

        :return: None
        :rtype: None
        
        """
        
        Liste.append(id_sommet)
        #print(id_sommet, end=";")
        if id_sommet != self.id_H_puits:
            for id_voisin in self.liste_adj_id_voisins[id_sommet]:
                #if arc.capacite_res>0 and not arc.destination.id in Liste :
                if id_voisin not in Liste :  
                    self.liste_id_sommets_atteignables_capacite_R(id_voisin,Liste)
        else:
            return (Liste)
                
        
    def liste_id_sommets_atteignables_capacite_res(self):
        
        """
        
        :param self: Objet Graphe complètement renseigné
        :type self: Graphe

        :return: la liste L des indices dans liste_sommets(des numéros de sommet) des sommets atteignables à partir de la source avec les voisins du sommet indiqué par id_sommet tels que la capacité des arcs qui les lie est strictement positive.
        :rtype: List
        
        """
        
        L=[]
        self.liste_id_sommets_atteignables_capacite_R(self.id_H_source,L)
        print()
        print(len(L))
        return L
        
    # def liste_id_sommets_atteignables_from_capacite(self):
    #     """
    #     :param self: Objet Graphe complètement renseigné
    #     :type self: Graphe

   ##       :return: la liste des indices dans liste_sommets(des numéros de sommet) des sommets atteignables à partir de la source
    #     :rtype: List
    #     
    #     """
    #     
    #     liste_id_sommets_atteignables=[]
    #     for id_sommet in range(self.nb_sommets):
    #         for arc in self.liste_adj_arcs[id_sommet]:
    #             if arc.capacite_res>0:
    #                 liste_id_sommets_atteignables.append(id_sommet)
    #     return liste_id_sommets_atteignables
        
    def liste_id_sommets_2_liste_coordonnees(self,liste_id_sommets):
        """
        
        :param self: Objet Graphe complètement renseigné
        :type self: Graphe
        
        :param liste_id_sommets: liste de numéro de sommet.
        :type liste_id_sommets: List  

        :return: la liste des cordonnée des sommets dont les numéros se trouvent dans liste_id_sommets (liste des numéros de sommet des sommets atteignables à partir de la source)
        :rtype: List
        
        """
        
        liste_sommets=[]
        for id_sommet in liste_id_sommets:
            x,y=self.id_sommet_2_coordonnees(id_sommet)
            liste_sommets.append([x,y])
        return liste_sommets    

    def graph_cut(self):
        
        """
        
        :param self: Objet Graphe complètement renseigné
        :type self: Graphe

        :return: la liste des cordonnées des sommets atteignables à partir de l'hypersource du graphe après applcation de Frod-Fulkerson/Bellman-Ford)
        :rtype: List
        
        """
        print("dans graph_cut lance FF")
        self.ford_fulkerson(self.id_H_source,self.id_H_puits)
        self.supprime_arcs_capacite_res_nulle()
        
        #liste_distance,Liste_peres=self.distances_minimales(self.id_H_source)
        #print(liste_distance)
        
        print("Calcul Sommets atteignables !")
        
        #liste_id_sommets_atteignables=self.liste_id_sommets_atteignables_from_liste_distances(liste_distance)
        liste_id_sommets_atteignables=self.liste_id_sommets_atteignables_capacite_res()
        
        print(liste_id_sommets_atteignables)
        
        liste_sommets_atteignables = self.liste_id_sommets_2_liste_coordonnees(liste_id_sommets_atteignables)
        return liste_sommets_atteignables      
        
   ## FONCTIONS POUR L'ALGORITHME DE GRAPH CUT  version 2      
    def edmond_karp_0(self,id_sommet_depart,id_sommet_arrivee):
        """ 
        Procédure qui modifie le graphe par l'Algorithme de Ford-Fulkerson avec recherche de chemin par parcours en largeur. Seuls les arcs du chemin trouvé sont modifiés. Le poid minimal des arcs du chemin est retiré de tous les arcs du chemin et retiré de tous les arcs inverses possibles (c'est à dire qui ne partent pas de source ou ne reviennent pas vers destination).  
        
        :param self: Objet Graphe complètement renseigné
        :type self: Graphe
        
        :param id_sommet_depart: indice d'un sommet dans la liste des sommets self.liste_sommets.
        :type id_sommet_depart: int  
        
        :param id_sommet_arrivee: indice d'un sommet dans la liste des sommets self.liste_sommets.
        :type id_sommet_arrivee: int 
        
        :return: None 
        :rtype: None object
        
        """
        
        print("EK_0")
        #les flots sont initialisés à 0 lors de la création des arcs
        chemin,B=self.cherche_chemin_PL_depuis_arrivee(id_sommet_depart,id_sommet_arrivee)
        i=0
        # le chemin est vide si le sommet d'arrivée n'est pas atteint
        while B:
            #affichage debug
            print("etape %d"%i)
            m=self.capacite_res_min_dans_chemin(chemin)
            for arc in chemin:
                arc.diminue_capacite_res(m)
                arc.augmente_flot(m)
                if not (arc.origine.id==id_sommet_depart or arc.destination.id==id_sommet_arrivee):
                    arc_inverse=self.arc_inverse(arc)
                    arc_inverse.augmente_capacite_res(m)
                    arc_inverse.diminue_flot(m)
                    # print("arc et inv")
                    # arc.affiche_arc()
                    # arc_inverse.affiche_arc()
                #     if arc_inverse.capacite_res==0:
                #         self.supprime_arc(arc_inverse)
                # if arc.capacite_res==0:
                #     self.supprime_arc(arc)    
            chemin,B=self.cherche_chemin_PL_depuis_arrivee(id_sommet_depart,id_sommet_arrivee)
            i+=1
            
    def graph_cut_2(self):
        
        """
        
        :param self: Objet Graphe complètement renseigné
        :type self: Graphe

        :return: la liste des cordonnées des sommets atteignables à partir de l'hypersource du graphe après applcation de Edmond-Karp)
        :rtype: List
        
        """
        print("debut EK0")
        self.edmond_karp_0(self.id_H_source,self.id_H_puits)
        
        # print(" affiche arcs")
        # #print(self.liste_adj_arcs)
        # print(len(self.liste_adj_arcs))
        # for l in self.liste_adj_arcs:
        #     for a in l:
        #         a.affiche_arc()
        #     print()
        
        #self.supprime_arcs_capacite_res_nulle()
        self.supprime_arcs_capacite_res_nulle()
        
        # print(" affiche arcs sans capa res nulle")
        # #print(self.liste_adj_arcs)
        # print(len(self.liste_adj_arcs))
        # for l in self.liste_adj_arcs:
        #     for a in l:
        #         a.affiche_arc()
        #     print()
        
        # print("liste adj id sommets")
        # for id in range(self.nb_sommets):
        #     print(id,end=" ; ")
        #     print(self.liste_adj_id_voisins[id])

        print(" ************* affiche arcs sans capa res nulle")
        #print(self.liste_adj_arcs)
        print(len(self.liste_adj_arcs))
        for l in self.liste_adj_arcs:
            for a in l:
                print(id,end=" ; ")
                a.affiche_arc()
            print()
        
        # liste_distance,Liste_peres=self.distances_minimales(self.id_H_source)
        # print(liste_distance)
        
        print("Calcul Sommets atteignables !")
        
        #liste_id_sommets_atteignables=self.liste_id_sommets_atteignables_from_liste_distances(liste_distance)
        liste_id_sommets_atteignables=self.liste_id_sommets_atteignables_capacite_res()
        
        #print(liste_id_sommets_atteignables)
        
        liste_sommets_atteignables = self.liste_id_sommets_2_liste_coordonnees(liste_id_sommets_atteignables)
        print("Fin graph cuts")
        return liste_sommets_atteignables 
                
    def edmond_karp(self,id_sommet_depart,id_sommet_arrivee):
        """ 
        Procédure qui modifie le graphe par l'Algorithme de Ford-Fulkerson avec recherche de chemin par parcours en largeur. Seuls les arcs du chemin trouvé sont modifiés. Le poid minimal des arcs du chemin est retiré de tous les arcs du chemin et retiré de tous les arcs inverses possibles (c'est à dire qui ne partent pas de source ou ne reviennent pas vers destination).  
        
        :param self: Objet Graphe complètement renseigné
        :type self: Graphe
        
        :param id_sommet_depart: indice d'un sommet dans la liste des sommets self.liste_sommets.
        :type id_sommet_depart: int  
        
        :param id_sommet_arrivee: indice d'un sommet dans la liste des sommets self.liste_sommets.
        :type id_sommet_arrivee: int 
        
        :return: None 
        :rtype: None object
        
        """
        #les flots sont initialisés à 0 lors de la création des arcs
        print("EK")
        liste_peres,B=self.parcours_largeur(self.id_H_source, self.id_H_puits)
        print(len(liste_peres))
        i=0
        # le chemin est vide si le sommet d'arrivée n'est pas atteint
        while B:
            #affichage debug
            print("etape %d"%i)
            m=math.inf
            id_sommet=self.id_H_puits
            while id_sommet!=self.id_H_source:
                sommet=self.liste_sommets[id_sommet]
                id_pere=liste_peres[id_sommet]
                arc=self.arc_de_sommet1_a_sommet2(id_pere, sommet.id)
                if arc.capacite_res<m:
                    m = arc.capacite_res
                id_sommet=id_pere
            print("mini = %d"%m)    
            id_sommet=self.id_H_puits    
            while id_sommet!=self.id_H_source: 
                id_pere=liste_peres[id_sommet]
                arc=self.arc_de_sommet1_a_sommet2(id_pere,id_sommet)
                arc.affiche_arc()
                arc.augmente_flot(m)
                arc.diminue_capacite_res(m)
                if not (arc.origine.id==id_sommet_depart or arc.destination.id==id_sommet_arrivee):
                    arc_inverse=self.arc_inverse(arc)
                    arc_inverse.augmente_capacite_res(m)
                    arc_inverse.diminue_flot(m)
                    # print("arc et inv")
                    # arc.affiche_arc()
                    # arc_inverse.affiche_arc()                    
                id_sommet=sommet.pere.id

                #     if arc_inverse.capacite_res==0:
                #         self.supprime_arc(arc_inverse)
                # if arc.capacite_res==0:
                #     self.supprime_arc(arc) 
                
            liste_peres,B=self.parcours_largeur(self.id_H_source, self.id_H_puits)
            i+=1
            
    def graph_cut_3(self):
        
        """
        
        :param self: Objet Graphe complètement renseigné
        :type self: Graphe

        :return: la liste des cordonnées des sommets atteignables à partir de l'hypersource du graphe après applcation de Edmond-Karp)
        :rtype: List
        
        """
        print("debut EK")
        self.edmond_karp(self.id_H_source,self.id_H_puits)
        
        # print(" affiche arcs")
        # #print(self.liste_adj_arcs)
        # print(len(self.liste_adj_arcs))
        # for l in self.liste_adj_arcs:
        #     for a in l:
        #         a.affiche_arc()
        #     print()
        
        #self.supprime_arcs_capacite_res_nulle()
        self.supprime_arcs_capacite_res_nulle()
        
        # print(" affiche arcs sans capa res nulle")
        # #print(self.liste_adj_arcs)
        # print(len(self.liste_adj_arcs))
        # for l in self.liste_adj_arcs:
        #     for a in l:
        #         a.affiche_arc()
        #     print()
        
        # print("liste adj id sommets")
        # for id in range(self.nb_sommets):
        #     print(id,end=" ; ")
        #     print(self.liste_adj_id_voisins[id])

        print(" ************* affiche arcs sans capa res nulle")
        #print(self.liste_adj_arcs)
        print(len(self.liste_adj_arcs))
        i=0
        for l in self.liste_adj_arcs:
            print("%d"%i,end=" ; ")
            for a in l:
                a.affiche_arc()
            i+=1
            print()
        
        # liste_distance,Liste_peres=self.distances_minimales(self.id_H_source)
        # print(liste_distance)
        
        print("Calcul Sommets atteignables !")
        
        #liste_id_sommets_atteignables=self.liste_id_sommets_atteignables_from_liste_distances(liste_distance)
        liste_id_sommets_atteignables=self.liste_id_sommets_atteignables_capacite_res()
        
        #print(liste_id_sommets_atteignables)
        
        liste_sommets_atteignables = self.liste_id_sommets_2_liste_coordonnees(liste_id_sommets_atteignables)
        print("Fin graph cuts")
        return liste_sommets_atteignables  