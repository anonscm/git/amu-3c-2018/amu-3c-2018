#####################################
# Cecile Prouteau
# CTES Projet Math-Info
# Projet segmentation d'image
# 15/01/2019
# Classe Application Version 1.4
#####################################

### IMPORTATION DES BIBLIOTHEQUES 
import tkinter.filedialog as tkfd
import tkinter as tk
import numpy as np
# import matplotlib.pyplot as plt
# import matplotlib.image as mpimg
# import time

### IMPORTATION DES CLASSES
from Image import Image
from Graphe import Graphe
from Sommet import Sommet

### Application d'affichage principale ( FENETRE TKINTER )

class Application(object):
    """
    
    **AUTRES CLASSES IMPORTEES**
    
    * Image
    * Graphe
    * Sommet
    * tkinter
    * tkinter.filedialog
    * numpy
   
    
    **LES METHODES**
     
    
    """
    
    def __init__(self):
        """
        Constructeur de l'objet fenêtre principale de l'application
        
        :param self: Objet Application  
        :type self: Application
        
        :return: Objet Application
        :rtype: Application
        
        Liste des champs de la classe
        
        * self.filepath Adresse du fichier image sur la machine
        * self.photo Ouverture du fichier image a traiter au format PNG niveau de gris et stockage dans la variable photo
        * self.img Instance d'un objet Image créé à partir du fichier ci dessus
        * self.Liste_points Liste des points de repérage des zones
        * self.Frame1
        * self.Frame2
        * self.Frame3
        * self.Frame4
        * self.Frame5
        * self.Frame5T
        * self.Frame5T1
        * self.Frame5T2
        * self.Frame5B
        * self.Frame5B1
        * self.Frame5B2
        * self.Frame6
        * self.Frame7
        * self.Frame8
        * self.no_algo Numéro de l'algorithme à appliquer 
        * self.bouton1 Bouton radio de sélection de l'algorithme 1 (Ford Fulkerson)
        * self.bouton2 Bouton radio de sélection de l'algorithme 2 (Partage des eaux)
        * self.Lambda le paramètre Lambda servira d'argument aux algorithme de graph cut en cas de besoin
        * self.scale curseur permettant de déterminer le paramètre Lambda.
        * self.sv_obj Elément StringVar de Tkinter permettant l'affichage de la liste des points de l'objet selectionnes dans la fenetre
        * self.bouton_init_obj Bouton de reinitialisation de la liste des points de l'objet a vide
        * self.sv_bkg Elément StringVar de Tkinter permettant l'affichage de la liste des points de l'arrière plan selectionnes dans la fenetre
        * self.bouton_init_bkg Bouton de reinitialisation de la liste des points de l'arrière plan a vide
        * self.canvas Canevas d'affichage du fichier image
        * self.bouton_importation Bouton d'importation d'une nouvelle image
        * self.bouton_calcul Bouton de lancement calcul des zones
        * self.bouton_affichage Bouton d'affichage des zones segmentées
        * self.bouton_exportation Bouton d'exportation
        * self.bouton_sortie Bouton pour quitter l'application
        * self.fenetre Fenêtre d'affichage
        """
        
        ## Creation de la fenetre
        self.fenetre = tk.Tk()
        self.fenetre.title("SEGMENTATION D'IMAGE")
        
        ## Initialisation des variables de classe

        # Selection du fichier image a traiter au format PNG niveau de gris a l'aide d'une fenetre de selection
        self.filepath = tkfd.askopenfilename(title="Ouvrir une image",filetypes=[('png files','.png'),('all files','.*')])
        
        # Ouverture du fichier image a traiter au format PNG niveau de gris et stockage dans la variable photo
        self.photo = tk.PhotoImage(file=self.filepath)
        
        # Creation d'une instance de Image à partir du fichier
        self.img=Image(self.filepath)
        
        # Liste des points de repérage des zones objet
        self.Liste_points_obj=[]
        
        # Liste des points de repérage des zones arrière plan
        self.Liste_points_bkg=[]
        
        ## Label de la fenetre principale 'fenetre'
        # self.label = tk.Label(self.fenetre, text="SEGMENTATION D'IMAGE",bg="ivory",font=("Helvetica", 28))
        # self.label.pack()
        
        ## Organisation des Frames
        # Frame1 dans fenetre
        self.Frame1 = tk.Frame(self.fenetre, borderwidth=0, relief=tk.GROOVE)
        self.Frame1.pack(side=tk.LEFT, padx=10, pady=10)
        
        # Frame2 dans fenetre
        self.Frame2 = tk.Frame(self.fenetre, borderwidth=0, relief=tk.GROOVE)
        self.Frame2.pack(side=tk.BOTTOM, padx=5, pady=5)
        
        # Frame3 dans Frame2
        self.Frame3 = tk.Frame(self.Frame2, borderwidth=0, relief=tk.GROOVE)
        self.Frame3.pack(side=tk.TOP, padx=5, pady=5)
        
        # Frame 4 dans Frame2
        self.Frame4 = tk.Frame(self.Frame2, bg="white", borderwidth=0, relief=tk.GROOVE)
        self.Frame4.pack(side=tk.BOTTOM, padx=40, pady=40)
        
        # Frame 5 dans Frame 4
        self.Frame5 = tk.Frame(self.Frame2, borderwidth=0, relief=tk.GROOVE)
        self.Frame5.pack(side=tk.TOP, padx=5, pady=5)
        
        # Frame 5T dans Frame 5
        self.Frame5T = tk.Frame(self.Frame5, borderwidth=0, relief=tk.GROOVE)
        self.Frame5T.pack(side=tk.TOP, padx=5, pady=5)
        
        # Frame 5B dans Frame 5
        self.Frame5B = tk.Frame(self.Frame5, borderwidth=0, relief=tk.GROOVE)
        self.Frame5B.pack(side=tk.BOTTOM, padx=5, pady=5)

        # Frame 5T1 dans Frame 5T
        self.Frame5T1 = tk.Frame(self.Frame5T, borderwidth=0, relief=tk.GROOVE)
        self.Frame5T1.pack(side=tk.TOP, padx=5, pady=5)
        
        # Frame 5T2 dans Frame 5T
        self.Frame5T2 = tk.Frame(self.Frame5T, borderwidth=0, relief=tk.GROOVE)
        self.Frame5T2.pack(side=tk.BOTTOM, padx=5, pady=5)

        # Frame 5B1 dans Frame 5B
        self.Frame5B1 = tk.Frame(self.Frame5B, borderwidth=0, relief=tk.GROOVE)
        self.Frame5B1.pack(side=tk.TOP, padx=5, pady=5)  
        
        # Frame 5B2 dans Frame 5B
        self.Frame5B2 = tk.Frame(self.Frame5B, borderwidth=0, relief=tk.GROOVE)
        self.Frame5B2.pack(side=tk.BOTTOM, padx=5, pady=5)        
        
        # Frame 6 dans Frame 4
        self.Frame6 = tk.Frame(self.Frame4, borderwidth=0, relief=tk.GROOVE)
        self.Frame6.pack(side=tk.BOTTOM, padx=5, pady=5)
        
        # Frame 7 dans Frame 6
        self.Frame7 = tk.Frame(self.Frame6, borderwidth=0, relief=tk.GROOVE)
        self.Frame7.pack(side=tk.LEFT, padx=5, pady=5)
        
        # Frame 8 dans Frame 6
        self.Frame8 = tk.Frame(self.Frame6, borderwidth=0, relief=tk.GROOVE)
        self.Frame8.pack(side=tk.RIGHT, padx=5, pady=5)
        
        ## Ajout de labels pour les Frames
        tk.Label(self.Frame1, text="CHOIX DES PARAMETRES",font=("Helvetica", 10, 'bold')).pack(padx=10, pady=10)
        tk.Label(self.Frame3, text="LISTE DES POINTS SELECTIONNES",font=("Helvetica", 10, 'bold')).pack(padx=10, pady=10,side="top")
        tk.Label(self.fenetre, text="Cliquez sur l'image avec le bouton droit de la souris pour sélectionner les points définissant les zones objet et ctrl-bouton droit pour déterminer les points de l'arrière plan").pack(padx=10, pady=10)
        tk.Label(self.Frame5T1, text="Objets",font=("Helvetica", 8, 'bold')).pack(padx=10, pady=10)
        tk.Label(self.Frame5B1, text="Arrière plan",font=("Helvetica", 8, 'bold')).pack(padx=10, pady=10)

        ## Ajouts des Widgets

        # Parametre de choix d'algorithme
        # radiobutton
        self.no_algo = tk.StringVar() 
        self.bouton1 = tk.Radiobutton(self.Frame1, text="Ford-Fulkerson-Bellman-Ford", variable=self.no_algo, value=1) 
        self.bouton2 = tk.Radiobutton(self.Frame1, text="Ford-Fulkerson-Parcours-Largeur", variable=self.no_algo, value=2)
        self.bouton3 = tk.Radiobutton(self.Frame1, text="Partage des eaux", variable=self.no_algo, value=3)
        self.bouton1.pack()
        self.bouton2.pack()
        self.bouton3.pack()
        
        # Parametre Lambda determine a l'aide d'un curseur
        self.Lambda = tk.DoubleVar()
        self.scale = tk.Scale(self.Frame1, orient='horizontal', from_=0, to=10, resolution=0.1, tickinterval=2, length=350, variable=self.Lambda, label='Lambda')
        self.scale.pack()
        
        # Affichage de la liste des points objets selectionnes dans la fenetre
        self.sv_obj = tk.StringVar()
        self.Frame5T = tk.LabelFrame(self.Frame5, text="Liste des points objets sélectionnés", padx=20, pady=20)
        self.Frame5T.pack(fill="both", expand="yes")
        self.sv_obj.set(str(self.Liste_points_obj))
        tk.Label(self.Frame5T1, textvariable=self.sv_obj).pack()

        # bouton de reinitialisation de la liste des points objets a vide 
        self.bouton_init_obj = tk.Button(self.Frame5T2, text="Réinitialisation de la liste des points de l'objet", command = self.liste_obj_nulle )
        self.bouton_init_obj.pack()    
        
        # en cas de clik droit de la souris, appel de la fonction select_pixel_maj pour mise a jour de la liste objet et de l'affichage
        self.fenetre.bind("<Button-3>", self.select_pixel_obj_maj )
        
        # Affichage de la liste des points arrière plans selectionnes dans la fenetre
        self.sv_bkg = tk.StringVar()
        self.Frame5B = tk.LabelFrame(self.Frame5, text="Liste des points de l'arrière-plan sélectionnés", padx=20, pady=20)
        self.Frame5B.pack(fill="both", expand="yes")
        self.sv_bkg.set(str(self.Liste_points_bkg))
        tk.Label(self.Frame5B1, textvariable=self.sv_bkg).pack()
        
        # bouton de reinitialisation de la liste des points arrière plan a vide 
        self.bouton_init_bkg = tk.Button(self.Frame5B2, text="Réinitialisation de la liste des points de l'arrière plan", command = self.liste_bkg_nulle )
        self.bouton_init_bkg.pack()
        
        # en cas de ctrl-clik droit de la souris, appel de la fonction select_pixel_maj pour mise a jour de la liste arrière plan et de l'affichage
        self.fenetre.bind("<Control-Button-3>", self.select_pixel_bkg_maj )
        
        # Affichage du fichier image
        self.canvas = tk.Canvas(self.fenetre, width=self.photo.width(), height=self.photo.height(), bg="gray")
        self.canvas.create_image(0, 0, anchor=tk.NW, image=self.photo)
        self.canvas.pack()
        
        # Bouton d'importation
        self.bouton_importation=tk.Button(self.Frame7, text = "Importer", command = self.import_image)
        self.bouton_importation.pack(side="left")
        
        # Bouton de calcul
        # self.bouton_calcul=tk.Button(self.Frame7, text = "Calculer", command = self.calcul_zone_0) # essai selection listes points
        self.bouton_calcul=tk.Button(self.Frame7, text = "Calculer", command = self.calcul_zone)
        self.bouton_calcul.pack(side="left")
        
        # Bouton d'affichage
        self.bouton_affichage=tk.Button(self.Frame7, text = "Afficher", command = self.affiche_zone)
        self.bouton_affichage.pack(side="left")
        
        # Bouton d'exportation
        self.bouton_exportation=tk.Button(self.Frame7, text = "Exporter", command = self.exporte_image)
        self.bouton_exportation.pack(side="left")
        
        # Bouton de sortie
        # command = self.fenetre.quit fenetre.quit plante la 2eme fois - comment quitter l'application ? 
        self.bouton_sortie=tk.Button(self.Frame8, text = "Quitter", command = exit ) 
        self.bouton_sortie.pack(side="right")
        
        self.fenetre.mainloop()

    ## FONCTIONS ASSOCIEES AUX WIDGETS

    def liste_obj_nulle(self):
        """
        Fonction d'initialisation de la liste des points Liste_point_obj
        procedure sans argument qui remet la variable globale Listepoints a vide 
        et qui met a jour son affichage dans la fenetre graphique
        
        :param self: Objet Application  
        :type self: Application
        
        :return: None 
        :rtype: None object
        
        """
    
        global Liste_points_obj
        self.Liste_points_obj=[]
        self.sv_obj.set(str(self.Liste_points_obj))
        
    def liste_bkg_nulle(self):
        """
        Fonction d'initialisation de la liste des points Liste_point_bkg
        procedure sans argument qui remet la variable globale Listepoints a vide 
        et qui met a jour son affichage dans la fenetre graphique
        
        :param self: Objet Application  
        :type self: Application
        
        :param self: Objet Application  
        :type self: Application
        
        :return: None 
        :rtype: None object
        
        """
    
        global Liste_points_bkg
        self.Liste_points_bkg=[]
        self.sv_bkg.set(str(self.Liste_points_bkg))
     

    def select_pixel_obj(self,event):
        """ 
        Fonction de sélection de pixel objets a la souris
        Quand l'évènement évènement event (qui sera le click droit de la souris) se produit , collecte les coordonnees X,Y du point clique et ajoute [X,Y] à la liste Liste_point_obj (variable globale).
        
        :param self: Objet Application  
        :type self: Application
        
        :param event: Objet Event  
        :type event: Event
        
        :return: None 
        :rtype: None object
        
        """

        X=event.x
        Y=event.y
        if 0<=Y<self.img.hauteur and 0<=X<self.img.largeur:
            self.Liste_points_obj.append([X,Y])
        
    def select_pixel_bkg(self,event):
        """ 
        Fonction de sélection de pixel arrière plan a la souris
        Quand l'évènement évènement event (qui sera le click droit de la souris) se produit , collecte les coordonnees X,Y du point clique et ajoute [X,Y] à la liste Liste_point_bkg (variable globale).
        
        :param self: Objet Application  
        :type self: Application
        
        :param event: Objet Event  
        :type event: Event
        
        :return: None 
        :rtype: None object
        
        """

        X=event.x
        Y=event.y
        if 0<=Y<self.img.hauteur and 0<=X<self.img.largeur:
            self.Liste_points_bkg.append([X,Y])
    
    def select_pixel_obj_maj(self,event):
        """ 
        Gestion de la selection des points : quand l'évènement event (se sera click droit de la souris sur l'image) se produit , la liste Liste_points_obj est augmentee des coordonnees du point clique et l'affichage de cette liste est mise a jour. 
        
        :param self: Objet Application  
        :type self: Application
        
        :param event: Objet Event  
        :type event: Event
        
        :return: None 
        :rtype: None object
        
        """
    
        self.select_pixel_obj(event)
        self.sv_obj.set(str(self.Liste_points_obj))
        
    def select_pixel_bkg_maj(self,event):
        """ 
        Gestion de la selection des points de l'arrière plan : quand l'évènement event (se sera ctrl-click droit de la souris sur l'image) se produit , la liste Liste_points_bkg est augmentee des coordonnees du point clique et l'affichage de cette liste est mise a jour. 
        
        :param self: Objet Application  
        :type self: Application
        
        :param event: Objet Event  
        :type event: Event
        
        :return: None 
        :rtype: None object
        
        """
    
        self.select_pixel_bkg(event)
        self.sv_bkg.set(str(self.Liste_points_bkg))
        
    def import_image(self): 
        """
        .. todo:: ???en test ??? comment remplacer l'ancienne image par la nouvelle dans le canvas? Actuellement il reste un cadre gris. Peut-être raffraichir la fenetre recompletement ??? A chercher 
        
        :param self: Objet Application  
        :type self: Application
        
        :return: None 
        :rtype: None object
        
        """
        
        # # Selection du nouveau fichier image a traiter au format PNG niveau de gris a l'aide d'une fenetre de selection
        # self.filepath = tkfd.askopenfilename(title="Ouvrir une image",filetypes=[('png files','.png'),('all files','.*')])
        # 
        # # Ouverture du fichier image a traiter au format PNG niveau de gris et stockage dans la variable photo
        # self.photo = tk.PhotoImage(file=self.filepath)
        # 
        # # Affichage du fichier image
        # self.canvas.delete(tk.ALL)
        # self.canvas = tk.Canvas(self.fenetre, width=self.photo.width(), height=self.photo.height(), bg="gray")
        # self.canvas.create_image(0, 0, anchor=tk.NW, image=self.photo)
        # self.canvas.pack()
        print("Import de la nouvelle image !")
    
    def calcul_zone_0(self):
        """ 
        Calcul de la zone en fonction de la méthode choisie puis coloriage des points correspondants.
        
        :param self: Objet Application  
        :type self: Application
        
        :return: None 
        :rtype: None object
        
        """
        
        if self.no_algo.get()=='1':
            methode_choisie="des Graph Cuts Ford-Fulkerson  / Belmann-Ford"
            print("Calcul de la zone a segmenter par la méthode %s. " %methode_choisie)
        elif self.no_algo.get()=='2':
            methode_choisie="des Graph Cuts Ford-Fulkerson  / Parcours en largeur / Edmond-Karp"
            print("Calcul de la zone à segmenter par la méthode %s. " %methode_choisie)
        elif self.no_algo.get()=='3':
            methode_choisie="de partage des eaux"
            print("Calcul de la zone à segmenter par la méthode %s. " %methode_choisie)
        else : 
            print("Vous n'avez pas choisi de méthode. Veuillez faire un choix.")
        
        # Coloriage d'une liste de points sélectionnés au clavier sur le tableau de l'image a exporter. 
        # Permettra de colorier une zone segmentée ulterieurement.
        if self.no_algo.get()=='1' or self.no_algo.get()=='2' or self.no_algo.get()=='3':
            self.img.colorie_zone_img_RGB_np(self.Liste_points_obj,'red')
            self.img.colorie_zone_img_RGB_np(self.Liste_points_bkg,'blue')
    
    def calcul_zone(self): 
        """ 
        Calcul de la zone en fonction de la méthode choisie puis coloriage des points correspondants.
        
        :param self: Objet Application  
        :type self: Application
        
        :return: None 
        :rtype: None object
        
        """
        #creation du graphe de flot
        if self.no_algo.get()=='1' or self.no_algo.get()=='2' or self.no_algo.get()=='3':
            L_source=self.Liste_points_obj
            L_puits=self.Liste_points_bkg
            graphe_flot=Graphe(self.img,L_source,L_puits)
        
        #segmentation    
        if self.no_algo.get()=='1' :
            Liste_points_a_colorier=graphe_flot.graph_cut()
            print('Liste_points_a_colorier')
            print(Liste_points_a_colorier)
            self.img.colorie_zone_img_RGB_np(Liste_points_a_colorier,'blue')
        elif self.no_algo.get()=='2':
            Liste_points_a_colorier=graphe_flot.graph_cut_2()
            print('Liste_points_a_colorier')
            print(Liste_points_a_colorier)
            self.img.colorie_zone_img_RGB_np(Liste_points_a_colorier,'blue')    
        elif self.no_algo.get()=='3':
            Liste_points_a_colorier=graphe_flot.graph_cut_3()
            print('Liste_points_a_colorier')
            print(Liste_points_a_colorier)
            self.img.colorie_zone_img_RGB_np(Liste_points_a_colorier,'blue')    
            
    
    def affiche_zone(self):
        """ 
        Affichage de l'image RGB avec les zones coloriees.
        
        :param self: Objet Application  
        :type self: Application
        
        :return: None 
        :rtype: None object
        
        """
        
        #print("Affichage de la zone segmentée!")
        self.img.affiche_image_RGB()
    
    
    def exporte_image(self):
        """" 
        Exportation de l'image avec les zones coloriees au format png.
        
        :param self: Objet Application  
        :type self: Application
        
        :return: None 
        :rtype: None object
        
        """
        
        #print("Exportation du fichier de l'image segmentée au format png!")  
        if self.no_algo.get()=='1' or self.no_algo.get()=='2' or self.no_algo.get()=='3':
            self.img.export_image_2_file(self.no_algo.get()) 
        else:
            self.img.export_image_2_file('0')
  
### FIN DE LA FENETRE TKINTER / classe APPLICATION


###______Programme principal_______________________________________________________
if __name__ == '__main__':
    f = Application() # instanciation de l'objet application
    
    ## test des fonctions
    # # Creation d'une instance de Image
    # img=Image(f.filepath)
    # # Coloriage d'une liste de points sélectionnés aux clavier sur le tableau de l'image a exporter 
    # # permettra de colorier une zone segmentee ulterieurement
    # img.colorie_zone_img_RGB_np(f.Liste_points,'red')
    # # Affichage de l'image transformee 
    # img.affiche_image_RGB()
    # # Export de l'image transformee
    # img.export_image_2_file(f.no_algo.get())
    
    # Affichage de la liste_des points, de Lamdba et de No_algo
    # print(f.Liste_points)
    # print(f.Lambda.get())
    # print(f.no_algo.get())

