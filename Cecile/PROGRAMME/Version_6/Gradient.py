#####################################
# Cecile Prouteau
# CTES Projet Math-Info
# Projet segmentation d'image
# 27/04/2019
# Classe Gradient Version 0.1
#####################################

### IMPORTATION DES BIBLIOTHEQUES
import numpy as np
import math

### IMPORTATION DES CLASSES
from Image import Image
from Pixel import Pixel

### DEBUT de la classe Partage_eaux

class Gradient:
    
    """
    
    **AUTRES CLASSES IMPORTEES**
    
    * Image
    * Pixel
    * numpy
    * math
   
    
    **LES METHODES**
     
    
    """
    
    def __init__(self,image,gamma):
        
        """

        Constructeur d'un objet Gradient à partir de l'image 'image' selon l'élément structurant 'gamma'.

        :param self: Objet Gradient 
        :type self: Gradient
        
        :param image: objet Image est un tableau numpy d'une image codée sur une couche en niveaux de gris
        :type image: Image 
        
        :param gamma: liste correspondante à l'élément structurant (Gamma_4 ou Gamma_8)
        :type gamma: List 
        
        :return: Objet Gradient 
        :rtype: Gradient 
        
        Liste des champs de la classe
        
        * self.largeur largeur de l'image
        * self.hauteur hauteur de l'image
        * self.nb_pixels nombre de pixels de l'image
        
        * self.image = image au format Numpy
        * self.gamma = liste correspondante à l'élément structurant
        * self.tab image du gradient
        
        Pour les lignes et les colonnes je me place du point de vue de l'image. 
        Dans le tableau numpy, les coordonnées sont inversées.
        
        """
        
        #self.image=image.image_np_gray
        
        self.largeur = image.largeur
        self.hauteur = image.hauteur
        self.nb_pixels=image.largeur*image.hauteur
        
        self.image=image
        self.gamma=gamma
        self.tab = self.cree_gradient_image(image)


    ## FONCTIONS DE CALCUL DE GRADIENT
    
    def cree_gradient_image(self,image):
        
        """

        Création du tableau modélisant le gradient de l'image 'image' à mettre dans le champ tab d'un objet Gradient

        :param self: Objet Gradient 
        :type self: Gradient
        
        :param image: objet Image est un tableau numpy d'une image codée sur une couche en niveaux de gris
        :type image: Image 
        
        :return: tableau des pixels correspondant à l'image avec comme valeur celle du gradient.
        :rtype: List
        
        """
        
        tableau=self.largeur*[None]
        for x in range(self.largeur):
            tableau[x]=self.hauteur*[None]
        
        for x in range(self.largeur):
            for y in range(self.hauteur):
                L=self.calcule_liste_coord_pixels_voisins_depuis_coord(x,y)
                tableau[x][y]=Pixel(x,y,self.calcule_valeur_gradient_pixel_de_liste_voisins(L,image))
                tableau[x][y].maj_liste_coord_voisins(L)
        return tableau
        
    def calcule_valeur_gradient_pixel_de_liste_voisins(self,L,image):
        """
        Fonction qui calcule la valeur du gradient d'un pixel à partir de la liste des coordonnées de ses voisins
        
        :param self: Gradient
        :type self: Objet Gradient
        
        :param L: Liste des coordonnées des voisins pour calcul du gradient
        :type L: List
        
        :return: gradient d'un pixel
        :rtype: int
        
        """    
        
        min=255
        max=0
        for coord in L:
            valeur=image.couleur_entier_8bits(coord[1],coord[0])
            if valeur<min:
                min=valeur
            if valeur>max:
                max=valeur

        return max-min    
    
    
        
    def calcule_valeur_gradient_pixel_de_coord(self,x,y,image):
        """
        Fonction qui calcule la valeur du gradient d'un pixel d'une image à partir de ses coodonnées
        
        :param self: Gradient
        :type self: Objet Gradient
        
        :param x: abscisse du pixel pour lequel on cherche le gradient
        :type x: int
        
        :param y: ordonnée du pixel pour lequel on cherche le gradient
        :type y: int 
        
        :return: gradient d'un pixel
        :rtype: int
        
        """    
        
        L=self.calcule_liste_coord_pixels_voisins_depuis_coord(x,y)
        min=255
        max=0
        for coord in L:
            valeur=image.couleur_entier_8bits(coord[1],coord[0])
            if valeur<min:
                min=valeur
            if valeur>max:
                max=valeur
        return max-min
        
    
    def calcule_valeur_gradient_pixel(self,pix,image):
        """
        Fonction qui calcule la valeur du gradient du pixel pix de l'image selon l'élément structurant self.gamma
        
        :param self: Gradient
        :type self: Objet Gradient
        
        :param pix: pixel pour lequel on cherche les voisins
        :type pix: objet Pixel  
        
        :return: liste des pixels voisins de pix 
        :rtype: List
        
        """    
        
        return self.calcule_valeur_gradient_pixel_de_coord(pix.x,pix.y,image)  
            


    
    ## FONCTIONS DE RECHERCHE DE VOISINS
    
    def calcule_liste_coord_pixels_voisins_depuis_coord(self,x,y):
        """
        Procédure qui calcule la liste des coordonnées des pixels voisins du pixel de coordonnées (x,y) selon l'élément structurant self.gamma
        
        :param self: Gradient
        :type self: Objet Gradient
        
        :param x: abscisse du pixel pour lequel on cherche les voisins
        :type x: int
        
        :param y: ordonnée du pixel pour lequel on cherche les voisins
        :type y: int
        
        :return: liste des coordonnées des pixels voisins du pixel de coordonnées (x,y) selon l'élément structurant self.gamma
        :rtype: List
        
        """
        # initialisation de la liste des coordonnées des pixels voisins
        L=[]
        # remplissage de la liste des coordonnées en tenant compte des bords de l'image
        for element in self.gamma:
            X=x+element[0]
            Y=y+element[1]
            if 0<=X<self.largeur and 0<=Y<self.hauteur:
                L.append([X,Y])
        return L

    
    def calcule_liste_pixels_voisins_depuis_coord(self,x,y):
        """
        Fonction qui calcule la liste des pixels voisins du pixel de coordonnées (x,y) selon l'élément structurant self.gamma
        
        :param self: Gradient
        :type self: Objet Gradient
        
        :param x: abscisse du pixel pour lequel on cherche les voisins
        :type x: int
        
        :param y: ordonnée du pixel pour lequel on cherche les voisins
        :type y: int
        
        :return: liste des pixels voisins de pix selon l'élément structurant self.gamma
        :rtype: List
        
        """
        # initialisation de la liste des pixels voisins
        L_pixels=[]
        # calcul de la liste des coordonnées des pixels voisins
        L_coord=self.calcule_liste_coord_pixels_voisins_depuis_coord(x,y)
        # remplissage de la liste des pixels voisins
        for coord in L_coord:
            L_pixels.append(self.tab[coord[0]][coord[1]])
        return L_pixels
        
    def calcule_liste_pixels_voisins_depuis_pixel(self,pix):
        """
        Procédure qui calcule la liste des pixels voisins du pixel pix selon l'élément structurant self.gamma
        
        :param self: Gradient
        :type self: Objet Gradient
        
        :param pix: pixel pour lequel on cherche les voisins
        :type pix: objet Pixel  
        
        :return: liste des pixels voisins de pix selon self.gamma
        :rtype: List
        
        """
        
        return self.calcule_liste_pixels_voisins_depuis_coord(pix.x,pix.y)

    def affiche(self):
        
        """
        Procédure qui affiche le gradient, ligne de pixel par ligne de pixel
        
        :param self: Gradient
        :type self: Objet Gradient
        
        :return: None
        :rtype: None object
        
        """
        
        for ligne in self.tab:
            for pix in ligne:
                pix.affiche_pixel()
        

