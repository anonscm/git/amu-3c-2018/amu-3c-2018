# Programme : Simulateur de la machine Enigma (version complète)*

alphabet=['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']



# Description des cinq rotors 
rotor_1=['E','K','M','F','L','G','D','Q','V','Z','N','T','O','W','Y','H','X','U','S','P','A','I','B','R','C','J']
rotor_2=['A','J','D','K','S','I','R','U','X','B','L','H','W','T','M','C','Q','G','Z','N','P','Y','F','V','O','E']
rotor_3=['B','D','F','H','J','L','C','P','R','T','X','V','Z','N','Y','E','I','W','G','A','K','M','U','S','Q','O']
rotor_4=['E','S','O','V','P','Z','J','A','Y','Q','U','I','R','H','X','L','N','F','T','G','K','D','C','M','W','B']
rotor_5=['V','Z','B','R','G','I','T','Y','U','P','S','D','N','H','L','X','A','W','M','J','Q','O','F','E','C','K']


# Description des deux réflecteurs
Ref_A=['Y','R','U','H','Q','S','L','D','P','X','N','G','O','K','M','I','E','B','F','Z','C','W','V','J','A','T']
Ref_B=['R','D','O','B','J','N','T','K','V','E','H','M','L','F','C','W','Z','A','X','G','Y','I','P','S','U','Q']



# Fonction qui retourne le nombre associé à une lettre de l'alphabet (A=0, B=1, C=2, ...)
def lettre_en_nombre(lettre):
    nombre=alphabet.index(lettre)
    return nombre

# Fonction qui sélectionne le rotor choisi par l'utilisateur de la machine
def choix_rotor(numero):
    rotor=[]
    if numero==1:
        rotor=rotor_1
    if numero==2:
        rotor=rotor_2
    if numero==3:
        rotor=rotor_3
    if numero==4:
        rotor=rotor_4
    if numero==5:
        rotor=rotor_5
    return rotor

# Fonction qui sélectionne le réflecteur choisi par l'uitilisateur de la machine
def choix_reflecteur(numero):
    reflecteur=[]
    if numero==1:
        reflecteur=Ref_A
    else:
        reflecteur=Ref_B
    return reflecteur
    
    

# Fonction qui permet à l'utilisateur de la machine de brancher les câbles reliant des paires de lettres (choix de 6 au lieu de 10 câbles pour faciliter le travail)
def configuration_cablage_de_depart():
    liste=[]
    cables=input('Entrer les 6 paires de lettres reliées par un cable, en tapant les lettres dans l ordre :')
    for i in range(12):
        liste.append(lettre_en_nombre(cables[i]))
    return liste

# Fonction qui permet de vérifier si la lettre à coder est reliée par un câble à une autre lettre : Si oui, elle est transformée en cette lettre, sinon elle reste identique.
def valeur_apres_cablage_de_depart(valeur,liste):
    nouv=valeur
    if nouv in liste:
        n=liste.index(valeur)
        if n%2==0:
            nouv=liste[n+1]
        else:
            nouv=liste[n-1]
    return nouv

# Fonction qui retourne la nouvelle valeur après le passage dans un rotor    
def passage_dans_un_rotor(rotor,valeur):
    nouv_2=rotor[valeur]
    return nouv_2

# Fonction qui retourne la nouvelle valeur après le passage dans le réflecteur
def passage_dans_le_reflecteur(reflecteur,valeur):
    nouv_5=reflecteur[valeur]
    return nouv_5

# Fonction qui retourne la nouvelle valeur après le passage dans un rotor après passage dans le réflecteur  
def inverse_rotor(rotor,valeur):
    nouv_6=rotor.index(valeur)
    return nouv_6


# Fonction qui retourne le rotor après le décalage d'un rang vers la gauche
def decalage_un_rang(liste):
    a=liste[25]
    liste[25]=liste[0]
    for i in range(25):
        liste[i]=liste[i+1]
    liste[24]=a
    return liste



# Transformation des rotors et des réflécteurs en valeurs numériques
for i in range(26):
    rotor_1[i]=lettre_en_nombre(rotor_1[i])
    rotor_2[i]=lettre_en_nombre(rotor_2[i])
    rotor_3[i]=lettre_en_nombre(rotor_3[i])
    rotor_4[i]=lettre_en_nombre(rotor_4[i])
    rotor_5[i]=lettre_en_nombre(rotor_5[i])
    Ref_A[i]=lettre_en_nombre(Ref_A[i])
    Ref_B[i]=lettre_en_nombre(Ref_B[i])



# Choix des trois rotors utilisés parmi les cinq
i=input('Entrer le numéro du premier rotor choisi (1, 2, 3, 4 ou 5) : ')
i1=int(i)
j=input('Entrer le numéro du deuxième rotor choisi : ')
j1=int(j)
k=input('Entrer le numéro du troisième rotor choisi : ')
k1=int(k)

R1=choix_rotor(i1)
R2=choix_rotor(j1)
R3=choix_rotor(k1)

# Choix du réflecteur parmi les deux
l=input('Entrer le numéro du réflecteur choisi (1 ou 2):')
l1=int(l)

Ref=choix_reflecteur(l1)

# Initialisation des listes câble, message et message_code
cables=[]
message=[]
message_code=[]
tour=0   # La variable tour va permettre de compter une à une les lettres du messages à coder

# Configuration des câblages de la machine par l'utilisateur
cables=configuration_cablage_de_depart()


# L'utilisateur entrer le message à coder
message=input('Entrer le message à coder : ')


while  tour < len(message):   # La boucle s'arrête lorsque l'on a codé chaque lettre du message
  
    if tour%78<=25:
        nombre=lettre_en_nombre(message[tour])                      # Les différentes étapes sont : conversion de la lettre en nombre, câbles,
        nouv_1=valeur_apres_cablage_de_depart(nombre,cables)        # premier rotor, deuxième rotor, troisième rotor, réflecteur, troisième rotor,
        nouv_2=passage_dans_un_rotor(R1,nouv_1)                     # deuxième rotor, premier rotor, câbles, lettre codée
        nouv_3=passage_dans_un_rotor(R2,nouv_2)
        nouv_4=passage_dans_un_rotor(R3,nouv_3)
        nouv_5=passage_dans_le_reflecteur(Ref,nouv_4)
        nouv_6=inverse_rotor(R3,nouv_5)
        nouv_7=inverse_rotor(R2,nouv_6)
        nouv_8=inverse_rotor(R1,nouv_7)
        nouv_9=valeur_apres_cablage_de_depart(nouv_8,cables)
        message_code.append(alphabet[nouv_9])
        decalage_un_rang(R1)                                        # Le rotor 1 tourne d'un rang vers la gauche jusqu'à atteindre le 26ème tour,
                                                                    # alors à sa position initiale.
                                                                    
        
    if tour%78>25 and tour%78<=51:
        decalage_un_rang(R2)                                        # A partir du 26ème tour jusqu'au 52ème tour, le rotor 2 se met en mouvement.
        nombre=lettre_en_nombre(message[tour])                      # Les étapes sont alors les mêmes que précédemment et le rotor 2 tourne d'un rang
        nouv_1=valeur_apres_cablage_de_depart(nombre,cables)        # vers la gauche à chaque tour.
        nouv_2=passage_dans_un_rotor(R1,nouv_1)
        nouv_3=passage_dans_un_rotor(R2,nouv_2)
        nouv_4=passage_dans_un_rotor(R3,nouv_3)
        nouv_5=passage_dans_le_reflecteur(Ref,nouv_4)
        nouv_6=inverse_rotor(R3,nouv_5)
        nouv_7=inverse_rotor(R2,nouv_6)
        nouv_8=inverse_rotor(R1,nouv_7)
        nouv_9=valeur_apres_cablage_de_depart(nouv_8,cables)
        message_code.append(alphabet[nouv_9])
        
    if tour%78>51 and tour%78<=77:                                  # A partir du 52ème tour jusqu'au 78ème tour, le rotor 3 se met en mouvement.
        decalage_un_rang(R3)                                        # Les étapes sont alors les mêmes que précédemment et le rotor 3 tourne d'un rang
        nombre=lettre_en_nombre(message[tour])                      # vers la gauche à chaque tour.
        nouv_1=valeur_apres_cablage_de_depart(nombre,cables)
        nouv_2=passage_dans_un_rotor(R1,nouv_1)
        nouv_3=passage_dans_un_rotor(R2,nouv_2)
        nouv_4=passage_dans_un_rotor(R3,nouv_3)
        nouv_5=passage_dans_le_reflecteur(Ref,nouv_4)
        nouv_6=inverse_rotor(R3,nouv_5)
        nouv_7=inverse_rotor(R2,nouv_6)
        nouv_8=inverse_rotor(R1,nouv_7)
        nouv_9=valeur_apres_cablage_de_depart(nouv_8,cables)
        message_code.append(alphabet[nouv_9])
        

    tour=tour+1

# Les messages codés en général étaient très courts mais si le message comporte plus de 78 lettres, on revient à la configuration de départ, à savoir
# le rotor 1 se met en mouvement, d'où l'utilisation du modulo 78.


print(message_code)    # Affichage du message codé        
    
    









