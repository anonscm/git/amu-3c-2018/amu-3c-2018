#####################################
# Cecile Prouteau
# CTES Projet Math-Info
# Projet segmentation d'image
# 15/01/2019
# Classe Sommet Version 0.1
#####################################

### DEBUT de la classe Sommet
### contient la recherche des voisins , on se place du point de vue des coordonnées sur l'image
class Sommet:
    def __init__(self,x,y):
        self.x=x
        self.y=y
        
    def voisin(self,direction):
        if direction=='droite':
            return Sommet(self.x+1,self.y)
        elif direction=='gauche':
            return Sommet(self.x-1,self.y)
        elif direction=='haut':
            return Sommet(self.x,self.y-1)
        elif direction=='bas':
            return Sommet(self.x,self.y+1)
        else :
            print("Erreur de paramètre dans Sommet.voisin!")  
            exit(1) 
             
    def est_egal(self,sommet):
        """retourne vrai si self et sommet sont égaux""" 
        return  (self.x==sommet.x and self.y==sommet.y)   
            
    def affiche_sommet(self):
        print("(%d,%d)"%(self.x,self.y),end=";")        