#####################################
# Cecile Prouteau
# CTES Projet Math-Info
# Projet segmentation d'image
# 15/01/2019
# Classe Arc Version 0.1
#####################################

### IMPORTATION DES BIBLIOTHEQUES
import math

### IMPORTATION DES CLASESS
from Sommet import Sommet

### DEBUT de la classe Arc

class Arc:
    def __init__(self,origine,destination,capacite):
        """
        Origine et destination sont des sommets, capacite est un nombre entier
        
        Constructeur d'un objet Arc

        :param self: Objet Arc 
        :type self: Arc
        
        :param origine: objet Sommet de départ
        :type origine: Sommet 
        
        :param destination: objet Sommet  d'arrivée
        :type destination: Sommet   
        
        :param capacite: entier entre 0 et 255, capacite le l'arc
        :type capacite: int   
        
        :return: Objet Arc de 'origine' vers 'destination' et de capacite 'capacite'
        :rtype: Arc
        
        Liste des champs de la classe
        
        * self.origine sommet origine de l'arc
        * self.destination sommet destination de l'arc
        * self.capacite capacite de l'arc
        * self.capacite_res capacité résiduelle de l'arc initialisée à capacite
        * self.flot flot de l'arc initialisé à 0 
        
        Pour les lignes et les colonnes je me place du point de vue de l'image. 
        Dans le tableau numpy, les coordonnées sont inversées.
        """
        
        
        self.origine=origine
        self.destination=destination
        self.capacite=capacite
        #distinguer capacité et flot
        self.capacite_res=capacite
        self.flot=0
    
    def affiche_origine(self):
        """
        Affiche le sommet origine de l'arc
        
        :param self: Objet Arc 
        :type self: Arc
        
        :return: None 
        :rtype: None
        
        """
        
        self.origine.affiche_sommet() 
        
    def affiche_destination(self):
        """
        Affiche le sommet destination de l'arc
        
        :param self: Objet Arc 
        :type self: Arc
        
        :return: None 
        :rtype: None
        
        """
        
        self.destination.affiche_sommet() 
        
    def affiche_capacite(self):
        """
        Affiche la capacité de l'arc
        
        :param self: Objet Arc 
        :type self: Arc
        
        :return: None 
        :rtype: None
        
        """
        
        print("Capacité : %d"%self.capacite)
        
    def affiche_capacite_res(self):
        """
        Affiche la capacité résiduelle de l'arc
        
        :param self: Objet Arc 
        :type self: Arc
        
        :return: None 
        :rtype: None
        
        """
        
        print("Capacité résiduelle : %d"%self.capacite_res)
        
    def affiche_flot(self):
        """
        Retourne le flot de l'arc
        
        :param self: Objet Arc 
        :type self: Arc
        
        :return: None 
        :rtype: None
        
        """
        
        print("Flot : %d"%self.flot)
    
    def inverse(self,x):
        """

        :param self: Objet Arc 
        :type self: Arc
        
        :param x: nombre entier 
        :param x: int
        
        :return: inverse de x si x non nul, infini sinon
        :rtype: flottant       
        
        """
        
        if x == 0 :
            return math.inf
        else :
            return 1/x
    
    def inverse_capacite_res(self):
        """
        retourne l'inverse de la capacité résiduelle de l'arc
        
        :param self: Objet Arc 
        :type self: Arc
        
        :return: flottant flot, inverse du flot de l'arc si non nul, infini sinon
        :rtype: flottant
        
        """
        
        return self.inverse(self.capacite_res)
        
    def oppose(x):
        """

        :param x: nombre entier 
        :param x: int
        
        :return: 255-x si x non nul, infini sinon
        :rtype: int       
        
        """
        
        if x == 0 :
            return math.inf
        else :
            return 255-x
        
    def oppose_capacite_res(self):
        """
        Retourne l'opposé de la capacité résiduelle de l'arc
        
        :param self: Objet Arc 
        :type self: Arc
        
        :return: flottant flot, inverse du flot de l'arc si non nul, infini sinon
        :rtype: flottant
        
        :return: entier flot, 255-capacite_res de l'arc si non nul, infini sinon
        :rtype: int       
        
        """
        
        return oppose(self.capacite_res)
    
    # def est_identique(self,arc):
    #     """
    #     
    #     :param self: Objet Arc 
    #     :type self: Arc
    #     
    #     :return: booléen True si self et arc sont égaux et False sinon
    #     :rtype: bool

   ##       """ 
    #     
    #     return (self.origine.est_egal(arc.origine) and self.destination.est_egal(arc.destination) and self.capacite==arc.capacite)
    
    def modifie_capacite(self,valeur):
        """
        Procédure qui modifie le capacite de l'arc 'self' de 'valeur'
        
        :param self: Objet Arc 
        :type self: Arc
        
        :param valeur:  valeur dont on veut augmenter le capacite (la capacité) de l'arc 
        :type valeur: int
        
        :return: None 
        :rtype: None object
        
        """
        
        self.capacite=valeur
        
    def modifie_capacite_res(self,valeur):
        """
        Procédure qui modifie la capacité résiduelle de l'arc 'self' de 'valeur'
        
        :param self: Objet Arc 
        :type self: Arc
        
        :param valeur:  valeur dont on veut modifier la capacité résiduelle de l'arc self.capacite_res 
        :type valeur: int
        
        :return: None 
        :rtype: None object
        
        """
        
        self.capacite_res=valeur    
    
    def modifie_flot(self,valeur):
        """
        Procédure qui modifie le flot de l'arc 'self' de 'valeur'
        
        :param self: Objet Arc 
        :type self: Arc
        
        :param valeur:  valeur dont on veut modifier le flot de l'arc self.flot 
        :type valeur: int
        
        :return: None 
        :rtype: None object
        
        """
        
        self.flot=valeur 
    
    # def augmente_capacite(self,valeur):
    #     """
    #     Procédure qui augmente la capacite de l'arc 'self' de 'valeur'
    #     
    #     :param self: Objet Arc 
    #     :type self: Arc
    #     
    #     :param valeur:  valeur dont on veut modifier le capacite (la capacité) de l'arc 
    #     :type valeur: int
    #     
    #     :return: None 
    #     :rtype: None object
    #     
    #     """
    #     
    #     self.capacite+=valeur
        
    def augmente_capacite_res(self,valeur):
        """
        Procédure qui augmente la capacité résiduelle de l'arc 'self' de 'valeur'
        
        :param self: Objet Arc 
        :type self: Arc
        
        :param valeur:  valeur dont on veut augmenter la capacité résiduelle de l'arc self.capacite_res 
        :type valeur: int
        
        :return: None 
        :rtype: None object
        
        """
        
        self.capacite_res+=valeur    
    
    def augmente_flot(self,valeur):
        """
        Procédure qui augmente le flot de l'arc 'self' de 'valeur'
        
        :param self: Objet Arc 
        :type self: Arc
        
        :param valeur:  valeur dont on veut augmenter le flot de l'arc self.flot 
        :type valeur: int
        
        :return: None 
        :rtype: None object
        
        """
        
        self.flot+=valeur 
    
    # def diminue_capacite(self,valeur):
    #     """
    #     Procédure qui dinimue la capacite de l'arc 'self' de 'valeur'
    #     
    #     :param self: Objet Arc 
    #     :type self: Arc
    #     
    #     :param valeur:  valeur dont on veut diminuer le capacite (la capacité) de l'arc 
    #     :type valeur: int
    #     
    #     :return: None 
    #     :rtype: None object
    #     
    #     """
    #     
    #     self.capacite-=valeur
        
    def diminue_capacite_res(self,valeur):
        """
        Procédure qui diminue la capacité résiduelle de l'arc 'self' de 'valeur'
        
        :param self: Objet Arc 
        :type self: Arc
        
        :param valeur:  valeur dont on veut diminuer la capacité résiduelle de l'arc self.capacite_res 
        :type valeur: int
        
        :return: None 
        :rtype: None object
        
        """
        
        self.capacite_res-=valeur    
    
    def diminue_flot(self,valeur):
        """
        Procédure qui diminue le flot de l'arc 'self' de 'valeur'
        
        :param self: Objet Arc 
        :type self: Arc
        
        :param valeur:  valeur dont on veut diminuer le flot de l'arc self.flot 
        :type valeur: int
        
        :return: None 
        :rtype: None object
        
        """
        
        self.flot-=valeur 
    
    def affiche_arc(self):
        """
        Procédure qui affiche l'arc 'self' sous la forme origine(origine.x,origine.y) ; destination(destination.x,destination.y) ; capacite(capacite)
        
        :param self: Objet Arc 
        :type self: Arc
        
        :return: None 
        :rtype: None object
        
        """
        
        #print("origine(%d,%d) ; destination(%d,%d) ; capacite(%d)"%(self.origine.x,self.origine.y,self.destination.x,self.destination.y,self.capacite),end=";")
        print(self.origine.x,self.origine.y,self.destination.x,self.destination.y,self.capacite,self.capacite_res,self.flot)