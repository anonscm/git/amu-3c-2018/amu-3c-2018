# -*- coding: utf-8 -*-

### IPORTATION DES BIBLIOTHEQUES 
import tkinter.filedialog as tkfd
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import tkinter as tk
import numpy as np
import time

### INITIALISATION DES CONSTANTES
## Couleurs disponibles pour le coloriage des zones detectees
# Ce dictionnaire associe des noms de couleur au triplet de couleurs RGB codes chacun sur 8 bits
coul = {'red':(255,0,0), 'blue':(0,0,255), 'green':(0,255,0), 'yellow':(255,255,0), 'cyan':(0,255,255), 'magenta':(255,0,255)}

### DEFINITION DES FONCTIONS
## Fonction convertissant une couleur au format texte provenant de Liste_couleur en un triplet correspondant a son codage RGB
# str->(int,int,int)
# Couleur est un element de List_couleur et chacun des 3 entiers du triplet retourné est compris entre 0 et 255
def couleur_2_RVB(Couleur):
    return coul[Couleur]

## Fonction convertissant une couleur au format texte provenant de Liste_couleur en un triplet correspondant a son codage RGB dans une image PNG
# str->(float,float,float)
# Couleur est un element de List_couleur et chacun des 3 entiers du triplet retourné est compris entre 0 et 1
def couleur_2_RVB_png(Couleur):
    (rouge,vert,bleu)=coul[Couleur]
    return (rouge/256,vert/256,bleu/256)

## Fonction de coloriage dans la couleur Couleur d'une liste de points de l'image dans un tableau NUMPY
# List * str * Numpy Array -> 
# Procedure prend en entree une liste de points au format [x,y], une couleur de la liste List_couleur, 
# un tableau Numpy Array representant une image PNG en RVB.
# la fonction change la couleur des points du tableau T de coordonnées les valeurs se trouvant dans la liste pour Couleur.
# le tableau T est modifie par effets de bord
def colorie_liste_points(L,Couleur,T):
    (rouge,vert,bleu)=couleur_2_RVB_png(Couleur)
    for point in L:     # un point est une liste de 2 cordonnees [x,y]
        i=point[1]
        j=point[0]
        T[i, j, 0] = rouge
        T[i, j, 1] = vert
        T[i, j, 2] = bleu

### Application d'affichage principale ( FENETRE TKINTER )

class Application(object):
    def __init__(self):
        """Constructeur de la fenêtre principale"""
        ## Creation de la fenetre
        self.fenetre = tk.Tk()
        self.fenetre.title("SEGMENTATION D'IMAGE")
        
        ## Selection et ouverture du fichier Image
        # Selection du fichier image a traiter au format PNG niveau de gris a l'aide d'une fenetre de selection
        self.filepath = tkfd.askopenfilename(title="Ouvrir une image",filetypes=[('png files','.png'),('all files','.*')])
        
        # Ouverture du fichier image a traiter au format PNG niveau de gris et stockage dans la variable photo
        self.photo = tk.PhotoImage(file=self.filepath)
        
        ## Label de la fenetre principale 'fenetre'
        # self.label = tk.Label(self.fenetre, text="SEGMENTATION D'IMAGE",bg="ivory",font=("Helvetica", 28))
        # self.label.pack()
        
        ## Organisation des Frames
        # Frame1 dans fenetre
        self.Frame1 = tk.Frame(self.fenetre, borderwidth=0, relief=tk.GROOVE)
        self.Frame1.pack(side=tk.LEFT, padx=10, pady=10)
        
        # Frame2 dans fenetre
        self.Frame2 = tk.Frame(self.fenetre, borderwidth=0, relief=tk.GROOVE)
        self.Frame2.pack(side=tk.BOTTOM, padx=5, pady=5)
        
        # Frame3 dans Frame2
        self.Frame3 = tk.Frame(self.Frame2, borderwidth=0, relief=tk.GROOVE)
        self.Frame3.pack(side=tk.TOP, padx=5, pady=5)
        
        # Frame 4 dans Frame2
        self.Frame4 = tk.Frame(self.Frame2, bg="white", borderwidth=0, relief=tk.GROOVE)
        self.Frame4.pack(side=tk.BOTTOM, padx=40, pady=40)
        
        # Frame 5 dans Frame 4
        self.Frame5 = tk.Frame(self.Frame2, borderwidth=0, relief=tk.GROOVE)
        self.Frame5.pack(side=tk.TOP, padx=5, pady=5)
        
        # Frame 6 dans Frame 4
        self.Frame6 = tk.Frame(self.Frame4, borderwidth=0, relief=tk.GROOVE)
        self.Frame6.pack(side=tk.BOTTOM, padx=5, pady=5)
        
        # Frame 7 dans Frame 6
        self.Frame7 = tk.Frame(self.Frame6, borderwidth=0, relief=tk.GROOVE)
        self.Frame7.pack(side=tk.LEFT, padx=5, pady=5)
        
        # Frame 8 dans Frame 6
        self.Frame8 = tk.Frame(self.Frame6, borderwidth=0, relief=tk.GROOVE)
        self.Frame8.pack(side=tk.RIGHT, padx=5, pady=5)
        
        ## Ajout de labels pour les Frames
        tk.Label(self.Frame1, text="CHOIX DES PARAMETRES",font=("Helvetica", 10, 'bold')).pack(padx=10, pady=10)
        tk.Label(self.Frame3, text="LISTE DES POINTS SELECTIONNES",font=("Helvetica", 10, 'bold')).pack(padx=10, pady=10,side="top")
        tk.Label(self.fenetre, text="Cliquez sur l'image avec le bouton droit de la souris pour sélectionner les points définissant les zones (fond en premier)").pack(padx=10, pady=10)

        ## Initialisation de variable de classe
        self.Liste_points=[]
        
        ## Ajouts des Widgets

        # Parametre de choix d'algorithme
        # radiobutton
        self.no_algo = tk.StringVar() 
        self.bouton1 = tk.Radiobutton(self.Frame1, text="Graph Cuts", variable=self.no_algo, value=1) 
        self.bouton2 = tk.Radiobutton(self.Frame1, text="Partage des eaux", variable=self.no_algo, value=2)
        self.bouton1.pack()
        self.bouton2.pack()
        
        # Parametre Lambda determine a l'aide d'un curseur
        self.Lambda = tk.DoubleVar()
        self.scale = tk.Scale(self.Frame1, orient='horizontal', from_=0, to=10, resolution=0.1, tickinterval=2, length=350, variable=self.Lambda, label='Lambda')
        self.scale.pack()
        
        # Affichage de la liste des points selectionnes dans la fenetre
        self.sv = tk.StringVar()
        self.Frame4 = tk.LabelFrame(self.Frame3, text="Liste des points sélectionnés", padx=20, pady=20)
        self.Frame4.pack(fill="both", expand="yes")
        self.sv.set(str(self.Liste_points))
        tk.Label(self.Frame3, textvariable=self.sv).pack()

        # bouton de reinitialisation de la liste des points a vide 
        self.bouton_init = tk.Button(self.Frame5, text="Réinitialisation de la liste des points", command = self.Liste_nulle )
        self.bouton_init.pack()    
        
        # en cas de clik droit de la souris, appel de la fonction select_pixel_maj pour mise a jour de la liste et de l'affichage
        self.fenetre.bind("<Button-3>", self.select_pixel_maj )
        
        # Affichage du fichier image
        self.canvas = tk.Canvas(self.fenetre, width=self.photo.width(), height=self.photo.height(), bg="gray")
        self.canvas.create_image(0, 0, anchor=tk.NW, image=self.photo)
        self.canvas.pack()
        
        # Bouton d'importation
        self.bouton_importation=tk.Button(self.Frame7, text = "Importer", command = self.import_image)
        self.bouton_importation.pack(side="left")
        
        # Bouton de calcul
        self.bouton_calcul=tk.Button(self.Frame7, text = "Calculer", command = self.calcul_zone)
        self.bouton_calcul.pack(side="left")
        
        # Bouton d'affichage
        self.bouton_affichage=tk.Button(self.Frame7, text = "Afficher", command = self.affiche_zone)
        self.bouton_affichage.pack(side="left")
        
        # Bouton d'exportation
        self.bouton_exportation=tk.Button(self.Frame7, text = "Exporter", command = self.exporte_image)
        self.bouton_exportation.pack(side="left")
        
        # Bouton de sortie
        self.bouton_sortie=tk.Button(self.Frame8, text = "Quitter", command = self.fenetre.quit)
        self.bouton_sortie.pack(side="right")
        
        self.fenetre.mainloop()

    ## FONCTIONS ASSOCIEES AUX WIDGETS

    # Fonction d'initialisation de la liste des points
    # procedure sans argument qui remet la variable globale Listepoints a vide 
    # et qui met a jour son affichage dans la fenetre graphique
    def Liste_nulle(self):
        global Liste_points
        self.Liste_points=[]
        self.sv.set(str(self.Liste_points))
     
    # Fonction de sélection de pixel a la souris
    # event ->
    # prend en entree un evenement (qui sera le click droit de la souris), collecte les coordonnees X,Y du point clique
    # ajoute [X,Y] à la liste Liste_point (variable globale)
    def select_pixel(self,event):
        X=event.x
        Y=event.y
        self.Liste_points.append([X,Y])
        
    # Gestion de la selection des points
    # event->
    # quand event se produit (se sera click droit de la souris sur l'image), 
    # la liste Liste_points est augmentee des coordonnees du point clique et l'affichage de cette liste est mise a jour 
    def select_pixel_maj(self,event):
        self.select_pixel(event)
        self.sv.set(str(self.Liste_points))
        
    def import_image(self):
    #     # Selection du fichier image a traiter au format PNG niveau de gris a l'aide d'une fenetre de selection
    #     filepath = tkfd.askopenfilename(title="Ouvrir une image",filetypes=[('png files','.png'),('all files','.*')])
    #     # Ouverture du fichier image a traiter au format PNG niveau de gris et stockage dans la variable photo
    #     photo = tk.PhotoImage(file=filepath)
        print("Import de l'image !")
    
    # lancement du calcul des zones
    def calcul_zone(self):
        if self.no_algo.get()=='1':
            methode_choisie="des Graph Cuts"
            print("Calcul de la zone a segmenter par la méthode %s. " %methode_choisie)
        elif self.no_algo.get()=='2':
            methode_choisie="de partage des eaux"
            print("Calcul de la zone a segmenter par la méthode %s. " %methode_choisie)
        else : 
            print("Vous n'avez pas choisi de méthode. Veuillez faire un choix.")
    
    # Affichage de l'image avec les zones coloriees
    def affiche_zone(self):
        print("Affichage de la zone segmentée!")
    
    # Exportation de l'image avec les zones coloriees
    def exporte_image(self):
        print("exportation du fichier segmenté!")   
    
### FIN DE LA FENETRE TKINTER / classe APPLICATION

### DEBUT de la classe Image
### contient IMPORT - EXPORT PNG en niveaux de gris <-> NUMPY ARRAY et AFFICHAGE 
class Image:
    def __init__(self,FilePath):
        print ("importation d'une image")
        self.file=FilePath
        self.image_np_gray=self.import_image_from_file()
        self.nb_ligne = self.image_np_gray.shape[0]
        self.nb_colonne = self.image_np_gray.shape[1]
        self.image_np_RGB=self.gray_2_RGB()

    # fonction de chargement dans la variable img de l'image png accessible avec le chemin filepath
    # str -> Numpy Array
    # importe l'image png à l'adresse FilePath
    def import_image_from_file(self):
        return mpimg.imread(self.file)
    
    # fonction de conversion d'un tableau numpy image importe du fichier image en niveaux de gris PNG en un tableau numpy png RGB
    # image -> numpy Array 
    # les donnees de l'image importee en niveaux de gris PNG sont recopiees dans un numpy Array image RGB PNG qui est retourne 
    def gray_2_RGB(self):
        # Initialisation des tableau numpy a la taille de l'image et pleins de zeros
        #gray = np.zeros((nb_ligne,nb_colonne))
        gray_RGB = np.zeros((self.nb_ligne,self.nb_colonne,3))
        # Importation des donnees de l'image dans les tableaux numpy, en niveau de gris d'une part (pour le traitement)
        # en couleur RVB d'autre part (pour l'affichage et l'export)
        for i in range (self.nb_ligne):
            for j in range (self.nb_colonne):
                gray_RGB[i, j, 0] = self.image_np_gray[i, j]
                gray_RGB[i, j, 1] = self.image_np_gray[i, j]
                gray_RGB[i, j, 2] = self.image_np_gray[i, j]
        return gray_RGB
    
    # colorie la zone determinee par L dans l'image RGB avec la couleur Couleur
    def colorie_zone_img_RGB_np(self,L,Couleur):
        colorie_liste_points(L,Couleur,self.image_np_RGB)
    
    # Affichage de l'image transformee 
    # numpy Array ->
    # procedure affichant l'image contenu dans le numpy Array image
    def affiche_image_RGB(self):       
        plt.axis('off')
        plt.imshow(self.image_np_RGB)
        plt.show()
    
    # Export de l'image  
    # numpy Array * str ->
    # procedure exportant le tableau numpy image à l'adresse FilePath modifiee pour tenir compte du changement de format
    def export_image_2_file(self):
        filepath_gray_RGB=self.file.rstrip('.png')+"_gray_RGB.png"
        mpimg.imsave(filepath_gray_RGB,self.image_np_RGB)
### FIN DE LA CLASSE IMAGE

###______Programme principal_______________________________________________________
if __name__ == '__main__':
    f = Application() # instanciation de l'objet application
    
    ## test des fonctions
    # Creation d'une instance de Image
    img=Image(f.filepath)
    # Coloriage d'une liste de points sélectionnés aux clavier sur le tableau de l'image a exporter 
    # permettra de colorier une zone segmentee ulterieurement
    img.colorie_zone_img_RGB_np(f.Liste_points,'red')
    # Affichage de l'image transformee 
    img.affiche_image_RGB()
    # Export de l'image transformee
    img.export_image_2_file()
    
    # Affichage de la liste_des points, de Lamdba et de No_algo
    print(f.Liste_points)
    print(f.Lambda.get())
    print(f.no_algo.get())

