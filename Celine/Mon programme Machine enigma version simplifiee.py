# Programme : Simulateur de la machine Enigma (version simplifiée)*

# Configuration de la machine 
alphabet=['A','B','C','D']
cablage_de_depart=[1,0,2,3]   # A devient B, B devient A, C reste C, D reste D
reflecteur=[1,0,3,2]          # A et B sont échangés, C et D sont échangés

# Fonction qui retourne le nombre associé à une lettre de l'alphabet (A=0,B=1,C=2,D=4)
def lettre_en_nombre(lettre):
    nombre=alphabet.index(lettre)
    return nombre

# Fonction qui retourne la nouvelle valeur de la lettre si elle est reliée à une autre par un câble
# ou garde sa valeur
def valeur_apres_cablage_de_depart(valeur):
    nouv_1=cablage_de_depart[valeur]
    return nouv_1

# Fonction qui retourne la nouvelle valeur de la lettre après passage dans le premier rotor

def passage_dans_le_premier_rotor(valeur):
    nouv_2=rotor_1[valeur]
    return nouv_2

# Fonction qui retourne la nouvelle valeur de la lettre après passage dans le deuxième rotor
def passage_dans_le_deuxieme_rotor(valeur):
    nouv_3=rotor_2[valeur]
    return nouv_3

# Fonction qui retourne la nouvelle valeur de la lettre après passage dans le troisème rotor
def passage_dans_le_troisieme_rotor(valeur):
    nouv_4=rotor_3[valeur]
    return nouv_4

# Fonction qui retourne la nouvelle valeur de la lettre après passage dans le réflecteur
def passage_dans_le_reflecteur(valeur):
    nouv_5=reflecteur[valeur]
    return nouv_5

# Fonction qui retourne la nouvelle valeur de la lettre après passage dans le troisième rotor
def inverse_rotor_3(valeur):
    nouv_6=rotor_3.index(valeur)
    return nouv_6

# Fonction qui retourne la nouvelle valeur de la lettre après passage dans le deuxième rotor
def inverse_rotor_2(valeur):
    nouv_7=rotor_2.index(valeur)
    return nouv_7

# Fonction qui retourne la nouvelle valeur de la lettre après passage dans le premier rotor
def inverse_rotor_1(valeur):
    nouv_8=rotor_1.index(valeur)
    return nouv_8

# Fonction qui retourne le premier rotor après le décalage d'un rang vers la gauche
def rotation_rotor_1(tour):
    rotor_1=[1,3,0,2]       # Le rotor tourne que sur les quatre premières lettres et
    if tour%12 == 1:        # retourne à sa position initiale 
        rotor_1=[3,0,2,1]
    if tour%12 == 2:
        rotor_1=[0,2,1,3]
    if tour%12 == 3:
        rotor_1=[2,1,3,0]
    return rotor_1

# Fonction qui retourne le deuxième rotor après le décalage d'un rang vers la gauche
def rotation_rotor_2(tour):
    rotor_2=[2,3,0,1]       # Le rotor tourne que sur les cinq premières lettres à huit
    if tour%12 == 5:        # et retourne à sa position initiale 
        rotor_2=[3,0,1,2]
    if tour%12 == 6:
        rotor_2=[0,1,2,3]
    if tour%12 == 7:
        rotor_2=[1,2,3,0]
    return rotor_2

# Fonction qui retourne le troisème rotor après le décalage d'un rang vers la gauche
def rotation_rotor_3(tour):
    rotor_3=[0,3,1,2]       # Le rotor tourne que sur les neuf premières lettres à douze
    if tour%12 == 9:        # et retourne à sa position initiale 
        rotor_3=[3,1,2,0]
    if tour%12 == 10:
        rotor_1=[1,2,0,3]
    if tour%12 == 11:
        rotor_1=[2,0,3,1]
    return rotor_3
    

# Initialisation des listes message, message_code, rotor_1, rotor_2 et rotor_3
message=[]
message_code=[]
rotor_1=[]
rotor_2=[]
rotor_3=[]

message=input('Entrer le message à coder : ')
tour=0    # La variable tour va permettre de compter une à une les lettres du messages à coder


while tour < len(message):   # La boucle s'arrête lorsque l'on a codé chaque lettre du message
    
    nombre=lettre_en_nombre(message[tour])              # Les différentes étapes sont : conversion de la lettre en nombre, câbles,
                                                        # premier rotor, deuxième rotor, troisième rotor, réflecteur, troisième rotor,
    nouv_1=valeur_apres_cablage_de_depart(nombre)       # deuxième rotor, premier rotor, câbles, lettre codée
    
    rotor_1=rotation_rotor_1(tour)
    rotor_2=rotation_rotor_2(tour)
    rotor_3=rotation_rotor_3(tour)
    
    nouv_2=passage_dans_le_premier_rotor(nouv_1)
    nouv_3=passage_dans_le_deuxieme_rotor(nouv_2)
    nouv_4=passage_dans_le_troisieme_rotor(nouv_3)
    
    nouv_5=passage_dans_le_reflecteur(nouv_4)
    
    nouv_6=inverse_rotor_3(nouv_5)
    nouv_7=inverse_rotor_2(nouv_6)
    nouv_8=inverse_rotor_1(nouv_7)
    
    nouv_9=valeur_apres_cablage_de_depart(nouv_8)
    
 
    message_code.append(alphabet[nouv_9])
    tour=tour+1
    
# Si le message code comporte plus de 12 lettres, on revient à la configuration de départ, à savoir le rotor 1 se met en mouvement,
#d'où l'utilisation du modulo 12


print(message_code)    # Affichage du message codé   






