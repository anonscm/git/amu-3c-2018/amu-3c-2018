#####################################
# Cecile Prouteau
# CTES Projet Math-Info
# Projet segmentation d'image
# 15/03/2019
# Classe Pixel Version 3.0
#####################################
import math

### DEBUT de la classe Pixel
### contient la recherche des voisins , on se place du point de vue des coordonnées sur l'image
class Pixel:
    """

    **LES METHODES**
     
    """
    
    def __init__(self,x,y,val):
        """
        Constructeur d'un objet Pixel
        
        Construit un sommet à partir des coordonnées d'un pixel sur une image
        
        :param self: Objet Sommet 
        :type self: Sommet
        
        :param x: abscisse du point sur l'image
        :type FilePath: int  
        
        
        :param y: ordonnée du point  
        :type FilePath: int      
        
        :param v: valeur du point  
        :type FilePath: int 
        
        :return: Objet Sommet
        :rtype: Sommet
        
        Liste des champs de la classe
        
        * self.x abscisse 
        * self.y ordonnée
        * self.val valeur du pixel self
        * self.zone indique dans quelle zone se trouve le pixel 
        * self.liste_coord_voisins contiendra la liste des coordonnées des voisins du pixel sur l'image ; initialisé à []
        
        Pour les lignes et les colonnes je me place du point de vue de l'image. 
        Dans le tableau numpy, les coordonnées sont inversées.
        
        """
        
        self.x=x
        self.y=y
        self.val=val
        self.zone=-1
        self.liste_coord_voisins=[]
        
    def maj_val(self,val):
        """
        Procedure qui met à jour l'attribu self.val avec la valeur val
        
        :param self: Objet Sommet 
        :type self: Sommet
        
        :param val: entier à pour mise à jour du numéro de sommet
        :type val: int  
        
        :return: None 
        :rtype: None object
        
        """
        self.val=val
        
    def maj_zone(self,zone):
        """
        Procedure qui met à jour l'attribu self.zone avec la valeur zone
        
        :param self: Objet Sommet 
        :type self: Sommet
        
        :param zone: entier à pour mise à jour de la zone d'appartenance au sommet
        :type zone: int  
        
        :return: None 
        :rtype: None object
        
        """
        self.zone=zone
    
    def maj_liste_coord_voisins(self,liste):
        """
        Procedure qui met à jour l'attribu self.zone avec la valeur zone
        
        :param self: Objet Sommet 
        :type self: Sommet
        
        :param liste: liste de coordonnées à ajouter à self.liste_coord_voisins
        :type liste: List 
        
        :return: None 
        :rtype: None object
        
        """
        for  element in liste:
              self.liste_coord_voisins.append(element)
   
    def coord_voisin(self,direction):
        
        """
        :param self: Objet Sommet 
        :type self: Sommet
        
        :param direction: chaine de caractère parmi 'droite', 'gauche', 'haut', 'bas', 'NE', 'SE', 'SO', 'NO'
        :type direction: str  
        
        :return: un couple de coordonnée entières (x', y') du pixel voisin de celui de coordonnée (x,y) dans la direction indiquée par la variable direction. 
        :rtype: int*int
        
        """
        
        if direction=='droite':
            return (self.x+1,self.y)
        elif direction=='gauche':
            return (self.x-1,self.y)
        elif direction=='haut':
            return (self.x,self.y-1)
        elif direction=='bas':
            return (self.x,self.y+1)
        elif direction=='NE':
            return (self.x+1,self.y-1)
        elif direction=='SE':
            return (self.x+1,self.y+1)
        elif direction=='SO':
            return (self.x-1,self.y+1)
        elif direction=='NO':
            return (self.x-1,self.y-1)
        else :
            print("Erreur de paramètre dans Sommet.coord_voisin!")  
            exit(1) 
             
    def est_superieur(self,pixel):
        """
        
        :param self: Objet Pixel 
        :type self: Pixel
        
        :param sommet: Objet Pixel
        :type sommet: Pixel  
        
        :return: True si la valeur de self est strictement plus grande que la valeur de pixel et False sinon
        :rtype: bool
        
        """ 
        
        return  self.val > pixel.val  
        
    def est_inferieur(self,pixel):
        """
        
        :param self: Objet Pixel 
        :type self: Pixel
        
        :param sommet: Objet Pixel
        :type sommet: Pixel  
        
        :return: True si la valeur de self est strictement plus petite que la valeur de pixel et False sinon
        :rtype: bool
        
        """ 
        
        return  self.val < pixel.val 
            
    def affiche_pixel(self):
        """
        Affiche les coordonnées de self
        
        :param self: Objet Sommet 
        :type self: Sommet
        
        :return: None 
        :rtype: None object
        
        """
        print("(",self.x,";",self.y,") ;",self.val,";",self.zone)        