.. Segmentation Image documentation master file, created by
   sphinx-quickstart on Wed Mar  6 16:56:54 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Projet Segmentation d'Image : Bienvenue!
=========================================
Ceci est la page de mon projet sur la segmentation d'image. Ce projet contient :
	* Un programme python permettant de découper des zones dans une image
	* La documentation du programme éditée avec sphinx
	* Les fichiers RST

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices et tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
