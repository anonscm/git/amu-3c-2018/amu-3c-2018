#####################################
# Cecile Prouteau
# CTES Projet Math-Info
# Projet segmentation d'image
# 15/01/2019
# Classe Couleur Version 1.4
#####################################

### INITIALISATION DES CONSTANTES
## Couleurs disponibles pour le coloriage des zones detectees
# Ce dictionnaire associe des noms de couleur au triplet de couleurs RGB codes chacun sur 8 bits
coul = {'red':(255,0,0), 'blue':(0,0,255), 'green':(0,255,0), 'yellow':(255,255,0), 'cyan':(0,255,255), 'magenta':(255,0,255)}

class Couleur:
    
    def __init__(self,couleur):
        self.nom=couleur
        self.RVB_8bits=self.couleur_2_RVB(couleur)
        self.RVB_png=self.couleur_2_RVB_png(couleur)
        
    ### DEFINITION DES FONCTIONS
    
    def couleur_2_RVB(self,Couleur):
        """ Fonction convertissant une couleur au format texte provenant de Liste_couleur en un triplet correspondant a son codage RGB
        str->(int,int,int)
        Couleur est un element de List_couleur et chacun des 3 entiers du triplet retourné est compris entre 0 et 255 """    
        
        return coul[Couleur]
    
    def couleur_2_RVB_png(self,Couleur):
        """ Fonction convertissant une couleur au format texte provenant de Liste_couleur en un triplet correspondant a son codage RGB dans une image PNG
        str->(float,float,float)
        Couleur est un element de List_couleur et chacun des 3 entiers du triplet retourné est compris entre 0 et 1"""
        
        (rouge,vert,bleu)=coul[Couleur]
        return (rouge/256,vert/256,bleu/256)
    
    def couleur_float_2_entier_8bits(c):
        """converti la couleur/niveau de gris flottant du format png comme entier entre 0 et 255"""
        c1=256*c
        if c1>255:
            c1=255
        return c1
