Documentation technique
+++++++++++++++++++++++

  
.. automodule:: Couleur

Couleur
=======
 
This comments are not included in the *docstrings*.

.. autoclass:: Couleur
  :members:
  :private-members:
  :special-members:
  

.. automodule:: Image

Image
=====
 
This comments are not included in the *docstrings*.

.. autoclass:: Image
  :members:
  :private-members:
  :special-members:
  

.. automodule:: Sommet

Sommet
======
 
This comments are not included in the *docstrings*.

.. autoclass:: Sommet
  :members:
  :private-members:
  :special-members:
  
.. automodule:: Arc

Arc
===
 
This comments are not included in the *docstrings*.

.. autoclass:: Arc
  :members:
  :private-members:
  :special-members:

 
.. automodule:: Graphe

Graphe
======
 
This comments are not included in the *docstrings*.

.. autoclass:: Graphe
  :members:
  :private-members:
  :special-members:


.. automodule:: Pixel
  
Pixel
=====
 
This comments are not included in the *docstrings*.

.. autoclass:: Pixel
  :members:
  :private-members:
  :special-members:


.. automodule:: Gradient
  
Gradient
========
 
This comments are not included in the *docstrings*.

.. autoclass:: Gradient
  :members:
  :private-members:
  :special-members:

 
.. automodule:: ListeTrieePixel
  
ListeTrieePixel
===============
 
This comments are not included in the *docstrings*.

.. autoclass:: ListeTrieePixel
  :members:
  :private-members:
  :special-members:

 
.. automodule:: Partage_eaux
  
Partage_eaux
============
 
This comments are not included in the *docstrings*.

.. autoclass:: Partage_eaux
  :members:
  :private-members:
  :special-members:
.. automodule:: Segmentation_GUI

Segmentation_GUI
================
 
This comments are not included in the *docstrings*.

.. autoclass:: Application
  :members:
  :private-members:
  :special-members:


 
